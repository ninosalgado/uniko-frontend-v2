export const environment = {
  production: false
};

export const URL_RESOURCES = {
  API_UNIKO: 'https://staging-api.uniko.co/api/v2',
  UNIKO: 'http://evento-v2.uniko.co',
  GLOBAL: 'https://uniko.co'
};

export const KEYS = {
  CONEKTA_GUEST: 'key_LvaKwL2B1egfr5suejx7mEA',
  CONEKTA_ACCOUNT: 'key_OmbhrYrDCMJtzhJmzxgxzrA',
  paypalLive: 'sandbox',
  INSTAGRAM_CLIENT_ID: '1cac2e75f3c44d7f9c75f7b760728f3b'
}
