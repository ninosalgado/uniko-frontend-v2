export const environment = {
  production: true
};

export const URL_RESOURCES = {
  API_UNIKO: 'https://api-dev.uniko.co/api/v2',
  UNIKO: 'https://evento.uniko.co',
  GLOBAL: 'https://uniko.co'
};

export const KEYS = {
  CONEKTA_GUEST: 'key_Ydfni2pxzs9y1Fo1GrxyU1A',
  CONEKTA_ACCOUNT: 'key_a9HqqNepYLsXpr2oUeFyJ5Q',
  paypalLive: 'live',
  INSTAGRAM_CLIENT_ID: '1cac2e75f3c44d7f9c75f7b760728f3b'
}
