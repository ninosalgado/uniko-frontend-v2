// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const URL_RESOURCES = {
  API_UNIKO: 'https://api-dev.uniko.co/api/v2',
  UNIKO: 'https://evento.uniko.co',
  GLOBAL: 'https://uniko.co'
};

export const KEYS = {
  CONEKTA_GUEST: 'key_Ydfni2pxzs9y1Fo1GrxyU1A',
  CONEKTA_ACCOUNT: 'key_a9HqqNepYLsXpr2oUeFyJ5Q',
  paypalLive: 'live',
  INSTAGRAM_CLIENT_ID: '1cac2e75f3c44d7f9c75f7b760728f3b'
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
