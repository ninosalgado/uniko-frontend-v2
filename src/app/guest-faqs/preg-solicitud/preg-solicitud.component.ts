import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IFrequencyQuestions } from 'src/app/_interface/frequencyQuestion';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from 'src/app/services/notifications.service';
import { FrequentQuestionService } from 'src/app/services/frequent-question.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LayoutService } from 'src/app/services/layout.service';


@Component({
  selector: 'app-preg-solicitud',
  templateUrl: './preg-solicitud.component.html',
  styleUrls: ['./../guest-faqs.component.scss']
})
export class PregSolicitudComponent implements OnInit {
  @Input() faqs: IFrequencyQuestions[] = [];
  dataForm: FormGroup;
  submit: boolean = false;
  faqEdit: IFrequencyQuestions;
  isOpen: boolean = false;
  constructor( 
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private frequentQuestionService: FrequentQuestionService,
    private formBuilder: FormBuilder,
    private layoutService: LayoutService,

  ) { 
    this.onForm();
  }

  ngOnInit() {
  }

  onForm() {
    this.dataForm = this.formBuilder.group({
      question: [this.faqEdit ? this.faqEdit.question : '', Validators.required],
      answer: [this.faqEdit ? this.faqEdit.answer : '', Validators.required]
    });
  }

  closeModal() {
    this.isOpen = false;
  }

  editaPregunta(faq: IFrequencyQuestions){
    this.faqEdit = faq;
    this.onForm();
    this.isOpen = true;
  }
  borraPregunta(faq: IFrequencyQuestions) {
    const dialogRef = this.dialog.open(DialogBorraSolicitud, { data: faq, disableClose: true });
   
     dialogRef.afterClosed().subscribe(result => {
       if(result){
         this.trash(faq.id);
       } else {
         console.log("Cancelar");
       }
     });
  }

  async trash(id: string): Promise<void> {
    try {
      this.spinner.show();
      await this.frequentQuestionService.delelte(id);
      this.faqs = this.faqs.filter(data => data['id'] !== id);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async save():Promise<void> {
    try {
      this.submit = true;
      this.spinner.show();
      if (this.dataForm.invalid) {
        return;
      }
      await this.frequentQuestionService.frequentQuestion(
        this.dataForm.value.question,
        this.dataForm.value.answer,
        this.faqEdit.id,
        ''
      );
      const faq = this.faqs.find(data => data.id === this.faqEdit.id);
      faq.answer = this.dataForm.value.answer;
      faq.question = this.dataForm.value.question;
      this.isOpen = false;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }
}

@Component({
  selector: 'borra-solicitud-dialog',
  templateUrl: './borra-solicitud-dialog.html',
  styleUrls: ['./../guest-faqs.component.scss']
})
export class DialogBorraSolicitud {
  constructor(public matDialogRef: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: any) {

  }
}