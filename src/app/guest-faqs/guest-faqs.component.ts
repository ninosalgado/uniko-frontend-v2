import { Component, OnInit } from '@angular/core';
import { Button, CurrentUser } from '../_models/data';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationsService } from '../services/notifications.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FrequentQuestionService } from '../services/frequent-question.service';
import { LayoutService } from '../services/layout.service';
import { IFrequencyQuestions } from '../_interface/frequencyQuestion';
import { ICoupleInformation } from '../_interface/coupleInformation';
import { CoupleService } from '../services/couple.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-guest-faqs',
  templateUrl: './guest-faqs.component.html',
  styleUrls: ['./guest-faqs.component.scss']
})
export class GuestFaqsComponent implements OnInit {
  buttons: Array<Button> = [];
  isOpen: boolean = false;
  dataForm: FormGroup;
  submit: boolean = false;
  faqs: IFrequencyQuestions[] = [];
  faqsPublics: IFrequencyQuestions[] = [];
  faqsSolicited: IFrequencyQuestions[] = [];
  isPublic: boolean = true;
  couple: ICoupleInformation;
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private router: Router,
    private route: ActivatedRoute,
    private frequentQuestionService: FrequentQuestionService,
    private layoutService: LayoutService,
    private coupleService: CoupleService

  ) { 
    localStorage.removeItem("currentUser");
    this.layoutService.setCurrentUser(new CurrentUser);
    this.get();
    this.onButtons();        
  }

  ngOnInit() {
    this.onForm();
  }

  onButtons() {
    this.buttons = [];
    const buttonPreview = new Button(
      'Enviar pregunta',
      this.add,
      'fa-plus',
      false,
      this
    );
    this.buttons.push(buttonPreview);
  }

  onForm() {
    this.dataForm = this.formBuilder.group({
      question: ['', Validators.required],
    });
  }

  add(event: Event): void {
    event.stopPropagation();
    event.preventDefault();
    this['parent'].isOpen = true;
  }

  closeModal() {
    this.isOpen = false;
  }

  async get(): Promise<void> {
    try {
      this.spinner.show();
      localStorage.clear();
      const couple: ICoupleInformation = await this.coupleService.findOneRestrict(this.route.snapshot.parent.params.url);
      this.couple = couple;
      this.layoutService.coupleInformation.info = couple;
      this.layoutService.setCoupleInformation(couple);
      const auth = await this.coupleService.getPlanPermisions(this.layoutService.coupleInformation.info.id);
      this.layoutService.authorization.info = auth;
      if (!this.layoutService.authorization.verify('QUESTIONS')) {
        this.layoutService.redirect();
      }
      
      this.faqs = await this.frequentQuestionService.getBycouple(this.layoutService.getCoupleInformation().id);
      this.faqsPublics = this.faqs.filter(data => data.public);
      this.faqsSolicited = this.faqs.filter(data => !data.public);    
      this.isPublic = this.couple.payInformationData.isPaid;      
    } catch (e) {
      this.notification.error(e);
      this.router.navigate(['/search']);
    } finally {
      this.spinner.hide();
    }
  }

  async save(): Promise<void> {
    try {
      this.submit = true;
      this.spinner.show();
      if (this.dataForm.invalid) {
        return ;
      }
      await this.frequentQuestionService.generateQuestion(
        this.couple.id,
        this.dataForm.value.question, 
      );
      this.isOpen = false;
      this.get();
      this.notification.success('Pregunta enviada')
    } catch (e) {
      this.notification.error(e)
    } finally {
      this.spinner.hide();
    }
  }

}
