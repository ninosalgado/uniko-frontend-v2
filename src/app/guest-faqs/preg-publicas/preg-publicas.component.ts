import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from 'src/app/services/notifications.service';
import { FrequentQuestionService } from 'src/app/services/frequent-question.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LayoutService } from 'src/app/services/layout.service';
import { IFrequencyQuestions } from 'src/app/_interface/frequencyQuestion';

@Component({
  selector: 'app-preg-publicas',
  templateUrl: './preg-publicas.component.html',
  styleUrls: ['./../guest-faqs.component.scss']
})
export class PregPublicasComponent implements OnInit {
  @Input() faqs: IFrequencyQuestions[] = [];
  dataForm: FormGroup;
  submit: boolean = false;
  faqEdit: IFrequencyQuestions;
  isOpen: boolean = false;
  constructor( 
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private frequentQuestionService: FrequentQuestionService,
    private formBuilder: FormBuilder,
    private layoutService: LayoutService,

  ) { 
    this.onForm();
  }

  ngOnInit() {
  }

  onForm() {
    this.dataForm = this.formBuilder.group({
      question: [this.faqEdit ? this.faqEdit.question : '', Validators.required],
      answer: [this.faqEdit ? this.faqEdit.answer : '', Validators.required]
    });
  }

  closeModal() {
    this.isOpen = false;
  }

  editaPregunta(faq: IFrequencyQuestions){
    this.faqEdit = faq;
    this.onForm();
    this.isOpen = true;
  }
  borraPregunta(faq: IFrequencyQuestions) {
    const dialogRef = this.dialog.open(DialogBorraPublicas, { data: faq, disableClose: true });
   
     dialogRef.afterClosed().subscribe(result => {
       if(result){
         this.trash(faq.id);
       } else {
         console.log("Cancelar");
       }
     });
  }

  async trash(id: string): Promise<void> {
    try {
      this.spinner.show();
      await this.frequentQuestionService.delelte(id);
      this.faqs = this.faqs.filter(data => data.id !== id);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  existImg(preg) {
    return preg && preg.image ? true : false;
  }

  getImageToVideo(preg) {
    const isExist = preg.image && preg.image.length ? true : false;
    const link = preg.image ? preg.image : '';
    const existType = link.search("type");
    if (existType > 10) {
      try {
        const type = link.slice(existType);
        const typeArray = type.split('=');
        const _type = atob(typeArray[1]);
        if (_type.search('video') >= 0 || (typeArray[1].search('video') >= 0)) {
          return `<video src="${link}" controls=""></video>`;
        } else {
          return `<img src="${link}">`;  
        }
      } catch (e) {
        return isExist ? `<img src="${link}">` : '';  
      }
    } else {
      return isExist ? `<img src="${link}">` : '';
    }
  }

  async save():Promise<void> {
    try {
      this.submit = true;
      this.spinner.show();
      if (this.dataForm.invalid) {
        return;
      }
      await this.frequentQuestionService.frequentQuestion(
        this.dataForm.value.question,
        this.dataForm.value.answer,
        this.faqEdit.id,
        ''
      );
      const faq = this.faqs.find(data => data.id === this.faqEdit.id);
      faq.answer = this.dataForm.value.answer;
      faq.question = this.dataForm.value.question;
      this.isOpen = false;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}

@Component({
  selector: 'borra-publicas-dialog',
  templateUrl: './borra-publicas-dialog.html',
  styleUrls: ['./../guest-faqs.component.scss']
})
export class DialogBorraPublicas {
  constructor(public matDialogRef: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: any) {

  }
}