import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestFaqsComponent } from './guest-faqs.component';

const routes: Routes = [{
  path: '',
  component: GuestFaqsComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestFaqsRoutingModule { }
