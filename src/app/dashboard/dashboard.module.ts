import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatMenuModule } from '@angular/material/menu';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { GiftListComponent } from './gift-list/gift-list.component';
@NgModule({
  declarations: [   
    DashboardComponent, 
    GiftListComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatMenuModule,
    FormsModule,
    MaterialModule,
    SharedModule
  ],
  providers: [  
    MatDatepickerModule,  
  ]
})
export class DashboardModule { }
