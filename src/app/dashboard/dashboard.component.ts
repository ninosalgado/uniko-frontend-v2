import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../services/layout.service';
import * as moment from 'moment';
import { ICoupleInformation } from '../_interface/coupleInformation';
import { CoupleService } from '../services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  diffDate: any = {
    days: '00',
    hours: '00',
    minutes: '00',
    seconds: '00'
  }
  coupleInformation: ICoupleInformation;
  totalGifts:number;
  guestList:any;
  guests:number = 0;
  confirmedGuests:number = 0;
  constructor(
    private layoutService:LayoutService,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService
  ) { 
    this.coupleInformation = this.layoutService.getCoupleInformation();
    this.layoutService.coupleEmitter.subscribe(data => {
      // this.ngOnInit();
    })
    this.diffDateNow();
    
  }

  async ngOnInit() {
    this.spinner.show();
    const couple = await this.coupleService.get(this.coupleInformation.id);
    this.coupleInformation = couple;
    this.layoutService.setCoupleInformation(this.coupleInformation);
    this.totalGifts = this.coupleInformation.productsRegistryList.length;
    await this.getGuestList();
    this.spinner.hide();
  }

  diffDateNow() {
    setInterval(() => {
      if (this.coupleInformation.weddingData.date) {
        const diffDays = moment(this.coupleInformation.weddingData.date).diff(moment(), 'day');
        const diffHours = moment(this.coupleInformation.weddingData.date).diff(moment().add(diffDays, 'day'), 'hours');
        const diffMinuts = moment(this.coupleInformation.weddingData.date).diff(moment().add(diffDays, 'day').add(diffHours, 'hours'), 'minutes');
        const diffSeconds = moment(this.coupleInformation.weddingData.date).diff(moment().add(diffDays, 'day').add(diffHours, 'hours').add(diffMinuts, 'minutes'), 'seconds');
        this.diffDate = {
          days: diffDays,
          hours: diffHours,
          minutes: diffMinuts,
          seconds: diffSeconds
        }
      }
    }, 1000)
  }

  async getGuestList(){
    let result = await this.coupleService.getGuestList(this.coupleInformation.id);
    console.log(result);
    this.guestList = result; 
    this.guests = this.guestList.length;
    this.confirmedGuests = this.guestList.filter(item=> item.notAsist).length;
  }
}
