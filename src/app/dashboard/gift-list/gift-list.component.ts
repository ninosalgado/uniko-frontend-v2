import { Component, OnInit, Input } from '@angular/core';
import { LayoutService } from './../../services/layout.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CoupleService } from 'src/app/services/couple.service';

@Component({
  selector: 'app-gift-list',
  templateUrl: './gift-list.component.html',
  styleUrls: ['./gift-list.component.scss']
})
export class GiftListComponent implements OnInit {
  couple: ICoupleInformation;
  gifts:any[] = [];
  giftReceived: number = 0;
  total:number;
  @Input() guest: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private coupleService: CoupleService,
  ) { 
    this.couple = this.layoutService.getCoupleInformation();
    this.gifts = this.couple.productsRegistryList;
    this.getTotal();
  }

  ngOnInit() {
  }

  async getTotal() {
    try {
      this.spinner.show();
      const totals = await this.coupleService.getCashoutTotal();
      this.total = totals.total;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  getTotalRecived() {
    this.giftReceived = this.gifts.reduce((total, gift) => {
      return total + gift.received;
    }, 0);
    this.total = this.gifts.filter(items => items.received).reduce((total, gift) => {
      return total + gift.price;
    }, 0);
    console.log(this.total);
  }

}
