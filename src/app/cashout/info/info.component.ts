import { Component, OnInit } from '@angular/core';
import { CoupleInformation } from 'src/app/_models/coupleInformation';
import { LayoutService } from 'src/app/services/layout.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoupleService } from 'src/app/services/couple.service';
import { CatalogsService } from 'src/app/services/catalogs.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  couple: CoupleInformation;
  cashoutForm: FormGroup;
  submit: boolean = false;
  selectedFile: any;
  file: any;
  typeDocument: string = 'INE'
  images: any = [];
  imageFrontend = '';
  errorBackend: boolean = false;
  errorFrontend: boolean = false;
  imageBackend = '';
  total: number = 0;
  totalCashouts: number = 0;
  plan: any;
  isPremium: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private router: Router,
    private formBuilder: FormBuilder,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private catalogsService: CatalogsService
  ) { 
    this.couple = this.layoutService.coupleInformation;
    this.getTotal();
  }

  validClabe(control) {

  }

  deleteImg(event: Event, type: string) { 
    event.stopPropagation();
    event.preventDefault();
    if (type === 'frontend') {
      this.imageFrontend = '';
    } else if (type === 'backend') {
      this.imageBackend = '';
    }
  }

  checkFileSize(fileEvent: any) {
    const file = fileEvent.target.files[0];
    return (file.size/1024/1024)> 5;
  }

  async upload(event, type: string) {
    if (event.target.files && event.target.files[0]) {
      if (this.checkFileSize(event)) {
        this.notification.warning("El tamaño del archivo no debe exceder los 5 MB");
        document.getElementById('new-gift-image').nodeValue = '';
        return
      }
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async(data) => {
          console.log(data);
          this.selectedFile = event.target.files[0];
          this.file = data.target['result'];
          this.uploadFile(type);
      };
    }
  }

  async uploadFile(type) {
    try {
      this.spinner.show();
      const response = await this.coupleService.uploadFilecashOutInformation(
        this.layoutService.getCoupleInformation().id, 
        this.selectedFile
      );
      console.log(type)
      if (type == 'frontend') {
        console.log("pasa");
        this.imageFrontend = response.url;
      } else if (type == 'backend') {
        this.imageBackend = response.url;
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  ngOnInit() {
    this.cashoutForm = this.formBuilder.group({
      name: [
        `${this.couple.info.weddingData.nameP1} ${this.couple.info.weddingData.lastnameP1}`,
        Validators.required
      ],
      bank: ['', Validators.required],
      numberBank: ['', Validators.required],
      amount: [this.total],
      typeDocument: ['INE', Validators.required]
    });    
    this.getPlan();
  }


  getPremium() {
    return !this.isPremium;
  }

  async getPlan() {
    try {
      this.spinner.show();
      const response = await this.coupleService.getPlan(this.layoutService.getCoupleInformation().id);
      const responsePlan = await this.catalogsService.plans();
      this.plan = responsePlan.find(info => {
        return info.id == response.planId;
      });
      const plan = responsePlan.find(info => {
        return String(info.id) === String(response.planId);
      });
      this.isPremium = plan && plan.totalCashouts === 0 ? true :  false;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  onUpload(event: Event, element: string) {
    event.stopPropagation();
    event.preventDefault();
    document.getElementById(element).click();
  }

  async getTotal() {
    try {
      this.spinner.show();
      const totals = await this.coupleService.getCashoutTotal();
      this.total = totals.total;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  get f() { return this.cashoutForm.controls; }

  next() {
    this.submit = true;
    this.images = [];
    console.log(this.cashoutForm.value.typeDocument);
    if (this.cashoutForm.invalid) {
      return ;
    }
    if (isNaN(this.cashoutForm.value.amount)) {
      this.notification.error("El monto no es válido");
      return;
    }
    if (this.cashoutForm.value.amount > this.total) {
      this.notification.error("El monto supera la cantidad acumulada");
      return;
    }
    if (!this.imageFrontend.length) {
      this.errorFrontend = true;
      return ;
    }
    this.images.push(this.imageFrontend);
    if (this.cashoutForm.value.typeDocument == 'INE' && !this.imageBackend.length) {
      this.errorBackend = true;
      console.log("A",this.cashoutForm.value.typeDocument == 'INE', !this.imageBackend.length);
      return ;
      
    } else {
      console.log("B");
      this.images.push(this.imageBackend);
    }

    this.layoutService.cashout.info = {
      bank: this.cashoutForm.value.bank,
      amount: this.cashoutForm.value.amount,
      numberBank: this.cashoutForm.value.numberBank,
      name: this.cashoutForm.value.name,
      images: this.images
    }
    console.log(this.layoutService.cashout.info);
    this.router.navigate(['/retiro/confirmacion'])
  }

}
