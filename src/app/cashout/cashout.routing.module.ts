import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { CongratsComponent } from './congrats/congrats.component';
import { CashoutComponent } from './cashout.component';
import { InfoComponent } from './info/info.component';
import { ConfirmComponent } from './confirm/confirm.component';

const routes: Routes = [{
    path: '',
    component: CashoutComponent,
    children: [{
        path: 'informacion',
        component: InfoComponent
    }, {
        path: 'confirmacion',
        component: ConfirmComponent
    }, {
        path: 'felicidades',
        component: CongratsComponent
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class CashoutRoutingModule { }