import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Cashout } from 'src/app/_models/cashout';
import { ICashout } from 'src/app/_interface/cashout';
import { CoupleService } from 'src/app/services/couple.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  cashout: ICashout
  constructor(
    private layoutService: LayoutService,
    private router: Router,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService
  ) { 
    this.cashout = this.layoutService.cashout.info;
  }

  ngOnInit() {
  }

  async save() {
    try {
      this.spinner.show();
      const cashout = {
        bank: this.cashout.bank,
        target: this.cashout.name,
        requiredAmount: this.cashout.amount,
        clabe: this.cashout.numberBank,
        images: this.cashout.images
      }
      await this.coupleService.cashoutCreate(
        cashout
      );
      this.layoutService.cashout.clear();
      this.router.navigate(['/retiro/felicidades']);
    } catch(e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
