import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashoutComponent } from './cashout.component';
import { InfoComponent } from './info/info.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { CongratsComponent } from './congrats/congrats.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CashoutRoutingModule } from './cashout.routing.module';

@NgModule({
  declarations: [
    CashoutComponent, 
    InfoComponent, 
    ConfirmComponent, 
    CongratsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    CashoutRoutingModule
  ]
})
export class CashoutModule { }
