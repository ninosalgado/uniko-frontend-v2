import { Component, OnInit } from '@angular/core';
import { LayoutService } from './../../services/layout.service';

@Component({
  selector: 'app-guest-gift-table',
  templateUrl: './guest-gift-table.component.html',
  styleUrls: ['./guest-gift-table.component.scss']
})
export class GuestGiftTableComponent implements OnInit {

  articulos:any[] = [];
  constructor(private _layoutService: LayoutService) { 
    this.articulos = this._layoutService.getCurrentUser().user['productsRegistryList'];
    console.log(this.articulos);
  }

  ngOnInit() {
  }

}
