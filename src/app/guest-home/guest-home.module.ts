import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuestHomeRoutingModule } from './guest-home-routing.module';
import { GuestHomeComponent } from './guest-home.component';

import { HeaderGuestComponent } from './../components/header-guest/header-guest.component';
import { DatosEventoComponent } from './datos-evento/datos-evento.component';
import { HistoriaComponent } from './historia/historia.component';
import { HotelesComponent } from './hoteles/hoteles.component';
import { GuestGiftTableComponent } from './guest-gift-table/guest-gift-table.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { GuestFaqsComponent } from './guest-faqs/guest-faqs.component'
import { SharedModule } from '../shared/shared.module';
import { GuestArticulo2Component } from './guest-articulo/guest-articulo.component';

@NgModule({
  declarations: [
    GuestHomeComponent,
    // HeaderGuestComponent,
    DatosEventoComponent,
    HistoriaComponent,
    HotelesComponent,
    GuestGiftTableComponent,
    GaleriaComponent,
    GuestFaqsComponent,
    GuestArticulo2Component
  ],
  imports: [
    CommonModule,
    GuestHomeRoutingModule,
    SharedModule
  ]
})
export class GuestHomeModule { }
