import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-guest-articulo2',
  templateUrl: './guest-articulo.component.html',
  styleUrls: ['./guest-articulo.component.scss']
})
export class GuestArticulo2Component implements OnInit {
  @Input() index:number;
  @Input() art:any = {};


  constructor(private router:Router) { 

  }

  ngOnInit() {
  }
  acomodoArt(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4 ArtSelec";
  }
  acomodoArt2(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4";
  }
  editarArt(){
    //this.router.navigate(['/gift-table/edit',this.index]);
  }
}