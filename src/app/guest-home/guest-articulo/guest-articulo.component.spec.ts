import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestArticulo2Component } from './guest-articulo.component';

describe('GuestArticulo2Component', () => {
  let component: GuestArticulo2Component;
  let fixture: ComponentFixture<GuestArticulo2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestArticulo2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestArticulo2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
