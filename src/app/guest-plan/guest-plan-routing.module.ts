import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuestPlanComponent } from './guest-plan.component'

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: '/search'
    },
    {
      path: ':nameId',
      component: GuestPlanComponent
    }
  ]  
}];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestPlanRoutingModule { }
