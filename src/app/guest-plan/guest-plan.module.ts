import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuestPlanRoutingModule } from './guest-plan-routing.module';
import { GuestPlanComponent } from './guest-plan.component';

import { SharedModule } from '../shared/shared.module';
import { HeaderGuestComponent } from './../components/header-guest/header-guest.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { Ng5SliderModule } from 'ng5-slider';



@NgModule({
  declarations: [GuestPlanComponent],
  imports: [
    CommonModule,
    GuestPlanRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    Ng5SliderModule
  ]
})
export class GuestPlanModule { }
