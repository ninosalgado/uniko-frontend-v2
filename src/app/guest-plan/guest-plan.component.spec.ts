import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestPlanComponent } from './guest-plan.component';

describe('GuestPlanComponent', () => {
  let component: GuestPlanComponent;
  let fixture: ComponentFixture<GuestPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
