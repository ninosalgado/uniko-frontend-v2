import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PregSolicitudComponent } from './preg-solicitud.component';

describe('PregSolicitudComponent', () => {
  let component: PregSolicitudComponent;
  let fixture: ComponentFixture<PregSolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PregSolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PregSolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
