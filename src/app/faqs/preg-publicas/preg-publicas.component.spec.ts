import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PregPublicasComponent } from './preg-publicas.component';

describe('PregPublicasComponent', () => {
  let component: PregPublicasComponent;
  let fixture: ComponentFixture<PregPublicasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PregPublicasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PregPublicasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
