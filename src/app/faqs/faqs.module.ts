import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';
import { MatDialogModule } from '@angular/material/dialog';

import { FaqsRoutingModule } from './faqs-routing.module';
import { FaqsComponent } from './faqs.component';
import { PregPublicasComponent } from './preg-publicas/preg-publicas.component';
import { PregSolicitudComponent } from './preg-solicitud/preg-solicitud.component';

import { DialogBorraPublicas } from './preg-publicas/preg-publicas.component';
import { DialogBorraSolicitud } from './preg-solicitud/preg-solicitud.component';

@NgModule({
  declarations: [
    FaqsComponent,
    PregPublicasComponent,
    PregSolicitudComponent,
    DialogBorraPublicas,
    DialogBorraSolicitud
  ],
  imports: [
    CommonModule,
    FaqsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MatDialogModule,
    SharedModule
  ],
  entryComponents: [
    DialogBorraPublicas,
    DialogBorraSolicitud
  ]
})
export class FaqsModule { }
