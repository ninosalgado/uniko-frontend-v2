import { Component, OnInit } from '@angular/core';
import { Button } from '../_models/data';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationsService } from '../services/notifications.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FrequentQuestionService } from '../services/frequent-question.service';
import { LayoutService } from '../services/layout.service';
import { IFrequencyQuestions } from '../_interface/frequencyQuestion';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent implements OnInit {
  buttons: Array<Button> = [];
  isOpen: boolean = false;
  dataForm: FormGroup;
  submit: boolean = false;
  faqs: IFrequencyQuestions[] = [];
  faqsPublics: IFrequencyQuestions[] = [];
  faqsSolicited: IFrequencyQuestions[] = [];
  isPublic: boolean = true;
  selectedFile: any;
  file: any;
  eventType: string = 'WEDDING';
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private frequentQuestionService: FrequentQuestionService,
    private layoutService: LayoutService,

  ) {
    this.get();
    this.onButtons();
  }

  ngOnInit() {
    this.onForm();
  }

  onUpload(event: Event, element: string) {
    event.stopPropagation();
    event.preventDefault();
    document.getElementById(element).click();
  }

  checkFileSize(fileEvent: any) {
    const file = fileEvent.target.files[0];
    return (file.size / 1024 / 1024) > 5;
  }

  async upload(event, type: string) {
    if (event.target.files && event.target.files[0]) {
      if (this.checkFileSize(event)) {
        this.notification.warning("El tamaño del archivo no debe exceder los 5 MB");
        document.getElementById('new-gift-image').nodeValue = '';
        return
      }
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async (data) => {
        console.log(data);
        this.selectedFile = event.target.files[0];
        this.file = data.target['result'];
      };
    }
  }

  onButtons() {
    this.buttons = [];
    const buttonPreview = new Button(
      'Agregar pregunta',
      this.add,
      'fa-plus',
      false,
      this
    );
    this.buttons.push(buttonPreview);
  }

  onForm() {
    this.dataForm = this.formBuilder.group({
      question: ['', Validators.required],
      answer: ['', Validators.required]
    });
  }

  add(event: Event): void {
    event.stopPropagation();
    event.preventDefault();
    this['parent'].isOpen = true;
  }

  closeModal() {
    this.isOpen = false;
  }

  async get(): Promise<void> {
    try {
      this.spinner.show();
      this.faqs = await this.frequentQuestionService.getBycouple(this.layoutService.getCoupleInformation().id);
      this.eventType = this.layoutService.getCoupleInformation().weddingData.type ? this.layoutService.getCoupleInformation().weddingData.type : this.eventType;
      this.faqsPublics = this.faqs.filter(data => data.public);
      this.faqsSolicited = this.faqs.filter(data => !data.public);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  getTypeEventString() {
    let type;
    switch (this.eventType) {
      case 'WEDDING':
        type = 'respecto a nuestra Boda'
        break;
      case 'BABY':
        type = 'para el baby shower'
        break;
      case 'BIRTHDAY':
        type = 'respecto a mi Cumpleaños'
        break;
    }
    return type;
  }

  async save(): Promise<void> {
    try {
      this.submit = true;
      this.spinner.show();
      let image;
      if (this.dataForm.invalid) {
        return;
      }
      if (this.selectedFile) {
        image = await this.frequentQuestionService.uploadImageFrequentQuestion(
          this.layoutService.getCoupleInformation().id,
          this.selectedFile
        );
      }
      await this.frequentQuestionService.generateFrequentQuestion(
        this.dataForm.value.question,
        this.dataForm.value.answer,
        image
      );
      this.isOpen = false;
      this.dataForm.reset();
      this.file = null;
      this.get();
    } catch (e) {
      this.notification.error(e)
    } finally {
      this.spinner.hide();
    }
  }

}
