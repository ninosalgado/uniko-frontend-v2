export interface ICashout {
    bank: string;
    numberBank: string;
    amount: number;
    name: string;
    images?: [];
}

export interface ICredit {    
    cardNumber: string;
    amount: number;
    msi?: number;
    fullname?: string;
    token?: string;
    type?: string;
    comission?: any;
}