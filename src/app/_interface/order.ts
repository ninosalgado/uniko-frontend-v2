import { IProductCart } from './cart';

export interface IMessageType {
    text: string;
}

export interface IMessageInfo {
    emails: IMessageType[];
    givers: IMessageType[];
    message: string;
    names?: string[];
    prefijo?: string;
    tel?: string;
}

export interface IPaymentInfo {
    address: any;
    msi?: any,
    birthdate: any;
    message: IMessageInfo;
    pay: any;
    payer: any;
    products: string[];
    urlRegistry: string;
    method?: string;
    otro?: any;
    first6?: string;
}

export interface IPaymentData {
    address: any;
    birthdate: any;
    message: IMessageInfo;
    pay: any;
    payer: any;
    products: IProductCart[];
    urlRegistry: string;
    amount: number
}


export interface IOrder {
    fakeid: string;
    paymentData: IPaymentData;
    adminUpdated?: any[];
    isPaid: boolean;
    isCashout: boolean;
    isGiftAdmin: boolean;
    comission: number;
    commisionMsi: number;
    receivedManualEmail: boolean;
    status: string;
    sendEmailPending: boolean;
    id: string;
    coupleAccountId: string;
    created: Date;
    modified: Date;
    recentlyUpdated: boolean;
    paymentMethod?: any;
    bonnus?: any;
}