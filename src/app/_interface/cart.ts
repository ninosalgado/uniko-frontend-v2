import { Products } from '../_models/data';
import { IProductRegister } from '../gift-table/general-item-cards/general-item-cards.component';

export interface IProductCart {
    name: string;
    description: string;
    sku: string;
    unit_price: number;
    price?: number;
    quantity: number;
    id?: string;
    image?: string
}

export interface ICart {
    total?: number,
    commission?: number;
    commissionMsi?: number;
    type?: string;
    products: any[],
    product: any
}