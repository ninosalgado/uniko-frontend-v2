export interface IPlan {
    nameCard?: string;
    token?: string;
    id?: string
    lastCard?: string;
    name?: string;
    date?: Date;
    conektaPlanId?: string;
    amount?: number;
    type?: '',
    method?: string,
    msi?: string
}