export interface ITemplate {
    colors: string,
    fontFamily: string,
    fontWeight: string,
    textAlign: string,
    fontSize: string,
    textTransform: string,
    fontStyle: string,
    text: string,
    latterSpace: string,
}

export interface ITemplateColor {
    index: number;
    color: string;
    select: boolean;
}

export interface ISaveDate {
    desktop: boolean;
    template: string;
    templateId: number;
    opacity: string;
    title: ITemplate;
    nametitle: ITemplate;
    titleDate: ITemplate;
    date: ITemplate;
    titleDescription: ITemplate;
    description: ITemplate;
    backgroundTemplate: ITemplateColor;
    colors: ITemplateColor;
    coupleAccountId: string;
    id?: string,
    cover?: string
}