export interface IFrequencyQuestions {
    answer?: string;
    question?: string;
    public?: boolean;
    isCouple: boolean;
    coupleAccountId?: string;
    id?:string;
    image?: string;
}