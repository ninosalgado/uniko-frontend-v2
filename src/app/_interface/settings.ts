export interface ISettings {
    currency: string;
    currencyChange?: number;
    country?: string;
    language?: string;
}