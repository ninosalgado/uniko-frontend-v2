
export interface IWeddingData {
  nameP1: string;
  sanitizedNameP1: string;
  lastnameP1: string;
  sanitizedLastnameP1: string;
  nameP2: string;
  sanitizedNameP2: string;
  lastnameP2: string;
  sanitizedLastnameP2: string;
  date: Date;
  dateText: string;
  requireConfirmation: boolean;
  welcomeMessage: string;
  coverPhoto: string;
  profilePhoto?: string;
  placeName?: string;
  address?: string;
  gpsaddress?: any;
  city?: string;
  isSetted: boolean;
  id?: string;
  titleMessage?: string;
  titleHistory?: string;
  messageHistory?: string;
  detailHistory?: string;
  type?: string;
}

export interface ICoupleInformation {
  url: string;
  currentUrl?: string;
  experience: boolean;
  email2?: string;
  phoneNumber1: string;
  phoneNumber2?: string;
  hasFirstProduct: boolean;
  createEmailReceived: boolean;
  paymentEmailReceived: boolean;
  requireOnboarding: boolean;
  tempAccount: boolean;
  isDisabled: boolean;
  paymentCommision?: boolean;
  isSendAccountBalance: boolean;
  rsvpDisabled: boolean;
  trackQuantity: boolean;
  limitQuantity: boolean;
  is2FA: boolean;
  prefijo: string;
  isAnswerQuestions: boolean;
  onboarding?: number;
  isActive: boolean;
  email: string;
  id: string;
  createdAt: Date;
  updatedAt: Date;
  fakeid: string;
  token2FA: string;
  totalGift?: number;
  totalGiftReceived?: number;
  domine: string;
  weddingData: IWeddingData;
  payInformationData?: any;
  cashOutInformationList: any[];
  registryData: any;
  giftList: any[];
  productsRegistryList: any[];
  totalCashout?: number;
  totalReceived?: number;
  onboardingStep?: number;
  stateId?: string;
  ticketsStatus: string;
  ticketsCant: number;
  shippingAddress: any;
  viewGifts?: boolean
  viewHelp?: boolean
  viewHistory?: boolean
  viewHotel?: boolean
  viewInstagram?: boolean
  viewTime?: boolean
  theme?: string
}

export interface instagramInfo {
  images: string[],
  hashtag: string
}