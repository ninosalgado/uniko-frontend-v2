export interface IListAggre {
    id: string;
    isMenu: boolean;
    keyMenu: string;
    name: string
}

export interface IPlanAggre {
    id: string;
    isActive: boolean;
    listAggre: IListAggre,
    listAggreId?: string;
    planId?: string;
}

export interface IAuthorization {
    amount?: number;
    commisions?: number;
    currency?: string;
    expiry?: string;
    frequency?: string;
    id?: string;
    interval?: string;
    isBack?: boolean;
    isBasic?: boolean;
    isPremium?: boolean;
    isSuscription?: boolean;
    name?: string;
    nameId?: string;
    planAggre: IPlanAggre[],
    totalCashouts?: number;
    trial?: number;
}