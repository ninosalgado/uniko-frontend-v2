export interface ISura {
    email: string;
    paid: boolean;
    customerId: string;
    ab: string;
    planId: string;
}