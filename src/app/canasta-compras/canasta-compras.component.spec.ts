import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanastaComprasComponent } from './canasta-compras.component';

describe('CanastaComprasComponent', () => {
  let component: CanastaComprasComponent;
  let fixture: ComponentFixture<CanastaComprasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanastaComprasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanastaComprasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
