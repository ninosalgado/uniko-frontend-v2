import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanastaComprasComponent } from './canasta-compras.component';

const routes: Routes = [{
  path: '',
  component: CanastaComprasComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CanastaComprasRoutingModule { }
