import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-canasta-compras',
  templateUrl: './canasta-compras.component.html',
  styleUrls: ['./canasta-compras.component.scss']
})
export class CanastaComprasComponent implements OnInit {

  @ViewChild('retiraCred1') retiraCred1: ElementRef;
  @ViewChild('retiraCred2') retiraCred2: ElementRef;
  @ViewChild('retiraCred3') retiraCred3: ElementRef;
  @ViewChild('retiraCred4') retiraCred4: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  muestConten(elemMostrar){
    if(elemMostrar=="paso0"){
      this.retiraCred1.nativeElement.hidden = false;
      this.retiraCred2.nativeElement.hidden = true;
      this.retiraCred3.nativeElement.hidden = true;
      this.retiraCred4.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso1"){
      this.retiraCred1.nativeElement.hidden = true;
      this.retiraCred2.nativeElement.hidden = false;
      this.retiraCred3.nativeElement.hidden = true;
      this.retiraCred4.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso2"){
      this.retiraCred1.nativeElement.hidden = true;
      this.retiraCred2.nativeElement.hidden = true;
      this.retiraCred3.nativeElement.hidden = false;
      this.retiraCred4.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso3"){
      this.retiraCred1.nativeElement.hidden = true;
      this.retiraCred2.nativeElement.hidden = true;
      this.retiraCred3.nativeElement.hidden = true;
      this.retiraCred4.nativeElement.hidden = false;
    }
    else if(elemMostrar=="paso4"){
      alert("Ya terminó")
    }
  }
}
