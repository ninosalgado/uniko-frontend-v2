import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';

import { CanastaComprasRoutingModule } from './canasta-compras-routing.module';
import { CanastaComprasComponent } from './canasta-compras.component';

@NgModule({
  declarations: [CanastaComprasComponent],
  imports: [
    CommonModule,
    CanastaComprasRoutingModule,
    FormsModule,
    MatInputModule,
    MatRadioModule
  ]
})
export class CanastaComprasModule { }
