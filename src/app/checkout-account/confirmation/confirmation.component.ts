import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { LayoutService } from 'src/app/services/layout.service';
import { IPlan } from 'src/app/_interface/plan';
import { OrderService } from 'src/app/services/order.service';
import { CoupleService } from 'src/app/services/couple.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
  plan: IPlan;
  _plan: any;
  constructor(
    private router: Router,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private catalogsService: CatalogsService,
    private layoutService: LayoutService,
    private orderService: OrderService,
    private coupleService: CoupleService,
    private route: ActivatedRoute
  ) { 
    this.plan = this.layoutService.plan.info;
    const token = this.route.snapshot.queryParams.token;
    const payerID = this.route.snapshot.queryParams.PayerID;
    const paymentId = this.route.snapshot.queryParams.paymentId;
    console.log(token, payerID, paymentId);
    if (token && payerID && paymentId) {
      this.getConfirm(token, payerID, paymentId);
    } else {
      this.getPlans();
    }
    
  }

  ngOnInit() {
  }

  async getConfirm(token: string, payerID: string, paymentId: string) {
    try {
      this.spinner.show();
      const plans =  await this.catalogsService.plans();
      this._plan = plans.find(plan => 
        plan.id === this.layoutService.plan.info.id
      );
      const response = await this.orderService.confirmPaypalPaymentAccount(
        this.layoutService.coupleInformation.info.id,
        paymentId,
        token,
        payerID
      );
      this.go();
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }



  save() {
      if (this._plan.isSuscription) {
        this.suscription();
      } else {
        this.payment();
      }
  }

  async getPlans() {
    try {
      this.spinner.show();
      const plans =  await this.catalogsService.plans();
      this._plan = plans.find(plan => 
        plan.id === this.layoutService.plan.info.id
      );
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async suscription() {
    try {
      this.spinner.show();
      const data = await this.orderService.makeCardPaymentPlan(
        this.layoutService.getCoupleInformation().id,
        {
          name: this.layoutService.plan.info.nameCard,
          token: this.layoutService.plan.info.token,
          planId: this.layoutService.plan.info.conektaPlanId,
          lastCard: this.layoutService.plan.info.lastCard
        }
      );
      this.go();
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async payment() {
    try {
      this.spinner.show();
      const couple = this.layoutService.coupleInformation.info;
      const payment = {
        pay: this.layoutService.plan.info.token,
        planId: this.layoutService.plan.info.id,
        products: [],
        payer: {
          fullname: this.layoutService.plan.info.nameCard,
          phone: couple.phoneNumber1,
          email: couple.email
        },
        otro: {
          pay_type: this.layoutService.plan.info.type
        },
        method: this.plan.method === 'paypal' ? 'paypal' : 'cc',
        address: {},
        msi: this.layoutService.plan.info.msi,
        birthdate: {}
      }
      if (this.plan.method === 'paypal') {
        const response = await this.orderService.makePaypalPaymentAccount(
          couple.id,
          payment
        );
        console.log(response);
        window.location.href = response.href;
      } else {
        const response = await this.orderService.makeCardPaymentAccount(
          couple.id,
          payment
        );
        this.go();
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async go() {
    try {
      this.spinner.show();
      const couple = await this.coupleService.get(this.layoutService.getCoupleInformation().id);
      this.layoutService.setCoupleInformation(couple);
      this.router.navigate(['/pago/felicidades'])
    } catch(e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();

    }
  }

}
