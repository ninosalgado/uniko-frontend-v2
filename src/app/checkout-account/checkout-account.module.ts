import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckoutAccountComponent } from './checkout-account.component';
import { PaymentComponent } from './payment/payment.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { CongratsComponent } from './congrats/congrats.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckoutAccountRoutingModule } from './checkout-account.routing.module';
import { MaterialModule } from '../material.module';


@NgModule({
  declarations: [
    CheckoutAccountComponent, 
    PaymentComponent, 
    ConfirmationComponent, 
    CongratsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    CheckoutAccountRoutingModule,
    MaterialModule
  ]
})
export class CheckoutAccountModule { }
