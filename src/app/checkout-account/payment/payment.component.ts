import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Spinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { LayoutService } from 'src/app/services/layout.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Token } from '@angular/compiler';
import { IPlan } from 'src/app/_interface/plan';
import { KEYS } from 'src/environments/environment';
import { CoupleService } from 'src/app/services/couple.service';
import { OrderService } from 'src/app/services/order.service';
declare var Conekta: any;
declare var $: any;
declare var PAYPAL: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  planForm: FormGroup;
  submit: boolean = false;
  paypalPlus: any;
  paypalId: string = '';
  years: number[] = [];
  months: string[] = [];
  method: string = 'card'
  plan: any;
  planId: string;
  code: any;
  type: any = {
    type: '',
    card: 16,
    cvc: 3
  }
  types: any = [{
    type: 'visa',
    card: 16,
    cvc: 3
  }, {
    type: 'mastercard',
    card: 16,
    cvc: 3
  }, {
    type: 'amex',
    card: 15,
    cvc: 4
  }]
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private catalogsService: CatalogsService,
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private orderService: OrderService
  ) {
    const year = new Date().getFullYear();
    this.years = Array.from({ length: 11 }, (v, i) => year + i)
    this.months = Array.from({ length: 12 }, (v, i) => String(i + 1));
    this.planId = this.route.snapshot.params.planId;
    this.getPlans();
    Conekta.setPublicKey(KEYS.CONEKTA_ACCOUNT);
  }

  ngOnInit() {
    this.planForm = this.formBuilder.group({
      name: ['', Validators.required],
      numberCard: ['', Validators.required],
      cvc: ['', Validators.required],
      year: [new Date().getFullYear(), Validators.required],
      month: [String(new Date().getMonth() + 1), Validators.required],
      code: [''],
      msi: ['']
    })
  }

  changeMethod(type: string) {
    this.method = type;
    if (this.method === 'paypal-plus') {
      this.loadPaypalPlus();
    }
  }

  async loadPaypalPlus() {
    try {
      this.spinner.show();
      const couple = this.layoutService.coupleInformation.info;
      const payment = {
        pay: '',
        planId: this.planId,
        products: [],
        payer: {
          fullname: `${couple.weddingData.nameP1} ${couple.weddingData.lastnameP1}`,
          phone: couple.phoneNumber1,
          email: couple.email
        },
        otro: {
          pay_type: this.plan.type
        },
        method: 'paypal',
        address: {},
        msi: 1,
        birthdate: {}
      }
      const paymentResponse = await this.orderService.makePaypalPaymentAccount(
        this.layoutService.coupleInformation.info.id,
        payment
      );
      this.load(paymentResponse);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  load(info: any) {
    const couple = this.layoutService.coupleInformation.info;
    this.paypalId = info.id;
    this.paypalPlus = PAYPAL.apps.PPP({
      approvalUrl: info.href,
      buttonLocation: "outside",
      preselection: "none",
      surcharging: false,
      hideAmount: false,
      placeholder: "paypal-plus",
      disableContinue: "continueButton",
      enableContinue: "continueButton",
      onContinue: (rememberedCards, payerId, token, term) => {
        console.log(rememberedCards, payerId, token, term);
        this.savePlus(token, payerId);
      },
      onError: (err) => {
        this.spinner.hide();
      },
      language: "es_MX",
      country: "MX",
      disallowRememberedCards: true,
      rememberedCards: true,
      mode: KEYS.paypalLive,
      useraction: "continue",
      payerEmail: couple.email,
      payerPhone: couple.phoneNumber1,
      payerFirstName: 'anonimo',
      payerLastName: 'anonimo',
      payerTaxId: "",
      payerTaxIdType: "",
      merchantInstallmentSelection: 1,
      merchantInstallmentSelectionOptional: 1,
      hideMxDebitCards: true,
      iframeHeight: 500
    });
  }

  async getCode() {
    try {
      this.spinner.show();
      const code = await this.orderService.getCode(
        this.layoutService.coupleInformation.info.id
      );
      if (code) {
        this.planForm.get('msi').disable();
        this.planForm.patchValue({
          msi: ''
        });
        if (code.status === 'APPROVED') {
          this.planForm.patchValue({
            'code': code.code
          });
          this.planForm.get('code').disable();
          if (code.discountCode.type === 'percent') {
            this.plan.amount = this.plan.amount - (this.plan.amount * (code.discountCode.amount / 100));
          } else {
            this.plan.amount = this.plan.amount - code.discountCode.amount;
          }
        }
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async validateCode() {
    try {
      this.spinner.show();
      const code = await this.orderService.applyCode(
        this.layoutService.coupleInformation.info.id,
        this.planForm.value.code
      )
      if (this.method === 'paypal-plus') {
        this.loadPaypalPlus();
      }
      await this.getCode();
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  validCard() {
    const first = this.planForm.value.numberCard.slice(0, 1);
    switch (first) {
      case '4':
        this.type = this.types.find(data => data.type == 'visa');
        break;
      case '3':
        this.type = this.types.find(data => data.type == 'amex');
        break;
      case '5':
        this.type = this.types.find(data => data.type == 'mastercard');
        break;
      default:
    }
  }

  async getPlans() {
    try {
      this.spinner.show();
      const plans = await this.catalogsService.plans();
      this.plan = plans.find(plan => plan.conektaPlanId === this.planId || plan.id === this.planId);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
      if (this.plan && !this.plan.isSuscription && !this.plan.isBasic) {
        this.getCode();
      }
    }
  }

  async next() {
    try {
      this.spinner.show();
      this.submit = true;
      if (this.planForm.invalid && this.method === 'card') {
        return;
      }
      switch (this.method) {
        case 'card':
          this.saveCard();
          break;
        case 'paypal':
          this.save();
          break;
        case 'paypal-plus':
          const couple = this.layoutService.coupleInformation.info;
          const payment = {
            pay: this.layoutService.plan.info.token,
            planId: this.layoutService.plan.info.id,
            products: [],
            payer: {
              fullname: this.layoutService.plan.info.nameCard,
              phone: couple.phoneNumber1,
              email: couple.email
            },
            otro: {
              pay_type: this.layoutService.plan.info.type
            },
            method: 'paypal',
            address: {},
            msi: 1,
            birthdate: {}
          }
          const paymentResponse = await this.orderService.makeCardPaymentAccount(
            this.layoutService.coupleInformation.info.id,
            payment
          );
          this.load(payment);
          break;
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  savePlus(token: string, PayerID: string) {
    const plan: IPlan = {
      nameCard: this.planForm.value.name,
      lastCard: ``,
      token: '',
      conektaPlanId: this.plan.conektaPlanId,
      id: this.plan.id,
      name: this.plan.name,
      amount: this.plan.amount,
      date: new Date(),
      type: this.type.type,
      method: 'paypal'
    }
    this.layoutService.plan.info = plan;
    this.router.navigate(['/pago', 'confirmacion'], {
      queryParams: {
        token,
        PayerID,
        paymentId: this.paypalId
      }
    });
  }

  async save() {
    try {
      this.spinner.show();
      const plan: IPlan = {
        nameCard: this.planForm.value.name,
        lastCard: ``,
        token: '',
        conektaPlanId: this.plan.conektaPlanId,
        id: this.plan.id,
        name: this.plan.name,
        amount: this.plan.amount,
        date: new Date(),
        type: this.type.type,
        method: this.method
      }
      this.layoutService.plan.info = plan;
      const planCouple = await this.coupleService.getPlanPermisions(this.layoutService.coupleInformation.info.id);
      this.layoutService.authorization.info = planCouple;
      this.router.navigate(['/pago', 'confirmacion']);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async saveCard() {
    try {
      this.spinner.show();
      const token = {
        card: {
          number: this.planForm.value.numberCard,
          name: this.planForm.value.name,
          exp_year: this.planForm.value.year,
          exp_month: this.planForm.value.month,
          cvc: this.planForm.value.cvc
        }
      }
      const response = await this.createToken(token);
      const plan: IPlan = {
        nameCard: this.planForm.value.name,
        lastCard: `${this.type.type == 16 ? '**** **** ****' : '**** ******'} ${this.planForm.value.numberCard.slice(this.type.type == 16 ? 12 : 10, this.type.type == 16 ? 16 : 15)}`,
        token: response.id,
        conektaPlanId: this.plan.conektaPlanId,
        id: this.plan.id,
        name: this.plan.name,
        amount: this.plan.amount,
        date: new Date(),
        type: this.type.type,
        method: this.method,
        msi: this.planForm.value.msi
      }
      this.layoutService.plan.info = plan;
      const planCouple = await this.coupleService.getPlanPermisions(this.layoutService.coupleInformation.info.id);
      this.layoutService.authorization.info = planCouple;
      this.router.navigate(['/pago', 'confirmacion']);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async createToken(card): Promise<any> {
    return new Promise((resolve, reject) => {
      Conekta.token.create(card, (token: Token) => {
        return resolve(token);
      }, (error) => {
        reject(error.message_to_purchaser);
      })
    });
  }

}
