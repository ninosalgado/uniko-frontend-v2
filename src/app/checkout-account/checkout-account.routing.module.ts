import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { CheckoutAccountComponent } from './checkout-account.component';
import { PaymentComponent } from './payment/payment.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { CongratsComponent } from './congrats/congrats.component';

const routes: Routes = [{
    path: '',
    component: CheckoutAccountComponent,
    children: [{
        path: 'plan/:planId',
        component: PaymentComponent
    }, {
        path: 'confirmacion',
        component: ConfirmationComponent
    }, {
        path: 'felicidades',
        component: CongratsComponent
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class CheckoutAccountRoutingModule { }