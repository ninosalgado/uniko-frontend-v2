import { Component, OnInit, Input, Inject } from '@angular/core';
import { Router } from '@angular/router';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CatalogsService } from 'src/app/services/catalogs.service';

@Component({
  selector: 'app-articulo-tarjeta',
  templateUrl: './articulo-tarjeta.component.html',
  styleUrls: ['./articulo-tarjeta.component.scss']
})

export class ArticuloTarjetaComponent implements OnInit {
  @Input() lock:boolean;
  @Input() index:number;
  @Input() art:any = {};
  @Input() store: string = '';
  @Input() category: string = '';
  @Input() isVisible:boolean;
  constructor(private router:Router, public dialog: MatDialog,
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private catalogService: CatalogsService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService
    ) { 


  }

  ngOnInit() {
    
  }
  acomodoArt(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4 ArtSelec";
  }
  acomodoArt2(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4";
  }
  borrarArt(idArt:string){
    //alert("Eliminar artículo: " + idArt);
    const dialogRef = this.dialog.open(DialogAlertaBorrar, { data: idArt });
 
   dialogRef.afterClosed().subscribe(result => {
     if(result==true){             
        this.commitRemoval(idArt);
        console.log("Guardar");
     }
     else{
       console.log("Cancelar");
     }
   });
  }
  async commitRemoval(idArt:string): Promise<void> {
    try {
      this.spinner.show();
      const couple = this.layoutService.getCoupleInformation();
      let _couple = await this.coupleService.get(couple.id);
      _couple.productsRegistryList = couple.productsRegistryList.filter(function( obj ) {
          return obj.id != idArt;
      });
      const update = await this.coupleService.setProductsList(_couple.id, _couple.experience, _couple.productsRegistryList); 
      this.layoutService.setCoupleInformation(_couple);
      this.catalogService.catalogChanges();
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }
  editarArt(id){
    this.router.navigate(['/gift-table/edit',id]);
  }
}

@Component({
  selector: 'alerta-borrar-dialog',
  templateUrl: './alerta-borrar-dialog.html',
  styleUrls: ['./articulo-tarjeta.component.scss']
})
export class DialogAlertaBorrar {
  constructor(public matDialogRef: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: any) {

  }
}