import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticuloTarjetaComponent } from './articulo-tarjeta.component';

describe('ArticuloTarjetaComponent', () => {
  let component: ArticuloTarjetaComponent;
  let fixture: ComponentFixture<ArticuloTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticuloTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticuloTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
