import { Component, OnInit, Inject, Output, EventEmitter, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LayoutService } from '../services/layout.service';
import { Button, IProductRegister } from '../_models/data';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatIcon } from '@angular/material';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from '../services/notifications.service';
import { CatalogsService } from '../services/catalogs.service';
import { ICoupleInformation } from '../_interface/coupleInformation';
import { UserService } from '../services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { load } from '@angular/core/src/render3';

@Component({
  selector: 'app-gift-table',
  templateUrl: './gift-table.component.html',
  styleUrls: ['./gift-table.component.scss']
})
export class GiftTableComponent implements OnInit {
  couple: ICoupleInformation;
  buttons: Array<Button> = [];
  isActive: boolean = false;
  @Input() sharedCouple: ICoupleInformation | any;
  @Output() broadcastCouple = new EventEmitter<ICoupleInformation>();
  isOpen: boolean = false;
  isOpenDialog: boolean = false;
  newItem: IProductRegister;
  categories: any[] = [];
  stores: any[] = [];
  images: any[] = [];
  imagesFiles: any[] = [];
  category: any;
  loading = false;
  newItemForm: FormGroup;
  announce = false;
  loadLock: boolean = false;
  @ViewChild('newGiftImage') inputImageField: ElementRef;

  constructor(
    public router: Router,
    private layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private coupleService: CoupleService,
    private catalogService: CatalogsService,
    private userService: UserService,
    private formBuilder: FormBuilder,
  ) {
    this.couple = this.layoutService.getCoupleInformation();
    this.setButtons();
    this.resetItem();
  }


  ngOnInit() {
    this.makeForm();
  }

  async onComissions() {
    console.log("HERE");
    await this.coupleService.onComissions(this.couple.id);
    await this.getCouple();
  }

  async onActiveComission() {
    try {
      this.spinner.show();
      if (!this.loadLock) {
        await this.onComissions();
      }
      this.loadLock = true;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.loadLock = false;
      this.spinner.hide();
    }
  }

  onClose() {
    this.isOpen = false;
    this.isOpenDialog = false;
    this.closeAnnounce();
  }

  closeAnnounce() {
    this.announce = false;
  }

  resetItem() {
    if (this.newItemForm) {
      this.newItemForm.reset();
    }
    this.images = [];
    this.imagesFiles = [];
    this.categories = [];
    this.stores = [];
    this.newItem = {
      id: '',
      categoriesIds: [],
      createByAccount: true,
      description: '',
      image: '',
      imagesList: [],
      name: '',
      price: null,
      qty: 1,
      storeId: '',
      isActive: true,
    };
  }

  setButtons(inAdd?: boolean) {
    try {
      let addGifts;
      if (inAdd) {
        addGifts = new Button(
          'Mesa de regalos',
          this.goToTable,
          'fa-list',
          null,
          this
        );
      } else {
        addGifts = new Button(
          'Agregar regalos',
          this.goToTableAdd,
          'fa-cart-plus',
          null,
          this
        );
      }

      const buttonMakeGift = new Button(
        'Crear Regalo',
        this.openDialog,
        'fa-gift'
      );
      const buttonComissions = new Button(
        'Comisiones',
        this.openDialogComissions,
        'fa-gift',
        null,
        this
      );
      this.buttons = [addGifts, buttonMakeGift, buttonComissions];
    } catch (e) {
      console.log(e);
    }
  }

  openDialogComissions(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this['parent'].isOpenDialog = true;
  }

  goToTable(e) {
    e.preventDefault();
    e.stopPropagation();
    this['parent'].setButtons();
    this['parent'].router.navigate(['/', 'gift-table']);
  }

  goToTableAdd(e) {
    e.preventDefault();
    e.stopPropagation();
    this['parent'].setButtons(true);
    this['parent'].router.navigate(['/', 'gift-table', 'add']);
  }

  guestManage(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }

  lock(event: Event) {

  }

  openDialog(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    document.getElementById('fakeButtonMake').click();
  }

  async makeDialog() {
    this.spinner.show();
    this.resetItem();
    await this.catalogs();
    this.isOpen = true;
    this.spinner.hide();
  }

  async getCouple(): Promise<void> {
    try {
      this.spinner.show();
      const couple = await this.coupleService.get(this.couple.id);
      this.couple = couple;
      this.layoutService.setCoupleInformation(this.couple);
    } catch (e) {
      this.notification.error(e)
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }

  onActive(active) {

  }

  makeForm() {
    this.newItemForm = this.formBuilder.group({
      name: [this.newItem.name, Validators.required],
      price: [this.newItem.price, [Validators.required, Validators.min(1)]],
      description: [this.newItem.description, Validators.required],
      storeId: [this.newItem.storeId, Validators.required],
      category: [this.newItem.categoriesIds, Validators.required]
    })
  }

  async catalogs(): Promise<void> {
    try {
      this.categories = await this.coupleService.getCategories();
      this.stores = await this.coupleService.getStores();
    } catch (e) {
      console.log(e);
    } finally {
    }
  }

  quitarFoto(index: number) {
    this.imagesFiles.splice(index, 1);
    this.images.splice(index, 1);
  }

  agregarFoto(e) {
    e.preventDefault();
    e.stopPropagation();
    document.getElementById('new-gift-image').click();
  }

  fileProgress(fileInput: any) {
    if (this.checkFileSize(fileInput)) {
      this.notification.warning("El tamaño del archivo no debe exceder los 5 MB");
      this.inputImageField.nativeElement.value = null;
      return
    }
    const fileData = <File>fileInput.target.files[0];
    this.readThis(fileData);
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(file: any): void {
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.images.push(myReader.result);
      this.imagesFiles.push(file);
    }
    myReader.readAsDataURL(file);
    this.inputImageField.nativeElement.value = null;
  }

  async uploadPicture(id: string, file: any): Promise<any> {
    return await this.coupleService.uploadImage(id, file);
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  checkFileSize(fileEvent: any) {
    const file = fileEvent.target.files[0];
    return (file.size / 1024 / 1024) > 5;
  }

  countImages() {
    if (this.images) {
      return 4 > this.images.length ? true : false;
    } else {
      return true;
    }
  }

  openAnnounce() {
    this.markFormGroupTouched(this.newItemForm);
    if (this.newItemForm.invalid) {
      this.notification.warning('Los campos marcados con * son requeridos');
      return;
    }
    this.announce = true;
  }

  async guardarRegalo() {
    try {
      this.spinner.show();
      this.closeAnnounce()
      this.markFormGroupTouched(this.newItemForm);
      if (this.newItemForm.invalid) {
        this.notification.warning('Los campos marcados con * son requeridos');
        return;
      }

      if (
        (!this.newItemForm.value.name || this.newItemForm.value.name == '') ||
        (!this.newItemForm.value.description || this.newItemForm.value.description == '') ||
        (!this.newItemForm.value.price || this.newItemForm.value.price < 1)
      ) {
        this.notification.warning('Los campos marcados con * son requeridos');
        return;
      }
      this.loading = true;
      let gift = this.newItem;

      gift.name = this.newItemForm.value.name;
      gift.description = this.newItemForm.value.description;
      gift.price = this.newItemForm.value.price
      gift.storeId = this.newItemForm.value.storeId;
      gift.qty = 1;
      gift.categoriesIds = [];

      const couple = this.layoutService.getCoupleInformation();
      if (this.newItemForm.value.category instanceof Array) {
        gift.categoriesIds = this.newItemForm.value.category;
      } else {
        gift.categoriesIds = [this.newItemForm.value.category];
      }
      let _couple = await this.coupleService.get(couple.id);
      gift.id = Date.now().toString();
      gift.previewOrder = couple.productsRegistryList.length + 1;

      for (const file of this.imagesFiles) {
        const url = await this.uploadPicture(gift.id, file);
        gift.imagesList.push(url.url);
      }

      gift.image = gift.imagesList[0]

      _couple.productsRegistryList.push(gift);

      const update = await this.coupleService.setProductsList(_couple.id, _couple.experience, _couple.productsRegistryList);
      _couple.productsRegistryList = update;
      this.layoutService.setCoupleInformation(_couple);
      let user = this.layoutService.getCurrentUser();
      user.user = _couple;
      this.layoutService.setCurrentUser(user);
      this.catalogService.catalogChanges();
      this.isOpen = false;
      this.newItemForm.reset();
    } catch (e) {
      this.notification.error(e);
      console.log(e);
    } finally {
      this.loading = false;
      this.spinner.hide();
    }
  }
}
