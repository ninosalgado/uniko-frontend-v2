import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LayoutService } from 'src/app/services/layout.service';
@Component({
  selector: 'app-general-table',
  templateUrl: './general-table.component.html',
  styleUrls: ['./general-table.component.scss']
})
export class GeneralTableComponent implements OnInit  {
  idArticulo:string ='';
  eventId:'string';
  constructor(private activateRoute: ActivatedRoute, private layoutService: LayoutService) {
    activateRoute.params.subscribe(params => {
      this.eventId= params['id'];
    });
  }

  ngOnInit() {
  }
}