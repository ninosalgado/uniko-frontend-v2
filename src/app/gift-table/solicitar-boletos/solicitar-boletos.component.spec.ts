import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarBoletosComponent } from './solicitar-boletos.component';

describe('SolicitarBoletosComponent', () => {
  let component: SolicitarBoletosComponent;
  let fixture: ComponentFixture<SolicitarBoletosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitarBoletosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarBoletosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
