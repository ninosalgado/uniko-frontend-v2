import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LayoutService } from 'src/app/services/layout.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { NotificationsService } from 'src/app/services/notifications.service';
import { async } from '@angular/core/testing';
@Component({
  selector: 'app-solicitar-boletos',
  templateUrl: './solicitar-boletos.component.html',
  styleUrls: ['./solicitar-boletos.component.scss']
})
export class SolicitarBoletosComponent implements OnInit {
  public muestPasos:boolean = true;
  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom: number;
  private geoCoder;
  mapAddress: any;
  formAddress: any;
  shippingAddress: any;//Final & Absurd obj
  neighbourhoods: string[];
  localities: string[];
  states: string[];
  coupleaccount: ICoupleInformation;
  selectedColony: any;
  colonies: any[];
  autoCompleteInput: string;
  ticketsForm: FormGroup;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  // NEW STUFF
  quantity: number;
  formattedAddress: string;
  specificAddress: any;
  additionalInfo: string;
  steep:number = -1;
  showExtra: boolean = false;
  neighbourhoodName: string;
  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private layoutService: LayoutService,
    private formBuilder: FormBuilder
  ) {
      this.coupleaccount = this.layoutService.getCoupleInformation();
      this.selectedColony = {
        settlement:'',
        state:{
          name:''
        },
        town:{
          name:''
        }
      }            
    //this.spinner.show();
  }

  ngOnInit() {
    this.ticketsForm = this.formBuilder.group({
      quantity: ["", Validators.required],
      street: ["", Validators.required],
      intNumber: [''],
      extNumber: ["", Validators.required],
      postalCode: ["", Validators.required],
      neighbourhood: ["", Validators.required],
      state: ["", Validators.required],
      town: ["", Validators.required],
      additional: [""]
    });

    this.reloadCouple();    
  }

  async reloadCouple(){
    this.spinner.show();
    const couple = await this.coupleService.get(this.coupleaccount.id);
    this.coupleaccount = couple;
    this.layoutService.setCoupleInformation(this.coupleaccount);
    this.steep = this.getStatus();
    //load Places Autocomplete
    if(this.steep == 0){
      console.log("loading");
      this.startGoogle();
    }else{
      this.spinner.hide();
    }
  }

  getStatus(){
    let shippingAddress = this.coupleaccount.shippingAddress;    

    console.log(shippingAddress);


    if(!shippingAddress){
      console.log("0");
      //NOT SOLICITED
      return 0;
    }


    if(!shippingAddress.ticketsStatus && !this.coupleaccount.ticketsStatus){
      console.log("0-1");
      //NOT SOLICIED NOR SENDING NOR ERROR
      return 0;
    } 

    if(shippingAddress.ticketsStatus && shippingAddress.ticketsStatus!=""){
      console.log("1");
      // NEW 
      status = shippingAddress.ticketsStatus;      
      if(shippingAddress.ticketsStatus == "SOLICITED"){
        console.log("1-1");
        this.quantity = shippingAddress.ticketsCant;
        this.formattedAddress = shippingAddress.formattedAddress;
        this.specificAddress = shippingAddress.specificAddress;
        this.additionalInfo = shippingAddress.additionalInfo;
        this.showExtra = true;
        return 1;
      }else if(shippingAddress.ticketsStatus == "SENDING"){
        console.log("1-2");
        this.quantity = shippingAddress.ticketsCant;
        this.formattedAddress = shippingAddress.formattedAddress;
        this.specificAddress = shippingAddress.specificAddress;
        this.additionalInfo = shippingAddress.additionalInfo;
        // this.showExtra = true;
        return 2;
      }else{
        console.log("1-fk");
        return 0;
      }
    }    

    //console.log(this.specificAddress);

    if(!shippingAddress.ticketsStatus && this.coupleaccount.ticketsStatus){
      console.log("2");
      if(this.coupleaccount.ticketsStatus == "SOLICITED"){
        this.quantity = this.coupleaccount.ticketsCant;
        this.formattedAddress = this.coupleaccount.shippingAddress.gpsaddress.name;
        this.specificAddress = {};
        this.additionalInfo = "";
        return 1;
      }else if(this.coupleaccount.ticketsStatus == "SENDING"){
        this.quantity = this.coupleaccount.ticketsCant;
        this.formattedAddress = this.coupleaccount.shippingAddress.gpsaddress.name;
        this.specificAddress = {};
        this.additionalInfo = "";
        return 2;
      }else{
        return 0;
      }
    }    

  }

  async startGoogle(){
    this.mapsAPILoader.load().then(async () => {                
      this.geoCoder = new google.maps.Geocoder;      
      await this.getUserLocation();         
    }); 
  }

  async getUserLocation() {
    if (navigator.geolocation) {
       window.navigator.geolocation.getCurrentPosition(position => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
          this.getAddress(this.latitude,this.longitude);
      });
    }
  }

  async getAddress(latitude, longitude) {   
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.mapAddress = results[0];
          console.log(this.mapAddress);
          this.formatResult(results[0].address_components,this.mapAddress.formatted_address);
        } else {
        //window.alert('No results found');
        }
      } else {
        //window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  async formatResult(address,formatted_address) {
    console.log("start format");
    //console.log(address);
      // this will lock in place the selects
      let streets = address.filter(items => items.types.includes('route')).map(function(obj){
        return obj.long_name;
      });
      let numbers = address.filter(items => items.types.includes('street_number')).map(function(obj) {
        return obj.long_name;
      });
      let zipCode = address.filter(items => items.types.includes('postal_code')).map(function(obj) {
        return obj.long_name;
      });
      let cpResult = await this.coupleService.getAddressZipcode(zipCode);      
      this.colonies = cpResult['colonies'].length?cpResult['colonies']:false;
      //this.ticketsForm.get('neighbourhood').setValue();
      this.ticketsForm.patchValue({
        street: streets.join(' '),
        extNumber: numbers?numbers.join(' '):'',
        postalCode: zipCode[0],        
        state: (cpResult['colonies'] && cpResult['colonies'][0])?cpResult['colonies'][0].state.name:"",
        town: (cpResult['colonies'] && cpResult['colonies'][0])?cpResult['colonies'][0].town.name:"",
        neighbourhood: cpResult['colonies'].length?0:""        
      });
      this.neighbourhoodName = cpResult['colonies'][0];

      this.searchElementRef.nativeElement.value = formatted_address;
      this.spinner.hide();
      this.setAutocomplete();
      console.log("End Format");    
  }

  async setAutocomplete(){
    let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(async () => {
        this.spinner.show();
        //get the place result
        let place: google.maps.places.PlaceResult = await autocomplete.getPlace();
        //verify result
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        //set latitude, longitude and zoom
        this.latitude = place.geometry.location.lat();
        this.longitude = place.geometry.location.lng();
        this.zoom = 8;
        await this.getAddress(this.latitude, this.longitude);
      });
    });
  }  

  getSteepClass(steep){
    switch (steep){
      case 0:
        switch (this.steep){
          case -1:
            return ''
          break;
          case 0:
            return 'active';
            break;
          case 1:
          case 2:
          default:
            return 'done';
          break;
        } 
      break;     
      case 1:
        switch (this.steep){
          case 0:
          case -1:
            return ''            
          break;
          case 1:
            return 'active'
          break;
          case 2:
            return 'done'
          break;
        }
      break;
      case 2:
        switch (this.steep){
          case -1:
          case 0:
          case 1:
            return ''
          break;
          case 2:
            return 'done'
          break;
        }
      break;
    }
  }

  async markerDragEnd($event: MouseEvent) {
    this.spinner.show();
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    await this.getAddress(this.latitude, this.longitude);
  }

  changeColony(event){
    let index = event.target.value.split(':');
    let selectedColony = this.colonies[index[0]];
    this.ticketsForm.patchValue({
      state: selectedColony.state.name,
      town: selectedColony.town.name
    });
  }

  shippingAddressBuilder(mapValue): any{
    this.neighbourhoodName = this.neighbourhoodName = this.colonies.length?this.colonies[this.ticketsForm.value.neighbourhood].settlement:this.ticketsForm.value.neighbourhood;
    let shippingAddress={
      "specificAddress":{
        "street":this.ticketsForm.value.street,
        "street_number":this.ticketsForm.value.extNumber,
        "interior_number":this.ticketsForm.value.intNumber,
        "neighbourhood": this.neighbourhoodName,
        "zipcode":this.ticketsForm.value.postalCode,
        "state":this.ticketsForm.value.state,
        "town":this.ticketsForm.value.town,
        "aditional":this.ticketsForm.value.aditional,
        "tickets":this.ticketsForm.value.quantity,
        "gpsaddress":{
          "idKey":mapValue.place_id,
          "place_id":mapValue.place_id,
          "name": mapValue.formatted_address,
          "latitude": this.latitude,
          "longitude" : this.longitude,
          "neighbourhood" : this.ticketsForm.value.town
        }
      },
      "formattedAddress":mapValue.formatted_address,
      "additionalInfo":this.ticketsForm.value.additional,
      "ticketsCant":this.ticketsForm.value.quantity,
      "ticketsStatus":"SOLICITED"
    }
    console.log(this.ticketsForm);
    return shippingAddress;
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  async sigPaso(){
    try{
      this.spinner.show();
      this.markFormGroupTouched(this.ticketsForm);
      if (this.ticketsForm.invalid) {
        return;
      }
      const shippingAddress = this.shippingAddressBuilder(this.mapAddress);
      
      let result = await this.coupleService.updateTickets(this.coupleaccount.id,shippingAddress);

      let couple = await this.coupleService.get(this.coupleaccount.id);

      couple.shippingAddress = shippingAddress;
      //this.coupleaccount = await this.coupleService.set//this.coupleService.updateRegister(couple);
      //let info = await this.layoutService.setCoupleInformation(couple);
      this.layoutService.setCoupleInformation(couple);    
      await this.reloadCouple();
    }catch(e){
      this.notification.error("Ha ocurrido un error.");
      console.log("E: ",e)
    }finally{
      this.spinner.hide();
    }
  }
}
