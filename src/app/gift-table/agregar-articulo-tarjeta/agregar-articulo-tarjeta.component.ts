import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from 'src/app/services/notifications.service';
import { CatalogsService } from 'src/app/services/catalogs.service';

@Component({
  selector: 'app-agregar-articulo-tarjeta',
  templateUrl: './agregar-articulo-tarjeta.component.html',
  styleUrls: ['./agregar-articulo-tarjeta.component.scss']
})
export class AgregarArticuloTarjetaComponent implements OnInit {
  @Input() index2:number;
  @Input() art2:any = {};
  @Input() lock:boolean;
  @Input() store: string = '';
  @Input() category: string = '';
  @Input() isVisible:boolean;

  constructor(
    private router:Router,
    private _layoutService: LayoutService, 
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private catalogService: CatalogsService,
    private notification: NotificationsService,
  ) { 
  }

  ngOnInit() {
    
  }

  async addItem(itemId:string){
    try{      
      this.spinner.show();  
      const quantity = 1;
      let gift = this.art2;
      const couple = this._layoutService.getCoupleInformation();
      let _couple = await this.coupleService.get(couple.id);
      let prevItem = _couple.productsRegistryList.find(item=> item.id == gift.id);
      let max = 0;
      if(_couple.productsRegistryList && _couple.productsRegistryList.length>0){
        const lastitem = _couple.productsRegistryList.reduce(function(prev, current) {
          return (prev.previewOrder > current.previewOrder) ? prev : current
        });
        max = lastitem.previewOrder;
      }
      
      gift.previewOrder = max+1;

      if(prevItem){
        prevItem.qty += quantity
      }else{
        gift.qty = quantity;
        _couple.productsRegistryList.push(gift);
      }      

      await this.coupleService.setProductsList(_couple.id, _couple.experience, _couple.productsRegistryList);   
      this._layoutService.setCoupleInformation(_couple);
      let user = this._layoutService.getCurrentUser();
      user.user = _couple;
      this._layoutService.setCurrentUser(user);
      this.notification.success('Articulo agregado');
      this.catalogService.catalogChanges();
    } catch (e) {
      this.notification.error(e);
      console.log(e);
    }finally{            
      this.spinner.hide(); 
      //this.router.navigate(['/','gift-table']);
    }   
  }

  

  viewItem(itemId:string){
    this.router.navigate(['gift-table','add',itemId]);
  } 

  acomodoArt(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4 ArtSelec";
  }
  acomodoArt2(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4";
  }
}
