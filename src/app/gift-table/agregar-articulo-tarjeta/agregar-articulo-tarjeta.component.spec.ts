import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarArticuloTarjetaComponent } from './agregar-articulo-tarjeta.component';

describe('AgregarArticuloTarjetaComponent', () => {
  let component: AgregarArticuloTarjetaComponent;
  let fixture: ComponentFixture<AgregarArticuloTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarArticuloTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarArticuloTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
