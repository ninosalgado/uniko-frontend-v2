import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GiftTableRoutingModule } from './gift-table-routing.module';
import { GiftTableComponent} from './gift-table.component';
import { TableComponent } from './table/table.component';
import { GeneralTableComponent } from './general-table/general-table.component';
import { SharedModule } from './../shared/shared.module';
import { Ng5SliderModule } from 'ng5-slider';
import { ArticuloTarjetaComponent } from './articulo-tarjeta/articulo-tarjeta.component';
import { EditarTarjetaComponent } from './editar-tarjeta/editar-tarjeta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArticulosTarjetasComponent } from './articulos-tarjetas/articulos-tarjetas.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AgregarArticuloTarjetaComponent } from './agregar-articulo-tarjeta/agregar-articulo-tarjeta.component';
import { DialogAlertaBorrar } from './articulo-tarjeta/articulo-tarjeta.component'
import { SolicitarBoletosComponent } from './solicitar-boletos/solicitar-boletos.component';
import { DragulaModule } from 'ng2-dragula';
import { GeneralItemCardsComponent } from './general-item-cards/general-item-cards.component';
import { AddGiftComponent } from './add-gift/add-gift.component';
import { AgmCoreModule } from '@agm/core';
import { MaterialModule } from '../material.module';
import {MatSelectModule} from '@angular/material/select';
@NgModule({
  declarations: [
    GiftTableComponent,  
    TableComponent,
    GeneralTableComponent,
    ArticuloTarjetaComponent,
    EditarTarjetaComponent,
    ArticulosTarjetasComponent,    
    GeneralItemCardsComponent,
    AgregarArticuloTarjetaComponent,
    SolicitarBoletosComponent,
    AddGiftComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    GiftTableRoutingModule,
    SharedModule,
    Ng5SliderModule,
    DragDropModule,
    DragulaModule.forRoot(),
    AgmCoreModule,
    MaterialModule,
    MatSelectModule
  ]
})
export class GiftTableModule { }
