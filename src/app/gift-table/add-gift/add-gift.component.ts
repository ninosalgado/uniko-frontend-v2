import { Component, Input, OnInit, OnChanges, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LayoutService } from '../../services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-add-gift',
  templateUrl: './add-gift.component.html',
  styleUrls: ['./add-gift.component.scss']
})
export class AddGiftComponent implements OnInit {
  articulo:any = {};
  categories: any[];
  stores: any[];
  loaded = false;
  itemId:string;
  currentImage:any;
  quantity: number = 1;
  constructor(
    private activatedRoute:ActivatedRoute, 
    private _layoutService: LayoutService, 
    public dialog: MatDialog,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private catalogService: CatalogsService,
    private notification: NotificationsService,
    private router: Router
    ) {
    this.activatedRoute.params.subscribe( params => {      
      this.getingStarted(params['id']);
      this.categories =[];
      this.stores =[];      
    } );
  }


  async getingStarted(id){
    await this.catalogs(id);    
  }

  async catalogs(id): Promise<void> {
    try {
      this.spinner.show();
      this.categories = await this.coupleService.getCategories();
      this.stores = await this.coupleService.getStores();    
      const result = await this.coupleService.getArticlesByCatalogs(0,30000,null,null,id);  
      this.articulo =  result[0];
      this.currentImage = this.articulo.image;
      console.log(this.articulo);
      
    } catch (e) {
      console.log(e);
    } finally {
      this.loaded = true;
      this.spinner.hide(); 
    }
  }

  changeImageView(image){
    this.currentImage = image;
  }

  getStoreName(id){
    let name = this.stores.filter(store=> store.id == id).map(function(obj) {
      return obj.name;
    });
    return name;
  }

  async addItem(){
    try{
      console.log(this.quantity);
      if(this.quantity<=0){
        return
      }
      this.spinner.show();  
      let gift = this.articulo;
      const couple = this._layoutService.getCoupleInformation();
      let _couple = await this.coupleService.get(couple.id);
      let prevItem = _couple.productsRegistryList.find(item=> item.id == gift.id);
      let max = 0;
      if(_couple.productsRegistryList && _couple.productsRegistryList.length>0){
        const lastitem = _couple.productsRegistryList.reduce(function(prev, current) {
          return (prev.previewOrder > current.previewOrder) ? prev : current
        });
        max = lastitem.previewOrder;
      }
      
      gift.previewOrder = max+1;

      if(prevItem){
        prevItem.qty += this.quantity
      }else{
        gift.qty = this.quantity;
        _couple.productsRegistryList.push(gift);
      }

      const update = await this.coupleService.setProductsList(_couple.id, _couple.experience, _couple.productsRegistryList);   
      this._layoutService.setCoupleInformation(_couple);
      let user = this._layoutService.getCurrentUser();
      user.user = _couple;
      this._layoutService.setCurrentUser(user);
      this.notification.success('Articulo agregado')
      this.catalogService.catalogChanges();
    } catch (e) {
      this.notification.error(e);
      console.log(e);
    }finally{            
      this.spinner.hide(); 
      this.router.navigate(['/','gift-table']);
    }    
  }

  getCatName(id){
    let name = this.categories.filter(cat=> cat.id == id).map(function(obj) {
      return obj.name;
    });
    return name;
  }

  ngOnInit() {
    this.catalogService.giftEmitter.subscribe(data=>{      
      this.articulo = this._layoutService.getProduct(this.itemId);
    });
  }
}

