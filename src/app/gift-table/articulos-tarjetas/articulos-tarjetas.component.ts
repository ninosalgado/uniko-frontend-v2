import { Component, OnInit, Inject, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { Options } from 'ng5-slider';
import { LayoutService } from './../../services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { DragulaService } from 'ng2-dragula';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from 'src/app/services/notifications.service';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { filter } from 'rxjs/operators';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { Router } from '@angular/router';
import { tmpdir } from 'os';

export interface IProductRegister {
  categoriesIds: Array<string>;
  createByAccount: boolean;
  description: string;
  image: string;
  name: string;
  price: number;
  qty: number;
  storeId: string;
  isActive: boolean;
  id?: string;
  altImg?: string;
  check?: boolean;
  createdAt?: Date;
  hidden?: boolean;
  isMercadolibre?: boolean;
  updatedAt?: Date;
  previewOrder: number;
}


@Component({
  selector: 'app-articulos-tarjetas',
  templateUrl: './articulos-tarjetas.component.html',
  styleUrls: ['./articulos-tarjetas.component.scss']
})
export class ArticulosTarjetasComponent implements OnInit {
  @Input()
  usefullInformation;
  couple: ICoupleInformation;
  minValue: number = 0;
  maxValue: number = 30000;
  categories: any[] = [];
  stores: any[] = [];
  storesSelected: any[] = [];
  categoriesSelected: any[] = [];
  filterPriceOpen = false;
  filterCategoriesOpen = false;
  filterStoresOpen = false;

  isOpen: boolean = false;
  isOpenFilter: boolean = false;
  options: Options = {
    floor: 0,
    ceil: 30000,
    step: 100
  };
  currentOrder: number = 3;
  lockDrag = false;
  dragula: any;
  articlesShow: IProductRegister[] = [];
  sharedCouple: any;

  @Input() verticalLT: string | any;


  constructor(
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private coupleService: CoupleService,
    private dragulaService: DragulaService,
    private catalogService: CatalogsService,
    private router: Router
  ) {
    this.couple = this.layoutService.getCoupleInformation();
  }

  ngOnInit() {
    this.dragula = this.dragulaService.dropModel("ITEMSCTLG").subscribe(async args => {
      this.sortFixer(args.targetModel);
    });
    this.dragulaService.createGroup("ITEMSCTLG", {
      moves: function (el, container, handle) {
        let order = document.getElementById('domCurrentOrder')['value'];
        return (handle.classList.contains('handle') && order == '3');
      },
    });
    this.layoutService.coupleEmitter.subscribe(data => {
      this.couple = this.layoutService.getCoupleInformation();
      this.setPreviewOrder(this.currentOrder);
    });
    this.catalogService.giftEmitter.subscribe(data => {
      this.couple = this.layoutService.getCoupleInformation();
    });
    localStorage.getItem('edoAgrega');
    this.getCouple();
    this.catalogs();
    this.maxValue = Math.ceil(this.getMaxPrice(this.couple.productsRegistryList) / 100) * 100;
    this.options.ceil = this.maxValue;
  }

  async sortFixer(model) {
    try {
      this.lockDrag = true;
      for (let i = 0; i < model.length; i++) {
        model[i].previewOrder = i + 1;
      }
      this.couple.productsRegistryList = model;
      await this.commitChanges();
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.lockDrag = false;
    }
  }

  async arrayComposter(model, index?) {
    this.couple.productsRegistryList = model;
    /*if(!index){
      index = 0;
    }
    await this.saveItemOrder(model[index]);

    if(index+1 < model.length){
      await this.arrayComposter(model,index+1);
    }*/
  }

  ngOnDestroy() {
    try {
      this.dragula.unsubscribe();
      this.dragulaService.destroy('ITEMSCTLG');
    } catch (e) {
      console.log(e);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    const width = event.target.innerWidth;
    if (width < 768) {
      this.forceColapse();
    }
  }

  setPreviewOrder(type) {
    let itemsSorted = <IProductRegister[]>JSON.parse(JSON.stringify(this.couple.productsRegistryList));
    try {
      switch (type) {
        case 1:
          itemsSorted.sort(function (obj1, obj2) {
            // Ascending: price
            return obj1.price - obj2.price;
          });
          break;
        case 2:
          itemsSorted.sort(function (obj1, obj2) {
            // Descending: price
            return obj2.price - obj1.price;
          });
          break;
        case 3:
          itemsSorted.sort(function (obj1, obj2) {
            // Original: index ( previewOrder )
            return obj1.previewOrder - obj2.previewOrder;
          });
          break;
        //array.sort((a,b) => a.title.rendered.localeCompare(b.title.rendered));
      }
    }
    catch (e) {
      console.log(e)
    }
    finally {
      this.currentOrder = type;
      this.couple.productsRegistryList = itemsSorted;
    }
  }

  async commitChanges() {
    const update = await this.coupleService.setProductsList(this.couple.id, this.couple.experience, this.couple.productsRegistryList);
    this.layoutService.setCoupleInformation(this.couple);
    let user = this.layoutService.getCurrentUser();
    user.user = this.couple;
    this.layoutService.setCurrentUser(user);
    this.catalogService.catalogChanges();
  }

  async saveItemOrder(item) {
    let info = await this.coupleService.updateProductTable(item, this.layoutService.getUser().id);
  }
  /*
  async resetPositions(star:number, end:number,index?:number){
    let trueIndex = index==null?star:index;
    if(trueIndex > end){
      await this.commitChanges();
      console.log('done, unlocking');
      this.lockDrag = false;
      return true;
    }
    this.couple.productsRegistryList[trueIndex].previewOrder = trueIndex;
    this.saveItemOrder(this.couple.productsRegistryList[trueIndex]);
    await this.resetPositions(star,end,trueIndex+1);
  }
  async resetPositionsReverse(star:number, end:number,index?:number){
    let trueIndex = index==null?star:index;
    if(trueIndex < end){
      await this.commitChanges();
      console.log('done, unlocking');
      this.lockDrag = false;
      return true;
    }
    this.couple.productsRegistryList[trueIndex].previewOrder = trueIndex;
    this.saveItemOrder(this.couple.productsRegistryList[trueIndex]);
    await this.resetPositionsReverse(star, end, trueIndex-1);
  }*/

  async getCouple(): Promise<void> {
    try {
      this.spinner.show();
      const couple = await this.coupleService.get(this.couple.id);
      this.couple = couple;
      this.layoutService.setCoupleInformation(this.couple);
    } catch (e) {
      this.notification.error(e)
      console.log(e);
    } finally {
      this.queryArticles();
      this.spinner.hide();
    }
  }

  async catalogs(): Promise<void> {
    try {
      this.categories = await this.coupleService.getCategories();
      this.stores = await this.coupleService.getStores();
    } catch (e) {
      console.log(e);
    } finally {

    }
  }

  setStore(store: any) {
    const exist = this.storesSelected.find(_store => {
      return _store.id === store.id
    });
    if (!exist) {
      this.storesSelected.push(store);
    }
  }

  setCategory(category: any) {
    const exist = this.categoriesSelected.find(_category => {
      return !!(_category.id === category.id)
    });
    if (!exist) {
      this.categoriesSelected.push(category);
    }
  }


  dynamicFilter(itemId) {
    const price = this.dynamicPrice(itemId);
    const store = this.dynamicStores(itemId);
    const category = this.dynamicCategories(itemId);
    return price && store && category;
  }

  dynamicPrice(itemId) {
    const item = this.couple.productsRegistryList.find(product => product.id == itemId);
    const price = item.price >= this.minValue && item.price <= this.maxValue;
    return price;
  }

  dynamicStores(itemId) {
    const item = this.couple.productsRegistryList.find(product => product.id == itemId);
    let allStores = true;
    if (this.storesSelected.length) {
      allStores = this.storesSelected.find(store => store.id === item.storeId || store.name === 'Otras');
    }
    return allStores;
  }

  dynamicCategories(itemId) {
    const item = this.couple.productsRegistryList.find(product => product.id == itemId);
    const selectedCategoriesId = this.categoriesSelected.map(category => { return category.id });
    let allCoincidences = false;

    if (selectedCategoriesId.length > 0) {
      selectedCategoriesId.forEach(cat => {
        let coincidence = item.categoriesIds.includes(cat);
        if (coincidence) {
          allCoincidences = true;
        }
      });
    } else {
      allCoincidences = true;
    }

    return allCoincidences;
  }


  getMaxPrice(articles) {
    let basin = articles;
    let maxValue = Math.max.apply(Math, articles.map(function (o) { return o.price; }));
    let expensive = articles.filter(function (o) { return o.price === maxValue; })[0];
    return expensive.price;
  }

  queryArticles() {
    this.couple.productsRegistryList.forEach(element => {
      element.visible = true;
    });
    this.setPreviewOrder(this.currentOrder);
  }

  removeStore(store: any) {
    let index = 0;
    this.storesSelected.forEach((_store, _index) => {
      if (store.id === _store.id) {
        index = _index;
      }
    })
    this.storesSelected.splice(index, 1);
  }

  removeCategory(category: any) {
    let index = 0;
    this.categoriesSelected.forEach((_category, _index) => {
      if (category.id === _category.id) {
        index = _index;
      }
    })
    this.categoriesSelected.splice(index, 1);
  }

  getNameStore(id: string): string {
    const store = this.stores.find(_store => _store.id == id);
    let name = store ? store.name : '';
    return name;
  }

  getStore(stores): string {

    return '';
  }

  getNameCategory(categories: any[]): string {
    let categoriesName = [];
    this.categories.forEach(category => {
      const _category = categories.find(cat => {
        return cat === category.id;
      });
      if (_category) {
        categoriesName.push(category.name);
      }
    });
    return categoriesName.join(', ');
  }

  colapsa(tipo: any) {
    //console.log(localStorage.getItem('edoAgrega'));
    let flecha = document.getElementById('flecha' + tipo);
    let elemento = document.getElementById('collapse' + tipo);
    let toggleDat = elemento.dataset.toggle;
    if (toggleDat == "up") {
      flecha.className = "fas fa-chevron-down float-right";
      elemento.setAttribute("data-toggle", "down");
      elemento.style.height = "0px";
      elemento.style.overflow = "hidden";
      elemento.style.padding = "0px";
    }
    else if (toggleDat == "down") {
      flecha.className = "fas fa-chevron-up float-right";
      elemento.setAttribute("data-toggle", "up");
      elemento.style.height = "auto";
      elemento.style.overflow = "auto";
      elemento.style.padding = ".75rem 1.25rem";
    }

  }

  forceColapse() {
    this.filterPriceOpen = false;
    this.filterCategoriesOpen = false;
    this.filterStoresOpen = false;
  }

  colapsa2(id: string) {
    let elemento = document.getElementById(id);
    let toggleDat = elemento.getAttribute('data-toggle');
    if (toggleDat == "up") {
      elemento.setAttribute("data-toggle", "down");
      elemento.style.display = "none";
    }
    else if (toggleDat == "down") {
      elemento.setAttribute("data-toggle", "up");
      elemento.style.display = "";
    }

  }
  acomodoArt(idArt: string) {
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4 ArtSelec";
  }
  acomodoArt2(idArt: string) {
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4";
  }
  borrarArt(idArt: number): void {
    this.couple.productsRegistryList.slice(idArt, 1);
    this.commitChanges();
  }
  /*editarArt(idArticulo:number){
    this.router.navigate(['/gift-table/edit/' + idArticulo]);
  }*/
  ordenPor(tipo: number) {
    alert(tipo);
  }

  iniMesa() {
    this.router.navigate(['/gift-table/add/']);
  }
}
