import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticulosTarjetasComponent } from './articulos-tarjetas.component';

describe('ArticulosTarjetasComponent', () => {
  let component: ArticulosTarjetasComponent;
  let fixture: ComponentFixture<ArticulosTarjetasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticulosTarjetasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticulosTarjetasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
