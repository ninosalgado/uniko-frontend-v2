import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralItemCardsComponent } from './general-item-cards.component';

describe('GeneralItemCardsComponent', () => {
  let component: GeneralItemCardsComponent;
  let fixture: ComponentFixture<GeneralItemCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralItemCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralItemCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
