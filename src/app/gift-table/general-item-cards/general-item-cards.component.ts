import { Component, OnInit, Inject, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { Options } from 'ng5-slider';
import { LayoutService } from './../../services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { DragulaService } from 'ng2-dragula';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from 'src/app/services/notifications.service';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { filter } from 'rxjs/operators';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

export interface IProductRegister{
  categoriesIds: Array<string>;
  createByAccount: boolean;
  description: string;
  image: string;
  name: string;
  price: number;
  qty: number;
  storeId: string;
  isActive: boolean;
  id?: string;
  altImg?:string;
  check?:boolean;
  createdAt?: Date;
  hidden?: boolean;
  isMercadolibre?: boolean;
  updatedAt?: Date;
  previewOrder: number;
}


@Component({
  selector: 'app-general-item-cards',
  templateUrl: './general-item-cards.component.html',
  styleUrls: ['../articulos-tarjetas/articulos-tarjetas.component.scss']
})
export class GeneralItemCardsComponent implements OnInit {
  @Input()
  usefullInformation;
  couple: ICoupleInformation;
  minValue: number = 0;
  maxValue: number = 30000;
  categories: any[] = [];
  stores: any[] = [];
  storesSelected: any[] = [];
  categoriesSelected: any[] = [];
  filterPriceOpen =true;
  filterCategoriesOpen =true;
  filterStoresOpen =true;

  isOpen: boolean = false;
  isOpenFilter: boolean = false;
  options: Options = {
    floor: 0,
    ceil: 30000,
    step: 100
  };
  currentOrder: number = 3;
  lockDrag = false;
  dragula:any;
  articles: IProductRegister[] = [];
  sharedCouple: any;

  @Input() verticalLT: string | any;


  constructor(
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private coupleService: CoupleService,
    private dragulaService: DragulaService,
    private catalogService: CatalogsService,
  ) {
    this.couple = this.layoutService.getCoupleInformation();

    this.catalogs();
  }

  ngOnInit() {
    this.catalogService.giftEmitter.subscribe(data=>{
      this.couple = this.layoutService.getCoupleInformation();
    });
    localStorage.getItem('edoAgrega');
    this.getCouple();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    const width = event.target.innerWidth;
    if(width<768){
      this.forceColapse();
    }
  }

  setPreviewOrder(type){
    let itemsSorted  = <IProductRegister[]> JSON.parse(JSON.stringify(this.articles));
    try{
      switch(type){
        case 1:
          itemsSorted.sort(function(obj1, obj2) {
            // Ascending: price
            return obj1.price - obj2.price;
          });
          break;
        case 2:
          itemsSorted.sort(function(obj1, obj2) {
            // Descending: price
            return obj2.price - obj1.price;
          });
          break;
        case 3:
          itemsSorted.sort(function(obj1, obj2) {
            // Original: index ( previewOrder )
            return obj1.previewOrder - obj2.previewOrder;
          });
          break;
          //array.sort((a,b) => a.title.rendered.localeCompare(b.title.rendered));
      }
    }
    catch(e){
      console.log(e)
    }
    finally{
      this.currentOrder = type;
      this.articles = itemsSorted;
    }
  }

  async getCouple(): Promise<void> {
    try {
      this.spinner.show();
      const couple = await this.coupleService.get(this.couple.id);
      this.couple = couple;
      this.layoutService.setCoupleInformation(this.couple);
    } catch (e) {
      this.notification.error(e)
    } finally {
      await this.getArticles();
      this.spinner.hide();
    }
  }

  async getArticles(): Promise<void> {
    try {
      //console.log(this._layoutService.getUser());
      const categories = this.categories.map((info) => (info.id));
      console.log(categories);
      this.articles = await this.coupleService.getArticlesByCatalogs(0,30000, categories);

      this.articles.forEach(
        (val : any, key: any) => {
          val.visible = true;
          if(!val.previewOrder){
            val.previewOrder = this.articles.length;
          }
        }
      )
    } catch (e) {
      console.log(e);
    } finally {
      this.maxValue = this.getMaxPrice(this.articles);
      this.options.ceil = this.maxValue;
    }
  }

  async catalogs(): Promise<void> {
    try {
      this.categories = await this.coupleService.getCategories();
      this.stores = await this.coupleService.getStores();
    } catch (e) {
      console.log(e);
    } finally {

    }
  }

  setStore(store: any) {
   const exist = this.storesSelected.find(_store => {
    return _store.id === store.id
   });
   if (!exist) {
    this.storesSelected.push(store);
   }
  }

  setCategory(category: any) {
    const exist = this.categoriesSelected.find(_category => {
      return !!(_category.id === category.id)
    });
    if (!exist) {
      this.categoriesSelected.push(category);
    }
  }


  dynamicFilter(itemId){
    const price = this.dynamicPrice(itemId);
    const store = this.dynamicStores(itemId);
    const category = this.dynamicCategories(itemId);
    return price && store && category;
  }

  dynamicPrice(itemId){
    const item = this.articles.find(product=>product.id ==itemId);
    const price = item.price >= this.minValue && item.price <= this.maxValue;
    return price;
  }

  dynamicStores(itemId){
    const item = this.articles.find(product=>product.id ==itemId);
    let allStores=true;
    if(this.storesSelected.length){
      allStores = this.storesSelected.find(store => store.id === item.storeId || store.name === 'Otras');
    }
    return allStores;
  }

  dynamicCategories(itemId){
    const item = this.articles.find(product=>product.id ==itemId);
    const selectedCategoriesId = this.categoriesSelected.map(category=>{return category.id});
    let allCoincidences=false;

    if(selectedCategoriesId.length>0){
      selectedCategoriesId.forEach(cat=>{
        let coincidence = item.categoriesIds.includes(cat);
        if(coincidence){
          allCoincidences = true;
        }
      });
    }else{
      allCoincidences = true;
    }

    return allCoincidences;
  }

  getMaxPrice(articles){
    let basin =  articles;
    let maxValue = Math.max.apply(Math,articles.map(function(o){return o.price;}));
    let expensive = articles.filter(function(o) { return o.price === maxValue; })[0];
    return expensive.price;
  }

  removeStore(store: any) {
    let index = 0;
    this.storesSelected.forEach((_store, _index) => {
      if (store.id === _store.id) {
        index = _index;
      }
    })
    this.storesSelected.splice(index, 1);
  }

  removeCategory(category: any) {
    let index = 0;
    this.categoriesSelected.forEach((_category, _index) => {
      if (category.id === _category.id) {
        index = _index;
      }
    })
    this.categoriesSelected.splice(index, 1);
  }

  getNameStore(id: string): string {
    const store = this.stores.find(_store => _store.id == id);
    return store ? store.name : '';
  }

  getStore(stores): string {
    return '';
  }

  getNameCategory(categories: any[]): string {
    let categoriesName = [];
    this.categories.forEach(category => {
      const _category = categories.find(cat => {
        return cat === category.id;
      });
      if (_category) {
        categoriesName.push(category.name);
      }
    });
    return categoriesName.join(', ');
  }

  colapsa(tipo:any){
    let flecha = document.getElementById('flecha'+tipo);
    let elemento = document.getElementById('collapse'+tipo);
    let toggleDat = elemento.dataset.toggle;
    if(toggleDat=="up"){
      flecha.className = "fas fa-chevron-down float-right";
      elemento.setAttribute("data-toggle","down");
      elemento.style.height="0px";
      elemento.style.overflow="hidden";
      elemento.style.padding="0px";
   }
   else if(toggleDat=="down"){
      flecha.className = "fas fa-chevron-up float-right";
        elemento.setAttribute("data-toggle","up");
        elemento.style.height="auto";
        elemento.style.overflow="auto";
        elemento.style.padding=".75rem 1.25rem";
   }

  }

  forceColapse(){
    this.filterPriceOpen = false;
    this.filterCategoriesOpen = false;
    this.filterStoresOpen = false;
  }

  colapsa2(id:string){
    let elemento = document.getElementById(id);
    let toggleDat = elemento.getAttribute('data-toggle');

    if(toggleDat=="up"){
        elemento.setAttribute("data-toggle","down");
        elemento.style.display="none";
    }
    else if(toggleDat=="down"){
        elemento.setAttribute("data-toggle","up");
        elemento.style.display="";
    }

  }
  acomodoArt(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4 ArtSelec";
  }
  acomodoArt2(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4";
  }
 /*editarArt(idArticulo:number){
   this.router.navigate(['/gift-table/edit/' + idArticulo]);
 }*/
 ordenPor(tipo:number){
   alert(tipo);
 }
}
