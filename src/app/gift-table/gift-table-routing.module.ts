import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GiftTableComponent } from './gift-table.component';
import { TableComponent } from './table/table.component';
import { GeneralTableComponent } from './general-table/general-table.component';
import { EditarTarjetaComponent } from './editar-tarjeta/editar-tarjeta.component';
import { AuthGuard } from '../_guards/auth.guard';
import { AddGiftComponent } from './add-gift/add-gift.component';
import { SolicitarBoletosComponent } from './solicitar-boletos/solicitar-boletos.component';

const routes: Routes = [{
  path: '',
  component: GiftTableComponent,  
  children: [{
    path: '',    
    component: TableComponent,
    data: {
        title: 'Masa de regalos'
    }
  },
  {
    path: 'add',
    component: GeneralTableComponent,
    data: {
        title: 'Mosa de regalos'
    }
  },
  {
    path: 'add/:id',
    component: AddGiftComponent,
  },
  {
    path:'edit/:id',
    component: EditarTarjetaComponent
  },
  {
    path:'tickets',
    component: SolicitarBoletosComponent
  }]
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GiftTableRoutingModule { }
