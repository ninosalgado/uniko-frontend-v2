import { Component, Input, OnInit, OnChanges, Inject, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';
import { LayoutService } from './../../services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from 'src/app/services/notifications.service';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-view-gift',
  templateUrl: './view-gift.component.html',
  styleUrls: ['./view-gift.component.scss']
})
export class ViewGiftComponent implements OnInit {
  articulo: any = {};
  categories: any[];
  couple: ICoupleInformation;
  stores: any[];
  loaded = false;
  currency: string;
  itemId: string;
  images: any[] = [];
  imagesFiles: any[] = [];
  item: any;
  category: any;
  isOpen: boolean = false;
  isOpenDelete: boolean = false;
  itemForm: FormGroup;
  quantity: number;
  @ViewChild('newGiftImage') inputImageField: ElementRef;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _layoutService: LayoutService,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private catalogService: CatalogsService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private formBuilder: FormBuilder,
  ) {
    this.quantity = 1;
    this.currency = this.layoutService.settings.info.currency;
    this.catalogService.giftEmitter.subscribe(data => {
      this.getProduct();
    });
  }

  get formatPrice() {
    return this.articulo.price / this.layoutService.settings.info.currencyChange;
  }

  getCatName(id) {
    let name = this.categories.filter(cat => cat.id == id).map(function (obj) {
      return obj.name;
    });
    return name;
  }

  getStoreName(id) {
    let name = this.stores.filter(store => store.id == id).map(function (obj) {
      return obj.name;
    });
    return name;
  }

  obtenURL(objDat: string) {
    this.articulo.image = objDat;
  }

  async catalogs(): Promise<void> {
    try {
      this.spinner.show();
      this.couple = this.layoutService.getCoupleInformation();
      console.log(this.couple.id);
      this.categories = await this.coupleService.getCategoriesForClient(this.couple.id);
      this.stores = await this.coupleService.getStores();
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
      this.activatedRoute.params.subscribe(params => {
        this.articulo = this._layoutService.getProduct(params['id']);
        if (!this.articulo.imagesList) {
          this.articulo.imagesList = [];
        }
        this.articulo.imagesList[0] = this.articulo.image;
        this.loaded = true;
        this.itemId = params['id'];
        this.item = <any>JSON.parse(JSON.stringify(this.articulo));
      });
    }
  }

  ngOnInit() {
    this.catalogs();
  }

  async getProduct() {
    this.spinner.show();
    this.articulo = await this._layoutService.getProduct(this.itemId);
    this.spinner.hide();
  }

  add(product) {
    product.quantity = this.quantity;
    this.layoutService.cart.add(product);
    this.router.navigate([this.couple.url, "checkout", "cart"]);
  }
}
