import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestArticulosTarjetasComponent } from './guest-articulos-tarjetas.component';

describe('ArticulosTarjetasComponent', () => {
  let component: GuestArticulosTarjetasComponent;
  let fixture: ComponentFixture<GuestArticulosTarjetasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestArticulosTarjetasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestArticulosTarjetasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
