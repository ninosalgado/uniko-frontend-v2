import { Component, OnInit, Inject } from '@angular/core';
import { Options } from 'ng5-slider';
import { LayoutService } from '../../services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from 'src/app/services/notifications.service';
import { IProductRegister } from 'src/app/_models/data';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';


@Component({
  selector: 'app-guest-articulos-tarjetas',
  templateUrl: './guest-articulos-tarjetas.component.html',
  styleUrls: ['./guest-articulos-tarjetas.component.scss']
})
export class GuestArticulosTarjetasComponent implements OnInit {
  usefullInformation;
  currency: string;
  isOpen: boolean = false;
  isOpenFilter: boolean = false;
  couple: ICoupleInformation;
  public muestAgregar: boolean = true;
  public muestraRegalos: boolean = true;
  minValue: number = 0;
  maxValue: number = 30000;
  categories: any[] = [];
  stores: any[] = [];
  storesSelected: any[] = [];
  categoriesSelected: any[] = [];
  url: string;
  options: Options = {
    floor: 0,
    ceil: 30000,
    step: 100
  };

  currencies = {
    "USD": 0,
    "CAD": 0,
    "EUR": 0,
    "MXN": 1
  };

  filterPriceOpen = false;
  filterCategoriesOpen = false;
  filterStoresOpen = false;
  loadedCurrency: boolean = false;

  currentOrder: number = 3;
  lockDrag = false;
  coupleId: string;
  articles: IProductRegister[] = [];
  articlesShow: IProductRegister[] = [];

  constructor(
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private coupleService: CoupleService,
    private activateRoute: ActivatedRoute,
    private layoutService: LayoutService
  ) {
    // activateRoute.params.subscribe(params => {
    //   this.coupleId = params['id'];
    // });
    this.coupleId = this.layoutService.getCoupleInformation() ?
      this.layoutService.getCoupleInformation().id : '';
    this.currency = this.layoutService.settings.info.currency;
    this.layoutService.coupleEmitter.subscribe(data => {
      this.coupleId = this.layoutService.getCoupleInformation().id;
      this.couple = this.layoutService.coupleInformation.info;
      this.catalogs();
      this.getCouple();
      this.currency = this.layoutService.settings.info.currency;
    });
    if (this.coupleId) {
      this.couple = this.layoutService.coupleInformation.info;
      this.catalogs();
      this.getCouple();
    }
  }

  ngOnInit() {
    localStorage.getItem('edoAgrega');
  }

  setPreviewOrder(type) {
    let itemsSorted = <IProductRegister[]>JSON.parse(JSON.stringify(this.couple.productsRegistryList));
    try {
      switch (type) {
        case 1:
          itemsSorted.sort(function (obj1, obj2) {
            // Ascending: price
            return obj1.price - obj2.price;
          });
          break;
        case 2:
          itemsSorted.sort(function (obj1, obj2) {
            // Descending: price
            return obj2.price - obj1.price;
          });
          break;
        case 3:
          console.log("Amazing");
          itemsSorted.sort(function (obj1, obj2) {
            // Original: index ( previewOrder )
            return obj1.previewOrder - obj2.previewOrder;
          });
          break;
        //array.sort((a,b) => a.title.rendered.localeCompare(b.title.rendered));
      }
    }
    catch (e) {
      console.log(e)
    }
    finally {
      this.currentOrder = type;
      this.couple.productsRegistryList = itemsSorted;
    }
  }


  dynamicFilter(itemId) {
    const price = this.dynamicPrice(itemId);
    const store = this.dynamicStores(itemId);
    const category = this.dynamicCategories(itemId);
    return price && store && category;
  }

  dynamicPrice(itemId) {
    const item = this.couple.productsRegistryList.find(product => product.id == itemId);
    const price = item.price >= this.minValue && item.price <= this.maxValue;
    return price;
  }

  dynamicStores(itemId) {
    const item = this.couple.productsRegistryList.find(product => product.id == itemId);
    let allStores = true;
    if (this.storesSelected.length) {
      allStores = this.storesSelected.find(store => store.id === item.storeId || store.name === 'Otras');
    }
    return allStores;
  }

  dynamicCategories(itemId) {
    const item = this.couple.productsRegistryList.find(product => product.id == itemId);
    const selectedCategoriesId = this.categoriesSelected.map(category => { return category.id });
    let allCoincidences = false;

    if (selectedCategoriesId.length > 0) {
      selectedCategoriesId.forEach(cat => {
        let coincidence = item.categoriesIds.includes(cat);
        if (coincidence) {
          allCoincidences = true;
        }
      });
    } else {
      allCoincidences = true;
    }

    return allCoincidences;
  }

  async getArticles(): Promise<void> {
    try {
      //console.log(this._layoutService.getUser());
      this.articles = await this.coupleService.getArticlesByUrl(this.coupleId);

      this.articles.forEach(
        (val: any, key: any) => {
          if (!val.previewOrder) {
            val.previewOrder = this.articles.length;
          }
        }
      )
      //console.log(this.articles);
      this.queryArticles();
    } catch (e) {
      console.log(e);
    } finally {

    }
  }

  async catalogs(): Promise<void> {
    try {
      this.categories = await this.coupleService.getCategoriesForClient(this.couple.id);
      this.stores = await this.coupleService.getStores();
    } catch (e) {
      console.log(e);
    } finally {

    }
  }

  async getCouple(): Promise<void> {
    try {
      this.spinner.show();
      console.log(this.couple.url);
      const couple = await this.coupleService.findOneRestrict(this.couple.url);
      this.couple = couple;
      // this.layoutService.setCoupleInformation(this.couple);
      this.queryArticles();
    } catch (e) {
      this.notification.error(e)
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }

  setStore(store: any) {
    const exist = this.storesSelected.find(_store => {
      return _store.id === store.id
    });
    if (!exist) {
      this.storesSelected.push(store);
      this.queryArticles();
    }
  }

  setCategory(category: any) {
    const exist = this.categoriesSelected.find(_category => {
      return _category.id === category.id
    });
    if (!exist) {
      this.categoriesSelected.push(category);
      this.queryArticles();
    }
  }

  queryArticles() {
    this.couple.productsRegistryList.forEach(element => {
      element.visible = true;
    });
    this.setPreviewOrder(this.currentOrder);
  }

  removeStore(store: any) {
    let index = 0;
    this.storesSelected.forEach((_store, _index) => {
      if (store.id === _store.id) {
        index = _index;
      }
    })
    this.storesSelected.splice(index, 1);
    this.queryArticles();
  }

  removeCategory(category: any) {
    let index = 0;
    this.categoriesSelected.forEach((_category, _index) => {
      if (category.id === _category.id) {
        index = _index;
      }
    })
    this.categoriesSelected.splice(index, 1);
    this.queryArticles();
  }

  getNameStore(id: string): string {
    const store = this.stores.find(_store => _store.id == id);
    return store ? store.name : '';
  }

  getStore(stores): string {
    console.log(stores);
    return '';
  }

  getNameCategory(categories: any[]): string {
    let categoriesName = [];
    this.categories.forEach(category => {
      const _category = categories.find(cat => {
        return cat === category.id;
      });
      if (_category) {
        categoriesName.push(category.name);
      }
    });
    return categoriesName.join(', ');
  }

  colapsa(tipo: any) {
    console.log(localStorage.getItem('edoAgrega'));
    let flecha = document.getElementById('flecha' + tipo);
    let elemento = document.getElementById('collapse' + tipo);
    let toggleDat = elemento.dataset.toggle;
    if (toggleDat == "up") {
      flecha.className = "fas fa-chevron-down float-right";
      elemento.setAttribute("data-toggle", "down");
      elemento.style.height = "0px";
      elemento.style.overflow = "hidden";
      elemento.style.padding = "0px";
    }
    else if (toggleDat == "down") {
      flecha.className = "fas fa-chevron-up float-right";
      elemento.setAttribute("data-toggle", "up");
      elemento.style.height = "auto";
      elemento.style.overflow = "auto";
      elemento.style.padding = ".75rem 1.25rem";
    }

  }

  async setCurrency(currency: string) {
    try {
      if (!this.loadedCurrency) {
        const response = await this.coupleService.formatedExchangeRate();
        this.currencies.CAD = response.find(data => { return data.code == 'CAD' }).value;
        this.currencies.USD = response.find(data => { return data.code == 'USD' }).value;
        this.currencies.EUR = response.find(data => { return data.code == 'EUR' }).value;
        this.loadedCurrency = true;
      }
      this.layoutService.currency = currency;
      this.layoutService.currencyChange = this.currencies[currency];
      this.currency = currency;
      this.layoutService.settings.currency = this.currency;
    } catch (e) {

    }
  }

  colapsa2(id: string, hiddenId: string) {
    let elemento = document.getElementById(id);
    let toggleDat = elemento.getAttribute('data-toggle');

    let elementoHidden = document.getElementById(hiddenId);
    let toggleDatHidden = elemento.getAttribute('data-toggle');
    if (toggleDat == "up") {
      elemento.setAttribute("data-toggle", "down");
      elemento.style.display = "none";
    }
    else if (toggleDat == "down") {
      elemento.setAttribute("data-toggle", "up");
      elemento.style.display = "";
      elementoHidden.setAttribute("data-toggle", "down");
      elementoHidden.style.display = "none";
    }

  }
  acomodoArt(idArt: string) {
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4 ArtSelec";
  }
  acomodoArt2(idArt: string) {
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4";
  }
  borrarArt(idArt: string): void {
    alert("Eliminar artículo: " + idArt);
  }
  /*editarArt(idArticulo:number){
    this.router.navigate(['/gift-table/edit/' + idArticulo]);
  }*/
  ordenPor(tipo: number) {
    alert(tipo);
  }
  onAdd() {
    this.muestAgregar = !this.muestAgregar;
  }
  iniMesa() {
    this.muestraRegalos = !this.muestraRegalos;
  }

}

