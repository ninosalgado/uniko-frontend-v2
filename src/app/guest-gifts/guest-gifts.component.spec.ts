import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestGiftsComponent } from './guest-gifts.component';

describe('GuestGiftsComponent', () => {
  let component: GuestGiftsComponent;
  let fixture: ComponentFixture<GuestGiftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestGiftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestGiftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
