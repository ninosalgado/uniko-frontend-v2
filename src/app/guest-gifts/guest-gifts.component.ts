import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../services/layout.service';
import { CoupleService } from '../services/couple.service';
import { Route } from '@angular/compiler/src/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ICoupleInformation } from '../_interface/coupleInformation';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CurrentUser } from '../_models/data';

@Component({
  selector: 'app-guest-gifts',
  templateUrl: './guest-gifts.component.html',
  styleUrls: ['../gift-table/gift-table.component.scss']
})
export class GuestGiftsComponent implements OnInit {
  isOpen: boolean = false;
  isOpenFilter: boolean = false;
  couple: ICoupleInformation;
  constructor(
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,

  ) { 
    localStorage.removeItem("currentUser");
    this.layoutService.setCurrentUser(new CurrentUser);
  }

  ngOnInit() {
    this.getInfo();
  }

  async getInfo() {
    try {
      this.spinner.show();
      localStorage.clear();
      const couple: ICoupleInformation = await this.coupleService.findOneRestrict(this.route.snapshot.parent.params.url);
      this.couple = couple;
      this.layoutService.coupleInformation.info = couple;
      this.layoutService.setCoupleInformation(couple);
      const auth = await this.coupleService.getPlanPermisions(this.layoutService.coupleInformation.info.id);
      this.layoutService.authorization.info = auth;
      if (!this.layoutService.authorization.verify('GIFT_TABLE')) {
        this.layoutService.redirect();
      }
    } catch (e) {
      this.notification.error(e);
      this.router.navigate(['/search']);
    } finally {
      this.spinner.hide();
    }
  }

}
