import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestArticuloTarjetaComponent } from './guest-articulo-tarjeta.component';

describe('GuestArticuloTarjetaComponent', () => {
  let component: GuestArticuloTarjetaComponent;
  let fixture: ComponentFixture<GuestArticuloTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestArticuloTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestArticuloTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
