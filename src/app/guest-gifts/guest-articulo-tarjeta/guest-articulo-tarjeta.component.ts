import { Component, OnInit, Input, Inject } from '@angular/core';
import { Router } from '@angular/router';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LayoutService } from 'src/app/services/layout.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-guest-articulo-tarjeta',
  templateUrl: './guest-articulo-tarjeta.component.html',
  styleUrls: ['../../gift-table/articulo-tarjeta/articulo-tarjeta.component.scss']
})

export class GuestArticuloTarjetaComponent implements OnInit {
  @Input() lock:boolean;
  @Input() index:number;
  @Input() art:any = {};
  @Input() store: string = '';
  @Input() category: string = '';
  @Input() couple: ICoupleInformation;
  constructor(
    private router:Router, 
    public dialog: MatDialog,
    private layoutService: LayoutService
  ) { 
  }

  ngOnInit() {
  }

  get currency() {
    return this.layoutService.settings.info.currency;
  }

  acomodoArt(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4 ArtSelec";
  }
  acomodoArt2(idArt:string){
    let idArticulo = document.getElementById(idArt);
    idArticulo.className = "cardArticulo card m-2 mb-4";
  }

  get formatPrice() {
    return this.art.price / this.layoutService.settings.info.currencyChange;
  }

  editarArt(){
    this.router.navigate(['/gift-table/edit',this.index]);
  }

  add(product) {
    product.quantity = 1;
    console.log(product)
    this.layoutService.cart.add(product);
    
  }

  goTo(id:string){
    this.router.navigate([this.couple.url, "gift-table",id]);
  }
}