import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../shared/shared.module';

import { Ng5SliderModule } from 'ng5-slider';
import { FormsModule } from '@angular/forms';

import { GuestGiftsComponent } from './guest-gifts.component';
import { GuestTableComponent } from './guest-table/guest-table.component';
import { GuestArticuloTarjetaComponent } from './guest-articulo-tarjeta/guest-articulo-tarjeta.component';
import { GuestArticulosTarjetasComponent } from './guest-articulos-tarjetas/guest-articulos-tarjetas.component';

import { GuestGiftsRoutingModule } from './guest-gifts-routing.module';
import { ViewGiftComponent } from './view-gift/view-gift.component';
//import { EditarTarjetaComponent } from './editar-tarjeta/editar-tarjeta.component';
//import { SolicitarBoletosComponent } from './solicitar-boletos/solicitar-boletos.component';

@NgModule({
  declarations: [
    ViewGiftComponent,
    GuestGiftsComponent,
    GuestTableComponent,
    GuestArticuloTarjetaComponent,
    GuestArticulosTarjetasComponent,    
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    Ng5SliderModule,
    GuestGiftsRoutingModule
  ]
})
export class GuestGiftsModule { }