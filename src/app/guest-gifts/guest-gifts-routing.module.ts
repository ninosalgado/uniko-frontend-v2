import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestTableComponent } from './guest-table/guest-table.component';
import { GuestGiftsComponent } from './guest-gifts.component';
import { ViewGiftComponent } from './view-gift/view-gift.component';

const routes: Routes = [
  {
    path: '',
    component: GuestGiftsComponent,
    children: [{
        path: '',
        component: GuestTableComponent
    },{
      path: ':id',
      component: ViewGiftComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestGiftsRoutingModule { }