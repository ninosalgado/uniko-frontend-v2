import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../components/header/header.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { OwlNativeDateTimeModule, OwlDateTimeModule } from 'ng-pick-datetime';
import { GuestSearchComponent } from './guest-search.component';
import { GuestSearchRouteModule } from './guest-search-routing.module';

@NgModule({
  declarations: [
    GuestSearchComponent
  ],
  imports: [
    GuestSearchRouteModule,
    FormsModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
  ]
})
export class GuestSearchModule { }
