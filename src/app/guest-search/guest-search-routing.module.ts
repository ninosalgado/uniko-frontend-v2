import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestSearchComponent } from './guest-search.component';


const routes: Routes = [{
  path: '',
  component: GuestSearchComponent
}];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestSearchRouteModule { }
