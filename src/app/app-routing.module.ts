import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './_guards/auth.guard';
import { UrlComponent } from './components/url/url.component';
import { ReferidosComponent } from './referidos/referidos.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
  },
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'gift-table',
        loadChildren: './gift-table/gift-table.module#GiftTableModule'
      }, {
        path: 'guest-book',
        loadChildren: './guest-book/guest-book.module#GuestBookModule'
      }, {
        path: 'questions',
        loadChildren: './faqs/faqs.module#FaqsModule'
      }, {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
      }, {
        path: 'guest-home',
        loadChildren: './guest-home/guest-home.module#GuestHomeModule'
      },
      {
        path: 'save-date',
        loadChildren: './save-date-2/save-date-2.module#SaveDate2Module'
      },
      {
        path: 'site',
        loadChildren: './site/site.module#SiteModule'
      }, {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }, {
        path: 'pago',
        loadChildren: './checkout-account/checkout-account.module#CheckoutAccountModule'
      }, {
        path: 'retiro',
        loadChildren: './cashout/cashout.module#CashoutModule'
      }, {
        path: 'cashout',
        loadChildren: './cash-whitraw/cash-whitraw.module#CashWhitrawModule'
      },
      {
        path: 'tools',
        loadChildren: './wedding-tools/wedding-tools.module#WeddingToolsModule'
      }
    ]
  },
  {
    path: 'sura',
    loadChildren: './sura/sura.module#SuraModule'
  },
  {
    path: '',
    children: [
      {
        path: 'boda',
        loadChildren: './guest-gifts/guest-gifts.module#GuestGiftsModule'
      },
      {
        path: 'onboarding',
        loadChildren: './onboarding/onboarding.module#OnboardingModule'
      },
      {
        path: 'search',
        loadChildren: './guest-search/guest-search.module#GuestSearchModule'
      },
      {
        path: 'referidos/:name',
        component: ReferidosComponent
      },
      {
        path: 'plans',
        loadChildren: './guest-plan/guest-plan.module#GuestPlanModule'
      }
    ]
  },
  {
    path: ':url',
    children: [
      {
        path: '',
        component: UrlComponent
      },
      {
        path: 'guest-book',
        loadChildren: './guest-book/guest-book.module#GuestBookModule'
      },
      {
        path: 'site',
        loadChildren: './guest-site/guest-site.module#GuestSiteModule'
      },
      {
        path: 'gift-table',
        loadChildren: './guest-gifts/guest-gifts.module#GuestGiftsModule'
      },
      {
        path: 'checkout2',
        loadChildren: './canasta-compras/canasta-compras.module#CanastaComprasModule'
      },
      {
        path: 'checkout',
        loadChildren: './checkout/checkout.module#CheckoutModule'
      },
      {
        path: 'questions',
        loadChildren: './guest-faqs/guest-faqs.module#GuestFaqsModule'
      },
      {
        path: 'save-date',
        loadChildren: './guest-save-the-date/guest-save-the-date.module#GuestSaveTheDateModule'
      },
    ]
  },
  {
    path: 'confirmation/:id',
    loadChildren: './guest-confirmation/guest-confirmation.module#GuestConfirmationModule'
  }, {
    path: '**',
    redirectTo: 'onboarding/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      preloadingStrategy: PreloadAllModules
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
