import { Component, OnInit, Input } from '@angular/core';
import { CoupleService } from '../services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-referidos',
  templateUrl: './referidos.component.html',
  styleUrls: ['./referidos.component.scss']
})
export class ReferidosComponent implements OnInit {
  @Input() menuKey: string;
  name: string;
  fullname: string;
  constructor(
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private notification: ToastrService,
    private route: ActivatedRoute
  ) { 
    this.name = this.route.snapshot.params.name;
    this.getReferal();
  }

  ngOnInit() {

  }

  mapMenuSelected(key){
    if(this.menuKey){
      return this.menuKey == key;
    }
  }

  async getReferal() {
    try {
      this.spinner.show();
      const data = await this.coupleService.getReferal(this.name);
      console.log(data);
      this.fullname = data.referal.fullName;
    } catch(e) {

    } finally {
      this.spinner.hide();
    }
  }

}
