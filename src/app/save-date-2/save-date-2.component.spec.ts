import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveDateComponent2 } from './save-date-2.component';

describe('SaveDateComponent', () => {
  let component: SaveDateComponent2;
  let fixture: ComponentFixture<SaveDateComponent2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveDateComponent2 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveDateComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
