import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaveDateComponent2 } from './save-date-2.component';
import { EditorComponent } from './editor/editor.component';
import { ViewerComponent } from './viewer/viewer.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
{
  path: '',
  children: [
  {
    path: '',
    component: ListComponent
  },
  {
    path: 'template/:id',
    component: ViewerComponent
  },
  {
    path: 'template/edit/:id',
    component: EditorComponent
  }],
  component: SaveDateComponent2,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaveDate2RoutingModule { }
