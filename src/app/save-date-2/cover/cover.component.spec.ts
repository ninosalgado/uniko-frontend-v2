import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveDateCoverComponent } from './cover.component';

describe('SaveDateCoverComponent', () => {
  let component: SaveDateCoverComponent;
  let fixture: ComponentFixture<SaveDateCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveDateCoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveDateCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
