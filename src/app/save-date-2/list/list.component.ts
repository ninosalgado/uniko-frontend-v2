import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SaveDateService } from '../../services/save-date.service';
import { LayoutService } from '../../services/layout.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  saveDate: any;
  isSelected: boolean = false;
  constructor(
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private saveDateService: SaveDateService,
    private layoutService: LayoutService,
  ) {
    this.getInfo();
  }

  ngOnInit() {
  }

  getType() {
    switch (this.layoutService.getCoupleInformation().weddingData.type) {
      case 'WEDDING':
        return '_wedding';
      case 'BABY':
        return '_baby';
      case 'BIRTHDAY':
        return '_birthday';
      default:
        return '_wedding';
    }
  }

  async getInfo() {
    try {
      this.spinner.show();
      this.saveDate = await this.saveDateService.getSaveDate(this.layoutService.getCoupleInformation().id);
      if (this.saveDate) {
        this.isSelected = true;
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
