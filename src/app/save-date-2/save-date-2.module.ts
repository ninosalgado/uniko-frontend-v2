import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveDate2RoutingModule } from './save-date-2-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material.module';

import { SaveDateComponent2 } from './save-date-2.component';
import { EditorComponent } from './editor/editor.component';
import { ToolsComponent } from './tools/tools.component';
import { ContentComponent } from './content/content.component';
import { TemplatesComponent } from './content/templates/templates.component';
import { ViewerComponent } from './viewer/viewer.component';
import { ListComponent } from './list/list.component';

import { ColorSketchModule } from 'ngx-color/sketch';
import { FormsModule } from '@angular/forms';
import { SaveDateCoverComponent } from './cover/cover.component';

@NgModule({
  declarations: [
    SaveDateComponent2,
    EditorComponent, 
    ToolsComponent, 
    SaveDateCoverComponent, 
    ContentComponent, 
    TemplatesComponent, 
    ViewerComponent, 
    ListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    MaterialModule,
    ColorSketchModule,
    SaveDate2RoutingModule
  ]
})
export class SaveDate2Module { }
