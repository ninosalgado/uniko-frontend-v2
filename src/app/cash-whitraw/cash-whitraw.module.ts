import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { CashWhitrawComponent } from './cash-whitraw.component';
import { CreditComponent } from './credit/credit.component';
import { CashWhitrawRoutingModule } from './cash-whitraw.routing.module';
import { ConfirmComponent } from './confirm/confirm.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CongratsComponent } from './congrats/congrats.component';
import { RequestStepComponent } from './request-step/request-step.component';
import { StepsComponent } from './steps/steps.component';

@NgModule({
  declarations: [
    CashWhitrawComponent, 
    CreditComponent,
    ConfirmComponent,
    CongratsComponent,
    RequestStepComponent,
    StepsComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    CashWhitrawRoutingModule,
    SharedModule
  ]
})
export class CashWhitrawModule { }
