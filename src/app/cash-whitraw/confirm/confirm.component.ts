import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Cashout } from 'src/app/_models/cashout';
import { ICashout, ICredit } from 'src/app/_interface/cashout';
import { CoupleService } from 'src/app/services/couple.service';
import { CoupleInformation } from 'src/app/_models/coupleInformation';
import { FormGroup } from '@angular/forms';
import { KEYS } from 'src/environments/environment';
import { OrderService } from 'src/app/services/order.service';
import { Token } from '@angular/compiler';

declare var Conekta: any;

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  couple: CoupleInformation;
  cashoutForm: FormGroup;
  years: number[] = [];
  months: string[] = [];
  credit: ICredit;
  commision: any;
  commissionSet: boolean = false;
  paymentAmount:number;
  constructor(
    private layoutService: LayoutService,
    private router: Router,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private orderService: OrderService,
  ) {    
    this.couple = this.layoutService.coupleInformation;
  }

  ngOnInit() {
    this.credit = this.layoutService.credit.info;
    if(!this.credit || !this.credit.amount){
      this.goto('credit-request');
    }
    this.getCommission();
  }

  goto(place){
    this.router.navigate(['cashout',place]);
  }

  async getCommission(){
    try{
      console.log(this.credit);
      this.spinner.show();
      const response: any = await this.coupleService.utilsComissionCredit();
      this.commision = response.value;
      this.paymentAmount = (this.credit.amount + (this.credit.amount * (this.commision / 100))) / this.credit.msi;
    }catch(e){
      console.log(e);
    }finally{
      this.spinner.hide();
      this.commissionSet = true;
    }
  }

  async save() {
    try {
      this.spinner.show();
      //const country: any = this.countries.find(data => data.phone == this.registerForm.value.country);
      
      const response = await this.coupleService.payCredit(
        this.credit.msi,
        this.credit.token,
        this.credit.cardNumber,
        this.credit.amount,
        'cdmx',
        this.credit.type,
        this.credit.fullname
        
      );

      this.layoutService.credit.clear();
      this.router.navigate(['/cashout/credit-congratulations']);
    } catch(e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
