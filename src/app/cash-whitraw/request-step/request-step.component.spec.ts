import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestStepComponent } from './request-step.component';

describe('RequestStepComponent', () => {
  let component: RequestStepComponent;
  let fixture: ComponentFixture<RequestStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
