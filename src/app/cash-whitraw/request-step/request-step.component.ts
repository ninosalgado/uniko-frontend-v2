import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { Router } from '@angular/router';
import { CoupleInformation } from 'src/app/_models/coupleInformation';
import { ICredit } from 'src/app/_interface/cashout';

@Component({
  selector: 'app-request-step',
  templateUrl: './request-step.component.html',
  styleUrls: ['./request-step.component.scss']
})
export class RequestStepComponent implements OnInit {
  monthsMSI: any[] = [];
  couple: CoupleInformation;
  requestForm: FormGroup;
  credit: ICredit;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private orderService: OrderService,
    private router: Router
  ) { 
    this.couple = this.layoutService.coupleInformation;
    this.credit = this.layoutService.credit.info;
  }

  ngOnInit() {    
    this.monthsMSI.push({msi: '1',label:'mes'}, {msi: '3', label:'meses'}, {msi: '6', label:'meses'}, {msi: '12', label:'meses'});
    this.requestForm = this.formBuilder.group({
      amount: [2000, Validators.required],
      msi: [1, Validators.required]
    });
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  setStepOne(){
    try{
      this.markFormGroupTouched(this.requestForm);
      if(this.requestForm.invalid){
        return;
      }
      this.layoutService.credit.info = {
        amount: this.requestForm.value.amount,
        comission: 0,
        cardNumber: this.credit?this.credit.cardNumber:'',
        msi: this.requestForm.value.msi
      }
      this.goto('credit-info');
    }catch(e){
      console.log(e);
    }
  }

  goto(place){
    this.router.navigate(['cashout',place]);
  }
}
