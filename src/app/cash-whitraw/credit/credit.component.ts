import { Component, OnInit } from '@angular/core';
import { CoupleInformation } from 'src/app/_models/coupleInformation';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { Router } from '@angular/router';
import { ICredit } from 'src/app/_interface/cashout';
import { CoupleService } from 'src/app/services/couple.service';
declare var Conekta: any;
import { Token } from '@angular/compiler';
import { KEYS } from 'src/environments/environment';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.scss']
})
export class CreditComponent implements OnInit {
  couple: CoupleInformation;
  cashoutForm: FormGroup;
  years: number[] = [];
  months: string[] = [];
  credit: ICredit;
  commision: any;
  commissionSet: boolean = false;
  type: any = {
    type: '',
    card: 16,
    cvc: 3
  }
  types: any = [{
    type: 'visa',
    card: 16,
    cvc: 3
  }, {
    type: 'mastercard',
    card: 16,
    cvc: 3
  }, {
    type: 'amex',
    card: 15,
    cvc: 4
  }]
  constructor(
    private spinner: NgxSpinnerService,
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private router: Router,
    private notification: ToastrService,
    private coupleService: CoupleService
  ) {
    this.couple = this.layoutService.coupleInformation;
    this.credit = this.layoutService.credit.info;
    const year = new Date().getFullYear();
    this.years = Array.from({ length: 11 }, (v, i) => year + i)
    this.months = Array.from({ length: 12 }, (v, i) => String(i + 1));
    Conekta.setPublicKey(KEYS.CONEKTA_GUEST);
    this.getComissions();
  }

  ngOnInit() {
    console.log(this.credit);
    if (!this.credit || !this.credit.amount) {
      this.goto('credit-request');
    }
    this.cashoutForm = this.formBuilder.group({
      fullname: [this.credit ? this.couple.info.weddingData.nameP1 : '', Validators.required],
      card: [this.credit ? this.credit.cardNumber : '', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      cvc: ['', Validators.required],
    });
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }


  get getAmountPayment() {
    let amount = this.credit.amount;
    amount = (this.credit.amount + (this.credit.amount * this.commision / 100)) / this.credit.msi;
    return amount;
  }

  async getComissions() {
    try {
      this.spinner.show();
      const response: any = await this.coupleService.utilsComissionCredit();
      console.log(response);
      this.commision = response.value;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  validCard() {
    const first = this.cashoutForm.value.card.slice(0, 1);
    switch (first) {
      case '4':
        if (this.type.type !== 'visa') {
          this.type = this.types.find(data => data.type == 'visa');
        }
        break;
      case '3':
        if (this.type.type !== 'amex') {
          this.type = this.types.find(data => data.type == 'amex');
        }
        break;
      case '5':
        if (this.type.type !== 'mastercard') {
          this.type = this.types.find(data => data.type == 'mastercard');
        }
        break;
      default:
    }
    console.log(this.type);
  }

  async setStepOne() {
    try {
      this.markFormGroupTouched(this.cashoutForm);
      console.log(this.cashoutForm.invalid);
      if (this.cashoutForm.invalid) {
        return;
      }

      this.validCard();
      const token = {
        card: {
          number: this.cashoutForm.value.card,
          name: this.cashoutForm.value.fullname,
          exp_year: this.cashoutForm.value.year,
          exp_month: this.cashoutForm.value.month,
          cvc: this.cashoutForm.value.cvc
        }
      }
      const card = String(this.cashoutForm.value.card)
      const response = await this.createToken(token);
      this.layoutService.credit.info = {
        amount: this.credit.amount,
        cardNumber: card.slice(
          card.length - 4,
          card.length
        ),
        msi: this.credit.msi,
        fullname: this.cashoutForm.value.fullname,
        type: this.type,
        token: response.id
      }
      this.goto('credit-confirmation');
    } catch (e) {
      console.log(e);
    } finally {
    }
  }

  async createToken(card): Promise<any> {
    return new Promise((resolve, reject) => {
      Conekta.token.create(card, (token: Token) => {
        return resolve(token);
      }, (error) => {
        reject(error.message_to_purchaser);
      })
    });
  }

  goto(place) {
    this.router.navigate(['cashout', place]);
  }

}
