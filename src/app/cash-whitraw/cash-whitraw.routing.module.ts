import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CashWhitrawComponent } from './cash-whitraw.component';
import { CreditComponent } from './credit/credit.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { CongratsComponent } from './congrats/congrats.component';
import { RequestStepComponent } from './request-step/request-step.component';


const routes: Routes = [{
  path: '',
  component: CashWhitrawComponent,
  children: [{
    path: 'credit-request',
    component: RequestStepComponent
  },{
    path: 'credit-info',
    component: CreditComponent
  }, {
    path: 'credit-confirmation',
    component: ConfirmComponent
  }, {
    path: 'credit-congratulations',
    component: CongratsComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWhitrawRoutingModule { }
