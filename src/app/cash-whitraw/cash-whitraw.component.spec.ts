import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWhitrawComponent } from './cash-whitraw.component';

describe('CashWhitrawComponent', () => {
  let component: CashWhitrawComponent;
  let fixture: ComponentFixture<CashWhitrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashWhitrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashWhitrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
