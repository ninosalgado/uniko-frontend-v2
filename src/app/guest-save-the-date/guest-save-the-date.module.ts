import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { GuestSaveTheDateRoutingModule } from './guest-save-the-date-routing';
import { GuestSaveTheDateComponent } from './guest-save-the-date.component';

import { ContentComponent } from './content/content.component';
import { TemplatesComponent } from './content/templates/templates.component';
import { SaveDateCoverComponent } from './cover/cover.component';

@NgModule({
  declarations: [
    GuestSaveTheDateComponent,    
    ContentComponent,
    SaveDateCoverComponent,
    TemplatesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GuestSaveTheDateRoutingModule
  ]
})
export class GuestSaveTheDateModule { }
