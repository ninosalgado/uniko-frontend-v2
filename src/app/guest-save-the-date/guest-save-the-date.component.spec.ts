import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestSaveTheDateComponent } from './guest-save-the-date.component';

describe('GuestSaveTheDateComponent', () => {
  let component: GuestSaveTheDateComponent;
  let fixture: ComponentFixture<GuestSaveTheDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestSaveTheDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestSaveTheDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
