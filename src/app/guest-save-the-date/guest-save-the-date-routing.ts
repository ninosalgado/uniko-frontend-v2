import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestSaveTheDateComponent } from './guest-save-the-date.component';


const routes: Routes = [{
    path: '',
    component: GuestSaveTheDateComponent,
  }];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class GuestSaveTheDateRoutingModule { }