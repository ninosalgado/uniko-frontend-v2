import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cover',
  templateUrl: './cover.component.html',
  styleUrls: ['./cover.component.scss']
})
export class SaveDateCoverComponent implements OnInit {
  @Input() content: any;
  @Input() templateId: any;
  @Input() cover: any;
  @Input() proxy: boolean = false;
  constructor() { }

  ngOnInit() {

  }

  getProxy() {
    if (this.proxy) {
      return this.cover;
      let fragment = this.cover.split('couple');
      return '/proxy/couple' + fragment[1];
    } else {
      return this.cover;
    }
  }

}
