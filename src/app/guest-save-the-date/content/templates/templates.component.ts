import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.scss']
})
export class TemplatesComponent implements OnInit {
  @Input() content: any;
  @Input() templateId: any;
  @Input() loadComplete: boolean;
  constructor() { }

  ngOnInit() {

  }

  get img() {
    const img = this.content.backgroundTemplate.find(data => data.select);
    console.log(this.content.backgroundTemplate);
    console.log(img);
    return `${this.content.name}${img ? img.index : ''}.png`;
  }

  get getHeight() {
    if (!this.loadComplete || !this.content) {
      return 500;
    }
    let elm = document.getElementById('ornate-container');
    return elm.offsetHeight;
  }

  get getwidth() {
    if (!this.loadComplete || !this.content) {
      return 500;
    }
    let elm = document.getElementById('ornate-container');
    return elm.offsetWidth;
  }

  get background() {
    const color = this.content.colors.find(data => data.select);
    return color ? color.color : '#fff';
  }

  get ornateColor() {
    const color = this.content.backgroundTemplate.find(data => data.select);
    return color ? color.color : '#fff';
  }

  specialTemplate(template?) {
    const specialTemplates = ["3", "5", "2", "4"];
    const flag = specialTemplates.indexOf(this.templateId) >= 0 ? true : false;
    return flag;
  }

}
