import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoupleService } from '../services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { LayoutService } from '../services/layout.service';
import { SaveDateService } from '../services/save-date.service';
import { ISaveDate } from '../_interface/sateDate';
import { ICoupleInformation } from '../_interface/coupleInformation';
import { TEMPLATES } from '../_const/templates';
import { Button, CurrentUser } from '../_models/data';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-guest-save-the-date',
  templateUrl: './guest-save-the-date.component.html',
  styleUrls: ['./guest-save-the-date.component.scss']
})
export class GuestSaveTheDateComponent implements OnInit {
  url: string;
  saveDate: ISaveDate;
  couple: ICoupleInformation;
  templateObj: any;
  buttons: Button[] = [];
  load: boolean = false;
  listBackround = TEMPLATES;
  cover: string;
  proxy: boolean = false;
  baseCover: string;
  constructor(
    private route: ActivatedRoute,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private layoutService: LayoutService,
    private router: Router,
    private saveDateService: SaveDateService,
    private datePipe: DatePipe
  ) {
    localStorage.removeItem("currentUser");
    this.layoutService.setCurrentUser(new CurrentUser);
    this.url = this.route.snapshot.parent.params.url;
  }

  ngOnInit() {
    console.log("initializing")
    this.getInfo();
  }


  getEventString() {
    switch (this.couple.weddingData.type) {
      case 'BABY':
        return '_baby';
      case 'WEDDING':
      default:
        return '';
    }
  }

  goRoute(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    this['parent'].router.navigate(['/', this['parent'].couple.url, 'guest-book'],
      { queryParams: { comment: 1 } }
    )
  }

  selectColor(index) {
    this.templateObj.colors = this.templateObj.colors.map(data => {
      if (data.index == index) {
        data.select = true;
        data.color = this.saveDate.colors.color;
      } else {
        data.select = false;
      }
      return data;
    });
  }
  selectTemplateColor(index) {
    this.templateObj.backgroundTemplate = this.templateObj.backgroundTemplate.map(data => {
      if (data.index == index) {
        data.select = true;
        data.color = this.saveDate.backgroundTemplate.color;
      } else {
        data.select = false;
      }
      return data;
    });
  }

  async getInfo() {
    try {
      this.spinner.show();
      this.load = false;
      this.couple = await this.coupleService.findOneRestrict(this.url);
      const buttonShared = new Button(
        'Escriba una dedicatoria',
        this.goRoute,
        'fa fa-comment',
        null,
        this
      );
      this.buttons = [buttonShared]
      if (this.couple) {
        this.layoutService.setCoupleInformation(this.couple);
      } else {
        this.notification.warning('No se encontro el evento');
        this.router.navigate(['/search']);
        return false;
      }
      this.saveDate = await this.saveDateService.getSaveDate(this.couple.id);
      if (!this.saveDate) {
        this.notification.warning('Este evento aun no cuenta con Save The Date');
        this.router.navigate(['/search']);
        return false;
      }
      this.baseCover = './../../../assets/img/templates/foto' + this.saveDate.templateId + this.getEventString() + '.jpg';
      console.log(this.saveDate);

      const template = Object.assign(this.listBackround[this.saveDate.templateId]);
      this.templateObj = JSON.parse(JSON.stringify(template));

      if (this.saveDate.templateId != 6) {
        this.templateObj.title.text = this.saveDate.title.text;
        this.templateObj.nametitle.text = this.saveDate.nametitle.text;
        this.templateObj.titleDate.text = this.saveDate.titleDate.text;
        this.templateObj.titleDescription.text = this.saveDate.titleDescription.text;
        this.templateObj.description.text = this.saveDate.description.text;
        this.templateObj.date.text = this.saveDate.date.text;
        this.templateObj.description.text = this.saveDate.description.text;
      }
      this.templateObj.desktop = this.saveDate.desktop;
      if (this.saveDate.cover) {
        this.proxy = true;
        this.cover = this.saveDate.cover;
      } else {
        this.cover = this.baseCover;
      }
      this.load = true;

    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  getSelectedIndex(color) {
    if (Array.isArray(color)) {
      return color.find(data => data.select);
    } else {
      return color
    }

  }

  get img() {
    const img = this.templateObj ? this.templateObj.backgroundTemplate.find(data => data.select) : null;
    return `${this.templateObj.name}${img ? img.index : ''}.png`;
  }

  get mode() {
    return this.templateObj.desktop;
  }
}
