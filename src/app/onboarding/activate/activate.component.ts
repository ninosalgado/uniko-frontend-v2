import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.scss']
})
export class ActivateComponent implements OnInit {
  coupleInformation: ICoupleInformation;
  mdCode:string;
  userPhone:string;
  isActive:boolean;
  code:string = '';
  change: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.coupleInformation = this.layoutService.getCoupleInformation();
    this.userPhone = this.coupleInformation.phoneNumber1;
    this.isActive = this.coupleInformation.is2FA;
    this.router.navigate(['/']);
   }

  ngOnInit() {
    console.log(this.coupleInformation);
    // this.sendValidationCode();
  }

  async sendValidationCode(){
    try {
      this.spinner.show();
     await this.coupleService.sendValidationCode(this.coupleInformation.id,this.userPhone);   
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();

    }
  }

  send() {
    if (this.change) {
      this.sendValidationCode();
    } else {
      this.getValidationCode();
    }
  }

  async getValidationCode(){
    this.spinner.show();
    let couple:any;
    try{
     couple = await this.coupleService.verifyValidationCode(this.coupleInformation.id,this.code);
     this.change = false;
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.show();
      await this.coupleService.updateRegister(couple);
      this.layoutService.setCoupleInformation(couple);
      this.router.navigate(['/', 'gift-table']);
    }
  }
}
