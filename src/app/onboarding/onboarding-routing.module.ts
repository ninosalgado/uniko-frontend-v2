import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnboardingComponent } from './onboarding.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from '../_guards/auth.guard';
import { RecoveryComponent } from './recovery/recovery.component';
import { Step2Component } from './step2/step2.component';
import { Step1Component } from './step1/step1.component';
import { Step3Component } from './step3/step3.component';
import { ResetComponent } from './reset/reset.component';
import { ActivateComponent } from './activate/activate.component';
import { Step0Component } from './step0/step0.component';

const routes: Routes = [{
  path: '',
  component: OnboardingComponent,
  children: [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'login'
    },
    {
      path: 'pre-register',
      component: Step0Component
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'recovery',
      component: RecoveryComponent
    }, {
      path: 'reset/:token',
      component: ResetComponent
    },
    {
      path: 'register',
      redirectTo: 'boda/register'
    },
    {
      path: ':id',
      children: [
        {
          path: 'register',
          component: RegisterComponent
        },
        {
          path: '1',
          canActivate: [AuthGuard],
          component: Step1Component
        },
        {
          path: '2',
          canActivate: [AuthGuard],
          component: Step2Component
        },
        {
          path: '3',
          canActivate: [AuthGuard],
          component: Step3Component
        },
        {
          path: 'activate',
          canActivate: [AuthGuard],
          component: ActivateComponent
        }
      ]
    },
  ]
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardingRoutingModule { }
