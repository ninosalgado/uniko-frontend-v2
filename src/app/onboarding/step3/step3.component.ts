import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements OnInit {
  coupleaccount: ICoupleInformation;
  submitted = false;
  experiences: any[] = [];
  eventType: string;
  eventUrl: string = 'boda';
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.coupleaccount = this.layoutService.getCoupleInformation();
    this.getExperiences();
    this.layoutService.eventOnboarding('3');
    this.eventUrl = activatedRoute.snapshot.params.id;
    this.eventType = this.getEventTypeString();
    console.log('here: ', this.eventType);
  }

  ngOnInit() {

  }

  add(experience: any) {
    experience.selected = !experience.selected;
  }

  getEventTypeString() {
    switch (this.eventUrl) {
      case 'boda':
        return 'WEDDING'
        break;
      case 'babyshower':
        return 'BABY'
        break;
      case 'cumple':
        return 'BIRTHDAY'
        break;
    }
    return 'WEDDING';
  }

  getEventString() {
    switch (this.eventType) {
      case 'WEDDING':
        return 'de la boda'
        break;
      case 'BABY':
        return 'del babyshower'
        break;
      case 'BIRTHDAY':
        return 'del cumpleaños'
        break;
    }
    return 'de su boda';
  }

  getTitle() {
    switch (this.eventType) {
      case 'WEDDING':
        break;
      case 'BABY':
      case 'BIRTHDAY':
        return 'Queremos ayudarte en el armado de tu mesa'
        break;
      default:
        return 'Queremos ayudarlos en el armado de su mesa'
    }
  }

  getMessage() {
    switch (this.eventType) {
      case 'WEDDING':
        return 'Estas son algunas sugerencias de mesas que pueden elegir. Si seleccionan alguna, se agregarán los regalos a su mesa, podrán quitarlos o agregar nuevos regalos.'
        break;
      case 'BABY':
      case 'BIRTHDAY':
        return 'Estas son algunas sugerencias que puedes elegir. Si seleccionas alguna, se agregarán los regalos a tu mesa, podrás quitarlos o agregar nuevos regalos.'
        break;
      default:
        return 'Estas son algunas sugerencias de mesas que pueden elegir. Si seleccionan alguna, se agregarán los regalos a su mesa, podrán quitarlos o agregar nuevos regalos.'
    }
  }

  async getExperiences(): Promise<void> {
    try {
      this.spinner.show();
      this.experiences = await this.coupleService.getExperiences(this.coupleaccount.weddingData.type);
      this.experiences.forEach(data => {
        data.selected = false
      });
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  selectedProducts(products: any[], _products: any[]): any {
    const limit = 5;
    const max = products.length - 1;
    if (max < limit && !_products.length) {
      return products;
    }
    if (_products.length === limit) {
      return _products;
    }
    const index = Math.round(Math.random() * (max - 0) + 0);
    let product = products.splice(index, 1);
    product = product.length ? product[0] : {};
    _products.push(product);
    return this.selectedProducts(products, _products);
  }

  selectedExperiences(experiences: any[], products: any[]) {
    if (!experiences.length) {
      return products;
    }
    const experience = experiences.pop();
    const _products = this.selectedProducts(experience.productsRegistryList, []);
    _products.forEach(p => products.push(p));
    return this.selectedExperiences(experiences, products);
  }

  async next(add: boolean): Promise<void> {
    try {
      this.spinner.show();
      if (add) {
        const experiences = this.experiences.filter(e => e.selected);
        const products = this.selectedExperiences(experiences, []);
        this.coupleaccount.productsRegistryList = products;
        await this.coupleService.setExperiences(this.coupleaccount.id, products);
      }
      this.coupleaccount.onboardingStep = 4;
      const couple = await this.coupleService.updateRegister(this.coupleaccount);
      this.layoutService.setCoupleInformation(couple);
      if (localStorage.getItem('type')) {
        switch (localStorage.getItem('type')) {
          case 'save-date':
            this.router.navigate(['/save-date']);
            break;
          case 'mesa-regalos':
            this.router.navigate(['/gift-table']);
            break;
          case 'web-boda':
            this.router.navigate(['/site']);
            break;
          default:
            this.router.navigate(['/site']);
        }
      } else {
        this.router.navigate(['/site']);
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }
}
