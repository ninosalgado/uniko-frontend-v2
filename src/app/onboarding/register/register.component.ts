import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router, ActivatedRoute } from '@angular/router';
import { COUNTRIES_PHONE } from 'src/app/_const/country-phones';
import { CoupleService } from 'src/app/services/couple.service';
import { CurrentUser } from 'src/app/_models/data';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  countries = COUNTRIES_PHONE;
  eventType: string;
  eventUrl: string = 'boda';
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.layoutService.eventOnboarding('register');
    localStorage.clear();
    if (this.route.snapshot.queryParams.type) {
      localStorage.setItem('type', this.route.snapshot.queryParams.type);
    }
    this.eventUrl = route.snapshot.params.id;
    this.eventType = this.getEventTypeString();
    console.log('here: ', this.eventType);
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', this.verifyEmail],
      password: ['', Validators.required],
      phone: ['', Validators.required],
      country: ['0052', Validators.required]
    })
  }

  getEventTypeString() {
    switch (this.eventUrl) {
      case 'boda':
        return 'WEDDING'
        break;
      case 'babyshower':
        return 'BABY'
        break;
      case 'cumple':
        return 'BIRTHDAY'
        break;
    }
    return 'WEDDING';
  }

  verifyEmail(control: AbstractControl) {
    const emails = control.value;
    let error = null;
    if (!emails.length) {
      return { ...error, email: 'El correo es requerido' };
    }
    const valid = /^\w+([\.-]|[+]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    if (!valid.test(emails)) {
      error = { ...error, email: 'El email son inválidos' };
    }
    return error;
  }

  get f() { return this.registerForm.controls; }


  terms(event: Event) {

  }

  async register(): Promise<void> {
    try {
      this.spinner.show();
      this.submitted = true;
      if (this.registerForm.invalid) {
        return;
      }
      const couple = await this.coupleService.register(
        this.registerForm.value.email,
        this.registerForm.value.phone,
        this.registerForm.value.country,
        this.registerForm.value.password
      );
      const data: any = {
        created: couple.token.created,
        id: couple.token.id,
        role: 'couple',
        ttl: couple.token.ttl,
        user: couple.coupleAccount
      }
      if (this.route.snapshot.queryParams.referal) {
        data.referal = this.route.snapshot.queryParams.referal;
      }
      const current = new CurrentUser(data);
      this.layoutService.setCoupleInformation(couple.coupleAccount);
      this.layoutService._setCurrentUser(current);
      this.router.navigate(['/onboarding', this.eventUrl, '1']);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
