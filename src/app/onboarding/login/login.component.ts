import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
import { LayoutService } from 'src/app/services/layout.service';
import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/services/notifications.service';
import { CurrentUser } from 'src/app/_models/data';
import { URL_RESOURCES } from 'src/environments/environment.dev';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  global = URL_RESOURCES.GLOBAL;
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private router: Router
  ) {
    if (localStorage.getItem('currentUser')) {
      this.router.navigate(['/']);
    }
    localStorage.clear();
    this.layoutService.eventOnboarding('login');
  }

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() { return this.loginForm.controls; }


  async login(): Promise<void> {
    try {
      this.submitted = true;
      this.spinner.show();
      if (this.loginForm.invalid) {
        return;
      }
      const user = await this.userService.login(
        this.loginForm.value.email,
        this.loginForm.value.password
      );
      const current = new CurrentUser(user);
      this.layoutService._setCurrentUser(current);
      this.layoutService.setCoupleInformation(current.user);
      if (!this.layoutService.getUser().is2FA) {
        this.router.navigate(['/', 'gift-table']);
      } else if (this.layoutService.getUser().onboardingStep < 4) {
        this.router.navigate(['/onboarding', this.layoutService.getUser().onboardingStep])
      } else if (this.layoutService.getUser().onboardingStep > 3) {
        this.router.navigate(['/', 'gift-table']);
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }
}
