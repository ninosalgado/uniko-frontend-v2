import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router, ActivatedRoute } from '@angular/router';
import { isDate } from '@angular/common/src/i18n/format_date';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
  onboardingForm: FormGroup;
  submitted = false;
  myDate = new Date();
  coupleAccount: ICoupleInformation;
  eventType: string;
  eventUrl: string = 'boda';
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.coupleAccount = this.layoutService.getCoupleInformation();
    this.layoutService.eventOnboarding('1');
    this.eventUrl = activatedRoute.snapshot.params.id;
    this.eventType = this.getEventTypeString();
    console.log('here: ', this.eventType);
  }

  ngOnInit() {
    switch (this.eventType) {
      case 'WEDDING':
        this.onboardingForm = this.formBuilder.group({
          name: [this.coupleAccount.weddingData.nameP1, Validators.required],
          name2: [this.coupleAccount.weddingData.nameP2, Validators.required],
          date: [this.coupleAccount.weddingData.date],
          isDate: [this.coupleAccount.weddingData.date ? false : true],
        });
        break;
      case 'BABY':
        this.onboardingForm = this.formBuilder.group({
          name: [this.coupleAccount.weddingData.nameP1, Validators.required],
          date: [this.coupleAccount.weddingData.date],
          isDate: [this.coupleAccount.weddingData.date ? false : true],
        });
        break;
    }
    this.onChanges();
  }

  getEventTypeString() {
    switch (this.eventUrl) {
      case 'boda':
        return 'WEDDING'
        break;
      case 'babyshower':
        return 'BABY'
        break;
      case 'cumple':
        return 'BIRTHDAY'
        break;
    }
    return 'WEDDING';
  }

  getEventString() {
    switch (this.eventType) {
      case 'WEDDING':
        return 'de la boda'
        break;
      case 'BABY':
        return 'del baby shower'
        break;
      case 'BIRTHDAY':
        return 'del cumpleaños'
        break;
    }
    return 'de su boda';
  }

  onChanges() {
    this.onboardingForm.get('date').valueChanges.subscribe(value => {
      if (value || value != null) {
        this.onboardingForm.patchValue({
          isDate: false
        });
      }
    });

    this.onboardingForm.get('isDate').valueChanges.subscribe(value => {
      if (value) {
        this.onboardingForm.patchValue({
          date: null
        });
      }
    });
  }

  IsValidJson(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  async next(): Promise<void> {
    try {
      this.submitted = true;
      if (this.onboardingForm.invalid) {
        console.log('invalid');
        console.log(this.onboardingForm);
        return;
      }
      this.spinner.show();

      this.coupleAccount.weddingData.nameP1 = this.onboardingForm.value.name;
      this.coupleAccount.weddingData.nameP2 = this.onboardingForm.value.name2;
      this.coupleAccount.onboardingStep = 2;
      this.coupleAccount.weddingData.type = this.eventType;
      let formatedDate = this.onboardingForm.value.date;

      if (this.IsValidJson(this.onboardingForm.value.date)) {
        if (!this.onboardingForm.value.isDate) {
          let _json = this.onboardingForm.value.date.toJSON();
          formatedDate = _json.toDateString();
        }
      }

      if (this.onboardingForm.value.isDate) {
        this.coupleAccount.weddingData.date = null;
      } else {
        this.coupleAccount.weddingData.date = formatedDate;
      }

      const couple = await this.coupleService.updateRegister(this.coupleAccount);
      this.layoutService.setCoupleInformation(couple);
      this.router.navigate(['/onboarding', this.eventUrl, '2']);
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
