import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router } from '@angular/router';
import { isDate } from '@angular/common/src/i18n/format_date';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-step0',
  templateUrl: './step0.component.html',
  styleUrls: ['./step0.component.scss']
})
export class Step0Component implements OnInit {
  submitted = false;
  myDate = new Date();
  type="WEDDING";
  types: any = [{
    type: 'Boda',
    code: 'WEDDING',
    url: 'boda',
  },{
    type: 'Babyshower',
    code: 'BABY',
    url: 'baby'
  }];
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private router: Router
  ) { 
  }

  
  ngOnInit() {

  }

  getSelectedUrl(){
    let found = this.types.find(type=> {return type.code==this.type});
    return found.url.toLowerCase();
  }

  getSelectedString(){
    let found = this.types.find(type=> {return type.code==this.type});
    return found.type.toLowerCase();
  }
}
