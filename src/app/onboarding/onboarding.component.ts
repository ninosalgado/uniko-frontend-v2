import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../services/layout.service';
import { ActivatedRoute } from '@angular/router';
import { AnonymousSubject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent implements OnInit {
  title: string = '¡Felicidades por su compromiso!';
  subtitle: string = 'Qué gusto tenerlos de vuelta';
  image: string = 'register_default.jpg';
  sub: any;
  route: any;
  type: string = 'login';
  typeEvent: string = "WEDDING";
  eventLoaded: boolean = false;
  typeLoaded: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.route = activatedRoute;
    this.route.firstChild.params.subscribe(params => {
      this.eventLoaded = true;
      this.typeEvent = this.getEventTypeString(params.id);
      this.set();
    });
    this.layoutService.onboardingEmitter.subscribe(type => {
      this.typeLoaded = true;
      this.type = type;
      this.set();
    });
  }

  ngOnInit() {
  }

  getEventTypeString(code) {
    switch (code) {
      case 'boda':
        return 'WEDDING'
        break;
      case 'babyshower':
        return 'BABY'
        break;
      case 'cumple':
        return 'BIRTHDAY'
        break;
    }
    return 'WEDDING';
  }

  getImages() {
    let url = '';
    switch (this.typeEvent) {
      case 'WEDDING':
        url += 'wedding/';
        break;
      case 'BABY':
        url += 'babyshower/';
        break;
    }

    switch (this.type) {
      case 'register':
        url += 'register_01.jpg';
        break;
      case '1':
        url += 'step-1.jpg';
        break;
      case '2':
        url += 'step-2.jpg';
        break;
      case '3':
        url += 'step-3.jpg';
        break;
      default:
        url += 'register_01.jpg';
    }
    return url;
  }

  getMessage() {
    switch (this.typeEvent) {
      case 'WEDDING':
        this.title = '¡Felicidades por su compromiso!';
        this.subtitle = 'Hagámoslo oficial y comiencen su sitio';
        break;
      case 'BABY':
        this.title = '¡Celebra este gran momento!';
        this.subtitle = 'Crea un sitio para tu baby shower, recibe regalos y comparte con todos las buenas noticias';
        break;
    }
  }

  set() {
    if (!this.typeLoaded || !this.eventLoaded) {
      console.log('nope');
      return;
    } else {
      console.log('yep');
    }
    switch (this.type) {
      case 'reset':
      case 'recovery':
      case 'login':
        this.title = '¡Felicidades por su compromiso!';
        this.subtitle = 'Qué gusto tenerlos de vuelta';
        this.image = 'wedding/register_01.jpg';
        break;
      case 'register':
        this.getMessage();
        this.image = this.getImages();
        break;
      case '1':
        this.getMessage();
        this.image = this.getImages();
        break;
      case '2':
        this.getMessage();
        this.image = this.getImages();
        break;
      case '3':
        this.getMessage();
        this.image = this.getImages();
        break;
    }
  }

}
