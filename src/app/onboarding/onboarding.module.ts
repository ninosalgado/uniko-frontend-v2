import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatMenuModule } from '@angular/material/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingComponent } from './onboarding.component';
import { RegisterComponent } from './register/register.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { LoginComponent } from './login/login.component';
import { RecoveryComponent } from './recovery/recovery.component';
import { Step1Component } from './step1/step1.component';
import { ResetComponent } from './reset/reset.component';
import { ActivateComponent } from './activate/activate.component';

import { urlOnlyDirective } from './../_helpers/directive.urlOnly';
import { Step0Component } from './step0/step0.component';

@NgModule({
  declarations: [
    urlOnlyDirective,
    OnboardingComponent, 
    RegisterComponent, 
    Step2Component, 
    Step3Component, 
    LoginComponent, 
    ResetComponent,
    RecoveryComponent, 
    Step1Component, 
    ActivateComponent, 
    Step0Component
  ],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatMenuModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatDialogModule,
    MatCheckboxModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
  ],
  providers: [  
    MatDatepickerModule,  
  ]
})
export class OnboardingModule { }
