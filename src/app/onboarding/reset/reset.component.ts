import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUser } from 'src/app/_models/data';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {
  recoveryForm: FormGroup;
  submitted = false;
  token: string;
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private router: Router,
    private route: ActivatedRoute
  ) { 
    localStorage.clear();
    this.layoutService.eventOnboarding('reset');

  }

  ngOnInit() {
    this.token = this.route.snapshot.params.token;
    this.recoveryForm = this.formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['', this.validatePass]
    });
  }

  validatePass(control: AbstractControl) {
    const pass = control.value;
    let error = null;
    if (!pass.length) {
      return {...error, email: 'La confirmación es requerida'};
    }
    return error;
  }

  async recovery(): Promise<void> {
    try {
      this.spinner.show();
      this.submitted = true;
      if (this.recoveryForm.invalid) {
        return;
      }
      const user: CurrentUser = {
        id: this.token,
        role: '',
        ttl: '',
        created: new Date()
      }
      this.layoutService._setCurrentUser(user);
      await this.userService.reset(this.recoveryForm.value.password);
      this.layoutService.clear();
      this.router.navigate(['/onboarding', 'login']);
    } catch (e) {
      this.layoutService.clear();
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
