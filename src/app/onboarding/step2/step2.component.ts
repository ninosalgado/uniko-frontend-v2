import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
  coupleaccount: ICoupleInformation;
  onboardingForm: FormGroup;
  submitted = false;
  myDate = new Date();
  suggestionsUrl = [];
  eventType: string;
  eventUrl: string = 'boda';
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.coupleaccount = this.layoutService.getCoupleInformation();
    this.suggestions();
    this.layoutService.eventOnboarding('2');
    this.eventUrl = activatedRoute.snapshot.params.id;
    this.eventType = this.getEventTypeString();
  }

  ngOnInit() {
    this.onboardingForm = this.formBuilder.group({
      url: ['', this.verifyUrl]
    })
  }
  get f() { return this.onboardingForm.controls; }

  getPlaceHolder() {
    switch (this.eventUrl) {
      case 'boda':
        return 'noviaynovio'
        break;
      case 'babyshower':
        return 'baby-bebe'
        break;
    }
    return 'noviaynovio';
  }

  getEventTypeString() {
    switch (this.eventUrl) {
      case 'boda':
        return 'WEDDING'
        break;
      case 'babyshower':
        return 'BABY'
        break;
      case 'cumple':
        return 'BIRTHDAY'
        break;
    }
    return 'WEDDING';
  }

  getEventString() {
    switch (this.eventType) {
      case 'WEDDING':
        return 'de la boda'
        break;
      case 'BABY':
        return 'del babyshower'
        break;
      case 'BIRTHDAY':
        return 'del cumpleaños'
        break;
    }
    return 'de su boda';
  }

  getMessage() {
    switch (this.eventType) {
      case 'WEDDING':
      default:
        return 'Personalicen su dirección web y compartan con sus invitados'
        break;
      case 'BABY':
      case 'BIRTHDAY':
        return 'Personaliza tu dirección web y comparte con tus invitados'
        break;
    }
  }

  verifyUrl(control: AbstractControl) {
    const data = control.value;
    const valid = /^([a-z0-9_\.-]+)$/;
    let error = null;
    if (!data.length) {
      return { ...error, url: 'La url es requerido' };
    }
    if (!valid.test(data)) {
      return { ...error, url: 'No debe contener mayusculas o caracteres especiales' };
    }
    return null;
  }

  async suggestions(): Promise<void> {
    try {
      this.spinner.show();
      this.suggestionsUrl = await this.coupleService.suggestionsUrl(this.coupleaccount.id);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  selectURL(event: Event, url: string) {
    event.stopPropagation();
    event.preventDefault();
    this.onboardingForm.patchValue({
      url: url
    });
  }

  async next(): Promise<void> {
    try {
      this.submitted = true;
      this.spinner.show();
      console.log(this.f);
      if (this.onboardingForm.invalid) {
        return;
      }
      this.coupleaccount.url = this.onboardingForm.value.url;
      this.coupleaccount.onboardingStep = 3;
      const couple = await this.coupleService.updateRegister(this.coupleaccount);
      this.layoutService.setCoupleInformation(couple);
      this.router.navigate(['/onboarding', this.eventUrl, '3']);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
