import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.scss']
})
export class RecoveryComponent implements OnInit {
  recoveryForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private router: Router
  ) { 
    localStorage.clear();
    this.layoutService.eventOnboarding('recovery');

  }

  ngOnInit() {
    this.recoveryForm = this.formBuilder.group({
      email: ['', Validators.required]
    });
  }

  async recovery(): Promise<void> {
    try {
      this.submitted = true;
      if (this.recoveryForm.invalid) {
        return;
      }
      this.spinner.show();
      await this.userService.recovery(this.recoveryForm.value.email);
      this.notification.success('En caso de que exista una cuenta asociada a este correo, recibirás un correo con las instrucciones para recuperar tu cuenta.');
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
