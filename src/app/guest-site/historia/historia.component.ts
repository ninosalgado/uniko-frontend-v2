import { Component, OnInit, Input } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoupleService } from 'src/app/services/couple.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-historia',
  templateUrl: './historia.component.html',
  styleUrls: ['./historia.component.scss']
})
export class HistoriaComponent implements OnInit {
  dataForm: FormGroup;
  @Input() couple: ICoupleInformation;
  isOpen: boolean = false;
  submitted: boolean = false;
  @Input() guest: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private notification: NotificationsService,
  ) {
  }

  ngOnInit() {
    this.onForm();
    this.defaultValues();
  }

  onForm() {
    this.dataForm = this.formBuilder.group({
      detail: [this.couple.weddingData.detailHistory, Validators.required],
      title: [this.couple.weddingData.titleHistory, Validators.required],
      messageHistory: [this.couple.weddingData.messageHistory, Validators.required]
    })
  }

  defaultValues() {
    this.couple.weddingData.detailHistory = this.couple.weddingData.detailHistory ?
    this.couple.weddingData.detailHistory : 'Nuestra historia';
    this.couple.weddingData.titleHistory = this.couple.weddingData.titleHistory ? 
    this.couple.weddingData.titleHistory : 'Título de su historia';
    this.couple.weddingData.messageHistory = this.couple.weddingData.messageHistory ?
    this.couple.weddingData.messageHistory : ' Sus invitados amarán saber todos los detalles acerca de su LOVE STORY y como llegaron hasta este día.';
  }

  edit() {
    this.isOpen = true;
  }

}
