import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import * as moment from 'moment';
import * as momentTimeZone from 'moment-timezone';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-datos-evento',
  templateUrl: './datos-evento.component.html',
  styleUrls: ['./datos-evento.component.scss']
})
export class DatosEventoComponent implements OnInit {
  @Output() edit = new EventEmitter();
  date: any;
  hour: any;
  @Output() isOpen = false;
  @Input() guest: boolean = false;
  @Input() couple: ICoupleInformation;
  diffDate: any = {
    days: '00',
    hours: '00',
    minutes: '00',
    seconds: '00'
  }
  time: any;
  constructor(
    private coupleService: CoupleService,
    private layoutService: LayoutService
  ) {     
  }

  ngOnInit() {
    moment.locale('es');    
    this.getDate();
    this.diffDateNow();
  }

  onEdit() {
    this.edit.emit('ok');
  }

  getDate() {
    if (this.couple.weddingData.date) {      
      let momentDate = momentTimeZone(this.couple.weddingData.date).tz('America/Mexico_City');      
      this.date = momentDate.format('dddd, DD MMMM YYYY');
      this.hour = momentDate.format('h:mm A');
    }
  }

  diffDateNow() {
    setInterval(() => {
      if (this.couple.weddingData.date) {
        let momentDate = moment(this.couple.weddingData.date).utc();
        const diffDays = momentDate.diff(moment().utc(), 'day');
        const diffHours = momentDate.diff(moment().add(diffDays, 'day').utc(), 'hours');
        const diffMinuts = momentDate.diff(moment().add(diffDays, 'day').add(diffHours, 'hours').utc(), 'minutes');
        const diffSeconds = momentDate.diff(moment().add(diffDays, 'day').add(diffHours, 'hours').add(diffMinuts, 'minutes').utc(), 'seconds');
        this.diffDate = {
          days: diffDays,
          hours: diffHours,
          minutes: diffMinuts,
          seconds: diffSeconds
        }
      }
    }, 1000)
  }
}
