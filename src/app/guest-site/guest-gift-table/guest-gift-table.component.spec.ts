import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestGiftTableComponent } from './guest-gift-table.component';

describe('GuestGiftTableComponent', () => {
  let component: GuestGiftTableComponent;
  let fixture: ComponentFixture<GuestGiftTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestGiftTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestGiftTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
