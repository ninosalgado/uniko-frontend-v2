import { Component, OnInit, Input } from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-guest-gift-table',
  templateUrl: './guest-gift-table.component.html',
  styleUrls: ['./guest-gift-table.component.scss']
})
export class GuestGiftTableComponent implements OnInit {
  @Input() couple: ICoupleInformation;
  gifts: any[] = [];
  giftReceived: number = 0;
  isOpen: boolean = false;
  dataForm: FormGroup;
  diffDate: any = {
    days: '00',
    hours: '00',
    minutes: '00',
    seconds: '00'
  }
  @Input() guest: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private orderService: OrderService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private formBuilder: FormBuilder) {

  }

  getEventCouple(event) {
    switch (event.type) {
      case 'WEDDING':
        return true;
        break;
      default:
        return false;
    }
  }

  diffDateNow() {
    setInterval(() => {
      if (this.couple.weddingData.date) {
        const date = moment(this.couple.weddingData.date).add(30, 'days');
        const diffDays = moment(date).diff(moment(), 'day');
        this.diffDate = {
          days: diffDays
        }
      }
    }, 1000)
  }

  ngOnInit() {
    this.dataForm = this.formBuilder.group({
      email: ['', Validators.required],
    })
    this.gifts = this.couple.productsRegistryList;
    this.getTotalRecived();
    this.diffDateNow();
  }

  openModal() {
    this.isOpen = true;
  }

  async saveEmail() {
    try {
      this.spinner.show();
      console.log("Delta");
      this.isOpen = false;
      await this.orderService.reminder(this.dataForm.value.email, this.couple.id);
      this.notification.success('Su correo ha sido guardado');
      this.dataForm.controls.email.reset();
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  getTotalRecived() {
    this.giftReceived = this.gifts.reduce((total, gift) => {
      return total + gift.received;
    }, 0);
  }
}
