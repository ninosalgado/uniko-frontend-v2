import { Component, OnInit, Input, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoupleService } from 'src/app/services/couple.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-hoteles',
  templateUrl: './hoteles.component.html',
  styleUrls: ['./hoteles.component.scss']
})
export class HotelesComponent implements OnInit {
  dataForm: FormGroup;
  mapAddress: any;
  hoteleGps: any [] = [];
  hotelsList: any[] = [];
  @Input() couple: ICoupleInformation;
  isOpen: boolean = false;
  submitted: boolean = false;
  showMap: boolean = false;
  latitude: any = 51.678418;
  longitude: any = 7.809007;
  zoom = 11;
  @Input() guest: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private notification: NotificationsService,
    private sanitizer: DomSanitizer,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { 
    
  }

  ngOnInit() {
    this.getHotls();
    this.loadMaps();
  }

  getIconByTheme(){
    const theme = this.couple.theme;
    switch(theme){
      case 'theme_gold':
        return './../../../assets/img/corazon_gold.svg'
        break;
      case 'theme_cake':
        return './../../../assets/img/corazon_cake.svg'
        break;
      default:
        return './../../../assets/img/corazon.svg'
    }
  }

  private async setCurrentLocation() {
    try {
      this.spinner.show();
      if (navigator.geolocation && !this.couple.weddingData.gpsaddress) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
          this.zoom = 13;
        });
      } else if (this.couple.weddingData.gpsaddress) {
        this.latitude = this.couple.weddingData.gpsaddress.geometry.location.lat;
        this.longitude = this.couple.weddingData.gpsaddress.geometry.location.lng;
        this.zoom = 13;
      }
    } catch (e) {

    } finally {
      this.spinner.hide();
    }
  }
  
  loadMaps() {
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
    });
  }

  async getHotls(): Promise<void> {
    try {
      this.spinner.show();
      this.hotelsList = await this.coupleService.getHotels(this.couple.id);
      this.hoteleGps = this.hotelsList.filter(data => data.gpsaddress);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  

}
