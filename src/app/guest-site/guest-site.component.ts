import { Component, OnInit } from '@angular/core';
import { Button, ImageSnippet, CurrentUser } from '../_models/data';
import { LayoutService } from '../services/layout.service';
import { MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoupleService } from '../services/couple.service';
import { NotificationsService } from '../services/notifications.service';
import { CatalogsService } from '../services/catalogs.service';
import * as moment from 'moment';
import * as momentTimeZone from 'moment-timezone';
import { MenuService } from '../services/menu.service';
import { promise } from 'protractor';
import { IFrequencyQuestions } from '../_interface/frequencyQuestion';
import { FrequentQuestionService } from '../services/frequent-question.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ICoupleInformation } from '../_interface/coupleInformation';

@Component({
  selector: 'app-guest-site',
  templateUrl: './guest-site.component.html',
  styleUrls: ['./guest-site.component.scss']
})
export class GuestSiteComponent implements OnInit {
  couple: ICoupleInformation;
  buttons: Array<Button> = [];
  isOpen: boolean = false;
  submit: boolean = false;
  dataForm: FormGroup;
  guest: boolean = false;
  states: any[] = [];
  cover: string;
  selectedFile: any;
  state: any;
  faqs: IFrequencyQuestions[] = [];
  url: string;
  zoneDate: string;
  constructor(
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private notification: NotificationsService,
    private catalogsService: CatalogsService,
    private frequentQuestionService: FrequentQuestionService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    moment.locale('es');
    localStorage.removeItem("currentUser");
    this.layoutService.setCurrentUser(new CurrentUser);
    this.url = this.route.snapshot.params.url;
    this.guest = this.layoutService.getCurrentUser().id ? false : true;
    this.getCouple();
  }

  ngOnInit() {

  }

  async get(): Promise<void> {
    try {
      this.spinner.show();
      const url = this.route.parent.snapshot.params.url;
      const couple: ICoupleInformation = await this.coupleService.findOneRestrict(url);
      this.layoutService.setCoupleInformation(couple); //NO!! NO!!!!!
      this.couple = couple;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  message(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }

  getCover() {
    this.cover = this.couple.weddingData.coverPhoto ? this.couple.weddingData.coverPhoto : null;
    if (this.cover && this.cover != '') {
      //this.cover = this.couple.weddingData.coverPhoto;
      return this.cover;
    } else {
      this.couple.theme = this.couple.theme ? this.couple.theme : '';
      const typeEvent = this.couple.weddingData.type ? this.couple.weddingData.type : 'WEDDING';
      let baseImg = './../../assets/img/site/site_default';
      baseImg += (this.couple.theme && this.couple.theme != '') ? '_' + this.couple.theme : this.couple.theme;
      switch (typeEvent) {
        case 'WEDDING':
          baseImg += '_wedding.jpg';
          break;
        case 'BABY':
          baseImg += '_baby.jpg';
          break;
        default:
          baseImg += '_wedding.jpg';
      }

      //this.cover = baseImg
      return baseImg;
    }
  }

  setState() {
    const state = this.states.find(_state => _state.id === this.couple.stateId);
    this.state = state ? state.name : '';
  }

  getEventString(event) {
    let type;
    switch (event.type) {
      case 'WEDDING':
        type = 'Nuestra Boda'
        break;
      case 'BABY':
        type = 'Mi Baby Shower'
        break;
      case 'BIRTHDAY':
        type = 'Mi Cumpleaños'
        break;
    }
    return type;
  }

  isCouple(event) {
    switch (event.type) {
      case 'WEDDING':
        return true;
      case 'BABY':
        return false;
      default:
        return true;
    }
  }

  async getStates(): Promise<void> {
    try {
      this.states = await this.catalogsService.states();
      this.setState();
    } catch (e) {
      this.notification.error(e);
    }
  }

  async getCouple(): Promise<void> {
    try {
      this.spinner.show();
      const couple: ICoupleInformation = await this.coupleService.findOneRestrict(this.url);
      this.layoutService.setCoupleInformation(couple);
      this.couple = couple;
      if (couple && couple !== null) {
        await this.getStates();
        await this.getFaqs();
        if (this.couple.weddingData.date) {
          let momentDate = momentTimeZone(this.couple.weddingData.date).tz('America/Mexico_City');
          this.zoneDate = momentDate.format('DD.MM.YYYY');
          this.couple.weddingData.date = new Date(momentDate.format());
        }
      } else {
        this.notification.warning("No se encuentra un evento con la liga proporcionada.");
        this.router.navigate(['/search']);
      }

    } catch (e) {
      this.notification.error(e)
    } finally {
      this.spinner.hide();
    }
  }

  async getFaqs(): Promise<void> {
    try {
      this.faqs = await this.frequentQuestionService.getBycouple(
        this.couple.id
      );
    } catch (e) {
      this.notification.error(e);
    }
  }

}
