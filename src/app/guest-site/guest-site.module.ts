import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuestSiteComponent } from './guest-site.component';
import { GuestSiteRoutingModule } from './guest-site-routing.module';
import { HeaderComponent } from '../components/header/header.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { DatosEventoComponent } from './datos-evento/datos-evento.component';
import { HistoriaComponent } from './historia/historia.component';
import { HotelesComponent } from './hoteles/hoteles.component';
import { GuestGiftTableComponent } from './guest-gift-table/guest-gift-table.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { GuestFaqsComponent } from './guest-faqs/guest-faqs.component';
import { OwlNativeDateTimeModule, OwlDateTimeModule } from 'ng-pick-datetime';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    GuestSiteComponent,
    DatosEventoComponent,
    HistoriaComponent,
    HotelesComponent,
    GuestGiftTableComponent,
    GaleriaComponent,
    GuestFaqsComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    GuestSiteRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    AgmCoreModule
  ]
})
export class GuestSiteModule { }
