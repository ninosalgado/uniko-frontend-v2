import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestSiteComponent } from './guest-site.component';


const routes: Routes = [{
  path: '',
  component: GuestSiteComponent
}];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestSiteRoutingModule { }
