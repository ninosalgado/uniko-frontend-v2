import { Component, OnInit, Input } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { galleryObject } from 'src/app/site/galeria/galeria.component';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.scss']
})
export class GaleriaComponent implements OnInit {
  @Input() couple: ICoupleInformation;
  @Input() guest: boolean = false;
  coupleGallery: any[];
  gallery: galleryObject[] = [];
  targetIndex:number;
  loaded: boolean = false;
  haveGallery: boolean = false;
  haveHashtag: boolean = false;
  instagramInfo:any;
  quickLock: boolean = false;
  constructor(
    private coupleService: CoupleService,
    private layoutService: LayoutService
  ) {
  }

  ngOnInit() {
    this.getInstagram();
  }


  setGallery(){
    if(this.instagramInfo.hashtag){      
      this.haveHashtag = true;
    }else{
      this.haveHashtag = false;
    }

    if(this.instagramInfo.images){
      let images = this.instagramInfo.images;
      this.gallery = [];
      this.coupleGallery = this.instagramInfo.images;      
      this.haveGallery = true;      
      for (let i = 0; i < images.length; i++){
        this.gallery.push({
          imageUrl:images[i],
          saved: true,
        });
      }
    }else{
      this.haveGallery = false;
    }
  }

  async getInstagram(){
    this.instagramInfo = await this.coupleService.getCouplesInstagram(this.couple.id);
    if(this.instagramInfo){
      console.log(this.instagramInfo);
      this.loaded = true;
      if(this.instagramInfo){
        this.setGallery();
      }
    }
  }
}
