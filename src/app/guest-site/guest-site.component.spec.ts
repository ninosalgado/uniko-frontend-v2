import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestSiteComponent } from './guest-site.component';

describe('GuestSiteComponent', () => {
  let component: GuestSiteComponent;
  let fixture: ComponentFixture<GuestSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
