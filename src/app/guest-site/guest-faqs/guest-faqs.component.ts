import { Component, OnInit, Input } from '@angular/core';
import { IFrequencyQuestions } from 'src/app/_interface/frequencyQuestion';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-guest-faqs',
  templateUrl: './guest-faqs.component.html',
  styleUrls: ['./guest-faqs.component.scss']
})
export class GuestFaqsComponent implements OnInit {
  @Input() couple: ICoupleInformation;
  @Input() faqs: IFrequencyQuestions[] = [];
  @Input() guest: boolean = false;
  constructor(
    private coupleService: CoupleService,
    private layoutService: LayoutService
  ) {
  }

  ngOnInit() {
  }

  existImg(preg) {
    return preg && preg.image ? true : false;
  }

  getImageToVideo(preg) {
    const isExist = preg.image && preg.image.length ? true : false;
    const link = preg.image ? preg.image : '';
    const existType = link.search("type");
    if (existType > 10) {
      try {
        const type = link.slice(existType);
        const typeArray = type.split('=');
        const _type = atob(typeArray[1]);
        if (_type.search('video') >= 0 || (typeArray[1].search('video') >= 0)) {
          return `<video src="${link}" controls=""></video>`;
        } else {
          return `<img src="${link}">`;  
        }
      } catch (e) {
        return isExist ? `<img src="${link}">` : '';  
      }
    } else {
      return isExist ? `<img src="${link}">` : '';
    }
  }
}
