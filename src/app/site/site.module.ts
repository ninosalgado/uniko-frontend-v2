import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteComponent } from './site.component';
import { SiteRoutingModule } from './site-routing.module';
import { HeaderComponent } from '../components/header/header.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { DatosEventoComponent } from './datos-evento/datos-evento.component';
import { HistoriaComponent } from './historia/historia.component';
import { HotelesComponent } from './hoteles/hoteles.component';
import { GuestGiftTableComponent } from './guest-gift-table/guest-gift-table.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { GuestFaqsComponent } from './guest-faqs/guest-faqs.component';
import { OwlNativeDateTimeModule, OwlDateTimeModule } from 'ng-pick-datetime';
import { AgmCoreModule } from '@agm/core';
import { ThemeManagerComponent } from './theme-manager/theme-manager.component';

@NgModule({
  declarations: [
    SiteComponent,
    DatosEventoComponent,
    HistoriaComponent,
    HotelesComponent,
    GuestGiftTableComponent,
    GaleriaComponent,
    GuestFaqsComponent,
    ThemeManagerComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    SiteRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    AgmCoreModule
  ]
})
export class SiteModule { }
