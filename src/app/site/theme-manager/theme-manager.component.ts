import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from 'src/app/services/notifications.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-theme-manager',
  templateUrl: './theme-manager.component.html',
  styleUrls: ['./theme-manager.component.scss']
})
export class ThemeManagerComponent implements OnInit {

  themes: any[];
  couple: ICoupleInformation;
  constructor(
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.couple = this.layoutService.getCoupleInformation();
  }

  ngOnInit() {
    const planUniko = {
      image: "/img/site-templates/template_00",
      code: "uniko",
      title: "UNIKO",
      active: (!this.couple.theme || this.couple.theme == "") ? true : false
    }

    const planGold = {
      image: "/img/site-templates/template_01",
      code: "theme_gold",
      title: "GOLD",
      active: this.couple.theme == "theme_gold" ? true : false
    }

    const planCake = {
      image: "/img/site-templates/template_02",
      code: "theme_cake",
      title: "PASTEL",
      active: this.couple.theme == "theme_cake" ? true : false
    }

    this.themes = [planUniko, planGold, planCake];

    this.couple.weddingData.type = this.couple.weddingData.type ? this.couple.weddingData.type : 'WEDDING';
  }

  getSubfix() {
    switch (this.couple.weddingData.type.toLowerCase()) {
      case 'wedding':
        return ''
      case 'baby':
        return '_baby'
    }
    return '';
  }

  goTheme(code) {
    this.router.navigate(["../preview", code], { relativeTo: this.route });
  }
}
