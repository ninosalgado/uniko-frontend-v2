import { Component, OnInit, Input } from '@angular/core';
import { LayoutService } from './../../services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import * as moment from 'moment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-guest-gift-table',
  templateUrl: './guest-gift-table.component.html',
  styleUrls: ['./guest-gift-table.component.scss']
})
export class GuestGiftTableComponent implements OnInit {
  @Input() couple: ICoupleInformation;
  stores: any[] = [];
  gifts: any[] = [];
  giftReceived: number = 0;
  date: any;
  hour: any;
  dataForm: FormGroup;
  isOpen: boolean = false;
  diffDate: any = {
    days: '00',
    hours: '00',
    minutes: '00',
    seconds: '00'
  }
  time: any;
  quickLock: boolean = false;
  @Input() guest: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private formBilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService
  ) {
    this.couple = this.layoutService.getCoupleInformation();
    this.gifts = this.couple.productsRegistryList;
    this.getTotalRecived();
    this.catalogs();
    this.diffDateNow();
  }

  ngOnInit() {
    this.dataForm = this.formBilder.group({
      email: ['', Validators.required]
    });
  }

  onClose() {
    this.isOpen = false;
  }

  getEventCouple(event) {
    switch (event.type) {
      case 'WEDDING':
        return true;
        break;
      default:
        return true;
    }
  }

  async saveEmail() {
    try {
      this.spinner.show();
      this.notification.success('Se le notificara por email cuando este cerca los 10 días');
      this.isOpen = false;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  openModal() {
    this.isOpen = true;
  }

  async catalogs(): Promise<void> {
    try {
      this.stores = await this.coupleService.getStores();
    } catch (e) {
      console.log(e);
    } finally {

    }
  }

  diffDateNow() {
    setInterval(() => {
      if (this.couple.weddingData.date) {
        const date = moment(this.couple.weddingData.date).add(30, 'days');
        const diffDays = moment(date).diff(moment(), 'day');
        this.diffDate = {
          days: diffDays
        }
      }
    }, 1000)
  }

  async updateView() {
    try {
      this.spinner.show();
      this.quickLock = true;
      let consult = await this.coupleService.updateSiteView(this.couple.id, { viewGifts: 1 });
      if (consult == "ok") {
        this.couple.viewGifts = !this.couple.viewGifts;
        this.layoutService.coupleInformation.info = this.couple;
      }
      this.quickLock = false;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  getNameStore(id: string): string {
    const store = this.stores.find(_store => _store.id == id);
    let name = store ? store.name : '';
    return name;
  }

  getTotalRecived() {
    this.giftReceived = this.gifts.reduce((total, gift) => {
      return total + gift.received;
    }, 0);
  }

  daysClose() {

  }

  async updateLite(): Promise<void> {
    try {
      this.couple = await this.coupleService.updateRegister(this.couple);
      this.layoutService.setCoupleInformation(this.couple);
    } catch (e) {
      console.log(e);
    } finally {
    }
  }
}
