import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SiteComponent } from './site.component';
import { ThemeManagerComponent } from './theme-manager/theme-manager.component';


const routes: Routes = [
  {
    path: '',
    component: SiteComponent
  },
  {
    path: 'preview/:theme',
    component: SiteComponent
  },
  {
    path: 'themes',
    component: ThemeManagerComponent
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }
