import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { Button, ImageSnippet } from '../_models/data';
import { LayoutService } from '../services/layout.service';
import { MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoupleService } from '../services/couple.service';
import { NotificationsService } from '../services/notifications.service';
import { CatalogsService } from '../services/catalogs.service';
import * as moment from 'moment';
import * as momentTimeZone from 'moment-timezone';
import { MenuService } from '../services/menu.service';
import { promise } from 'protractor';
import { IFrequencyQuestions } from '../_interface/frequencyQuestion';
import { FrequentQuestionService } from '../services/frequent-question.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ICoupleInformation } from '../_interface/coupleInformation';
import { MapsAPILoader } from '@agm/core';
@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.scss']
})
export class SiteComponent implements OnInit {
  eventPlace: string;
  @ViewChild('search') public searchElementRef: ElementRef;
  @ViewChild('coverInput') inputImageField: ElementRef;
  gpsaddress: any = null;
  couple: ICoupleInformation;
  buttons: Array<Button> = [];
  isOpen: boolean = false;
  isOpenCropper: boolean = false;
  isOpenShare: boolean = false;
  submit: boolean = false;
  dataForm: FormGroup;
  guest: boolean = false;
  states: any[] = [];
  cover: string;
  selectedFile: any;
  state: any;
  faqs: IFrequencyQuestions[] = [];
  imageChangedEvent: any = '';
  croppedImage: any = '';
  showCroppedImage: boolean = false;
  file: any;
  preview: boolean = false;
  theme: string = '';
  zoneDate: string;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private notification: NotificationsService,
    private catalogsService: CatalogsService,
    private menuService: MenuService,
    private frequentQuestionService: FrequentQuestionService,
    private router: Router,
    private route: ActivatedRoute,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
  ) {

    moment.locale('es');
    this.couple = this.layoutService.getCoupleInformation();
    this.layoutService.coupleEmitter.subscribe(isUpdate => {
      this.couple = this.layoutService.getCoupleInformation();
    });
    this.guest = this.layoutService.getCurrentUser().id ? false : true;
    this.getInfo();
  }

  ngOnInit() {
    const theme = this.route.snapshot.params.theme;
    if (theme) {
      this.preview = true;
      this.theme = theme == "uniko" ? "" : theme;
    } else {
      this.theme = this.couple.theme;
    }
    this.cover = this.couple.weddingData.coverPhoto ? this.couple.weddingData.coverPhoto : null;
    this.setButtons();
    this.onForm();
  }

  getEventString(event) {
    let type;
    switch (event.type) {
      case 'WEDDING':
        type = 'Nuestra Boda'
        break;
      case 'BABY':
        type = 'Mi Baby Shower'
        break;
      case 'BIRTHDAY':
        type = 'Mi Cumpleaños'
        break;
    }
    return type;
  }

  isCouple(event) {
    switch (event.type) {
      case 'WEDDING':
        return true;
      case 'BABY':
        return false;
      default:
        return true;
    }
  }

  setButtons() {
    const share = new Button(
      'Compartir',
      this.screenShared,
      'fa-share-alt',
      null,
      this
    );
    if (this.preview) {
      const themes = new Button(
        'Guardar tema',
        this.saveTheme,
        'fa-save',
        null,
        this
      );
      const back = new Button(
        'ver mas temas',
        this.goToThemes,
        'fa-palette',
        null,
        this
      );
      this.buttons = [share, themes, back];
    } else {
      const themes = new Button(
        'Temas',
        this.goToThemes,
        'fa-palette',
        null,
        this
      );
      this.buttons = [share, themes];
    }
  }

  async getInfo(): Promise<void> {
    this.getFaqs();
    this.getStates();

  }

  goToThemes(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    this['parent'].router.navigate(['/', 'site', 'themes']);
  }

  saveTheme(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    this['parent'].setTheme();
  }

  screenShared(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    this['parent'].isOpenShare = true;
  }

  onForm() {
    this.dataForm = this.formBuilder.group({
      name: [this.couple.weddingData.nameP1, Validators.required],
      name2: [this.couple.weddingData.nameP2],
      date: [this.couple.weddingData.date],
      title: [this.couple.weddingData.titleMessage],
      description: [this.couple.weddingData.welcomeMessage],
      placeName: [this.couple.weddingData.placeName]
    });
    this.onAddress();
  }

  onAddress() {
    this.searchElementRef.nativeElement.value = this.couple.weddingData.address
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation()
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.gpsaddress = place;
          this.eventPlace = place.name;
        });
      });
    });
  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log(position);
      });
    }
  }

  async getCouple(): Promise<void> {
    try {
      this.spinner.show();
      const couple = await this.coupleService.get(this.couple.id);
      this.layoutService.getCoupleInformation()
      this.couple = couple;
      this.layoutService.setCoupleInformation(couple);
      this.couple = couple;
      if (this.couple.weddingData.date) {
        let momentDate = momentTimeZone(this.couple.weddingData.date).tz('America/Mexico_City');
        this.zoneDate = momentDate.format('DD.MM.YYYY');
        this.couple.weddingData.date = new Date(momentDate.format());
      }
      this.onForm();
    } catch (e) {
      this.notification.error(e)
    } finally {
      this.spinner.hide();
    }
  }

  message(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }

  async setTheme() {
    try {
      this.spinner.show();
      let couple = this.couple;
      couple.theme = this.theme;
      const _couple = await this.coupleService.updateRegister(couple);
      this.layoutService.setCoupleInformation(this.couple);
      this.notification.success('Tema guardado');
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
      this.router.navigate(['/', 'site']);
    }
  }

  getCover() {
    if (this.cover && this.cover != '') {
      //this.cover = this.couple.weddingData.coverPhoto;
      return this.cover;
    } else {
      this.theme = this.theme ? this.theme : '';
      const typeEvent = this.couple.weddingData.type ? this.couple.weddingData.type : 'WEDDING';
      let baseImg = './../../assets/img/site/site_default';
      baseImg += (this.theme && this.theme != '') ? '_' + this.theme : this.theme;
      switch (typeEvent) {
        case 'WEDDING':
          baseImg += '_wedding.jpg';
          break;
        case 'BABY':
          baseImg += '_baby.jpg';
          break;
        default:
          baseImg += '_wedding.jpg';
      }

      //this.cover = baseImg
      return baseImg;
    }
  }


  onUpload(event: Event, element: string) {
    event.stopPropagation();
    event.preventDefault();
    document.getElementById(element).click();
  }

  setState() {
    const state = this.states.find(_state => _state.id === this.couple.stateId);
    this.state = state ? state.name : '';
    console.log(state);
  }

  openCropper(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async () => {
        //  await this.uploadCoverPhoto(url, file);
        // this.selectedFile = new ImageSnippet(url, file);
        this.isOpenCropper = true;
        this.imageChangedEvent = event;
      };
    }
  }

  async uploadCropper(file) {
    try {
      this.selectedFile = new ImageSnippet('', file);
      const coverPhoto = await this.coupleService.uploadCover(
        this.couple.id,
        this.selectedFile.file
      );
      this.cover = coverPhoto.url;
    } catch (e) {
      this.notification.error(e);
      console.log(e);
    } finally {
    }
  }

  async upload(event, type: string) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      const file = event.target.files[0];
      const url = event.target['result'];
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = async () => { // called once readAsDataURL is completed
        await this.uploadCoverPhoto(url, file);
      };
    }
  }

  saveCropp() {
    this.showCroppedImage = true;
    this.isOpenCropper = false;
  }

  async uploadCoverPhoto(url, file): Promise<void> {
    try {
      this.selectedFile = new ImageSnippet(url, file);
      const coverPhoto = await this.coupleService.uploadCover(
        this.couple.id,
        this.selectedFile.file
      );
      this.cover = coverPhoto.url;
    } catch (e) {
      this.notification.error(e);
    } finally {
    }
  }

  onEdit() {
    this.isOpen = true;
    this.getCouple();
  }

  async getStates(): Promise<void> {
    try {
      this.states = await this.catalogsService.states();
      this.setState();
    } catch (e) {
      this.notification.error(e);
    }
  }

  onClose() {
    this.isOpen = false;
    this.isOpenCropper = false;
    this.showCroppedImage = false;
  }
  onCloseCropper() {
    this.isOpenCropper = false;
  }

  async onActive(active?: any): Promise<void> {

  }

  async getFaqs(): Promise<void> {
    try {
      this.faqs = await this.frequentQuestionService.getBycouple(
        this.couple.id
      );
    } catch (e) {
      this.notification.error(e);
    }
  }

  getCity() {
    let city = this.gpsaddress.address_components.filter(items => items.types.includes('locality') || items.types.includes('sublocality')).map(function (obj) {
      return obj.long_name;
    });
    return city[0];
  }

  formatData() {
    if (this.gpsaddress && this.gpsaddress.formatted_address) {
      this.couple.weddingData.address = this.gpsaddress.formatted_address;
      this.couple.weddingData.city = this.getCity();
      this.couple.weddingData.gpsaddress = this.gpsaddress;
    }
  }

  async update(type?: string): Promise<void> {
    try {
      this.spinner.show();
      this.submit = true;
      if (this.dataForm.invalid) {
        return;
      }

      const couple = this.couple;
      this.formatData();
      if (this.showCroppedImage) {
        await this.uploadCropper(this.file);
        this.couple.weddingData.coverPhoto = this.cover;
      }
      if (this.dataForm.value.date) {
        couple.weddingData.date = this.dataForm.value.date;
        couple.weddingData.dateText = moment(this.dataForm.value.date).format('LLLL');
      } else {
        couple.weddingData.date = null;
        couple.weddingData.dateText = "Sin Fecha";
      }
      couple.weddingData.nameP1 = this.dataForm.value.name;
      couple.weddingData.nameP2 = this.dataForm.value.name2;
      couple.weddingData.titleMessage = this.dataForm.value.title;
      couple.weddingData.welcomeMessage = this.dataForm.value.description;
      couple.weddingData.placeName = this.gpsaddress ? this.gpsaddress.name : this.dataForm.value.placeName;
      couple.stateId = this.dataForm.value.state;
      this.couple = await this.coupleService.updateRegister(couple);
      this.isOpen = false;
      this.layoutService.setCoupleInformation(this.couple);
      this.cover = this.couple.weddingData.coverPhoto;
      this.notification.success('Datos actualizados');
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.file = event.file;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    this.inputImageField.nativeElement.value = null;
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
}
