import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import * as moment from 'moment';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-datos-evento',
  templateUrl: './datos-evento.component.html',
  styleUrls: ['./datos-evento.component.scss']
})
export class DatosEventoComponent implements OnInit {
  @Output() edit = new EventEmitter();
  @Input() couple: ICoupleInformation;
  date: any;
  hour: any;
  @Output() isOpen = false;
  @Input() guest: boolean = false;
  diffDate: any = {
    days: '00',
    hours: '00',
    minutes: '00',
    seconds: '00'
  }
  time: any;
  quickLock: boolean = false;
  constructor(
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private notification: ToastrService,
    private spinner: NgxSpinnerService
  ) { 
    moment.locale('es');
    this.couple = this.layoutService.getCoupleInformation();
    this.layoutService.coupleEmitter.subscribe(data => {
      this.diffDate = {
        days: '00',
        hours: '00',
        minutes: '00',
        seconds: '00'
      }
      this.couple = this.layoutService.getCoupleInformation();
      this.getDate();
    });
    this.getDate();
    this.diffDateNow();
  }

  ngOnInit() {

  }

  async updateView(){
    try {
      this.spinner.show();
      this.quickLock = true;
      let consult = await this.coupleService.updateSiteView(this.couple.id,{viewTime:1});    
      if(consult=="ok"){
        this.couple.viewTime = !this.couple.viewTime;
        this.layoutService.coupleInformation.info = this.couple;
      }
      this.quickLock = false;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  onEdit() {
    this.edit.emit('ok');
  }

  getDate() {
    if (this.couple.weddingData.date) {
      this.date = moment(this.couple.weddingData.date).format('dddd, DD MMMM YYYY');
      this.hour = moment(this.couple.weddingData.date).format('h:mm A');
    }
  }

  diffDateNow() {
    setInterval(() => {
      if (this.couple.weddingData.date) {
        const diffDays = moment(this.couple.weddingData.date).diff(moment(), 'day');
        const diffHours = moment(this.couple.weddingData.date).diff(moment().add(diffDays, 'day'), 'hours');
        const diffMinuts = moment(this.couple.weddingData.date).diff(moment().add(diffDays, 'day').add(diffHours, 'hours'), 'minutes');
        const diffSeconds = moment(this.couple.weddingData.date).diff(moment().add(diffDays, 'day').add(diffHours, 'hours').add(diffMinuts, 'minutes'), 'seconds');
        this.diffDate = {
          days: diffDays,
          hours: diffHours,
          minutes: diffMinuts,
          seconds: diffSeconds
        }
      }
    }, 1000)
  }

  async updateLite(): Promise<void> {
    try {
      this.couple = await this.coupleService.updateRegister(this.couple);
      this.layoutService.setCoupleInformation(this.couple);
    } catch (e) {
      console.log(e);
    } finally {    
    }
  }
}
