import { Component, OnInit, Input, ElementRef, ViewChild, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoupleService } from 'src/app/services/couple.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { DomSanitizer } from '@angular/platform-browser';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-hoteles',
  templateUrl: './hoteles.component.html',
  styleUrls: ['./hoteles.component.scss']
})
export class HotelesComponent implements OnInit {
  dataForm: FormGroup;
  plans: FormArray;
  isOpen: boolean = false;
  isOpenInfo: boolean = false;
  mapAddress: any;
  hoteleGps: any [] = []
  @ViewChild('search') public searchElementRef: ElementRef;
  private geoCoder;
  submitted: boolean = false;
  showMap: boolean = false;
  hotelsList: any[] = [];
  @Input() guest: boolean = false;
  @Input() couple: ICoupleInformation;
  quickLock: boolean = false;
  latitude: any = 51.678418;
  longitude: any = 7.809007;
  zoom = 11;
  gpsaddress: any;
  hotel: any;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private notification: NotificationsService,
    private sanitizer: DomSanitizer,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { 
    this.couple = this.layoutService.getCoupleInformation();
    this.layoutService.coupleEmitter.subscribe(data => {
      this.couple = this.layoutService.getCoupleInformation();
      this.loadMaps();
    });
    this.getHotls();
    
  }

  ngOnInit() {
    this.onForm();
    this.loadMaps();
  }

    get plan() {
      return this.dataForm.get('plan') as FormArray;
    }

  onClosedInfo() {
    this.isOpenInfo = false;
  }

  getIconByTheme(){
    const theme = this.couple.theme;
    switch(theme){
      case 'theme_gold':
        return './../../../assets/img/corazon_gold.svg'
        break;
      case 'theme_cake':
        return './../../../assets/img/corazon_cake.svg'
        break;
      default:
        return './../../../assets/img/corazon.svg'
    }
  }

  private async setCurrentLocation() {
    try {
      this.spinner.show();
      if (navigator.geolocation && !this.couple.weddingData.gpsaddress) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
          this.zoom = 13;
        });
      } else if (this.couple.weddingData.gpsaddress) {
        this.latitude = this.couple.weddingData.gpsaddress.geometry.location.lat;
        this.longitude = this.couple.weddingData.gpsaddress.geometry.location.lng;
        this.zoom = 13;
      }
    } catch (e) {

    } finally {
      this.spinner.hide();
    }
  }

  loadMaps() {
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
    });
  }

  async removeHotel() {
    try {
      this.spinner.show();
      await this.coupleService.delHotel(this.couple.id , this.hotel.id);
      this.isOpenInfo = false;
      this.getHotls();
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  confirmDelHotel(hotel: any, planId?: string) {
    this.isOpenInfo = true;
    this.hotel = hotel;
    if (planId) {
      this.hotel.removePlanId = planId;
    }
  }

  async removePlan(id: string) {
    try {
      this.isOpenInfo = true;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  add() {
    this.isOpen = true;
    this.submitted = false;
    this.dataForm.reset();
    this.onForm();
    this.onAddress();
  }

  onClosed() {
    this.isOpen = false;
  }

  onForm() {
    this.dataForm = this.formBuilder.group({
          name: ['', Validators.required],
          phone: ['', Validators.required],
          address: ['', Validators.required],
          plan: new FormArray([])
    });
  }

  onAddress() {
    this.searchElementRef.nativeElement.value = '';
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace(); 
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          console.log(place);
          this.gpsaddress = place;  
          if (place.name) {
            this.dataForm.patchValue({
              name: place.name,
              phone: place.international_phone_number
            });
          }        
          // this.eventPlace = place.name;
        });
      });
    });
  }


  async getHotls(): Promise<void> {
    try {
      this.spinner.show();
      this.hotelsList = await this.coupleService.getHotels(this.layoutService.getCoupleInformation().id);
      this.hoteleGps = this.hotelsList.filter(data => data.gpsaddress);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  delPlan(event: Event, plan) {
    event.stopPropagation();
    event.preventDefault();
    this.plans.controls.splice(plan, 1);
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  async updateView(view){
    try {
      this.spinner.show();
      this.quickLock = true;
      let consult = await this.coupleService.updateSiteView(this.couple.id,view);
      if(consult=="ok"){
        this.couple.viewHotel = !this.couple.viewHotel;
        this.layoutService.coupleInformation.info = this.couple;
      }
      this.quickLock = false;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }
  

  

  addPlan() {
    this.plans = this.dataForm.get('plan') as FormArray;;
    this.plans.push(this.createPlan());
    console.log(this.plans);
  }

  createPlan(): FormGroup {
    return this.formBuilder.group({
      bed: ['1', Validators.required],
      price: [0, Validators.required],
      cupon: [''],
      name: ['', Validators.required]
    })
  }

  async update(): Promise<void> {
    try {
      this.spinner.show();
      this.submitted = true;
      if (this.dataForm.invalid) {
        return;
      }
      const hotel = {
        name: this.dataForm.value.name,
        phone: this.dataForm.value.phone,
        address: this.dataForm.value.address,
        plan: this.dataForm.value.plan,
        gpsaddress: this.gpsaddress
      }
      console.log(this.dataForm.value, this.gpsaddress);
      const data = await this.coupleService.setHotels(this.couple.id, hotel);
      this.isOpen = false;
      this.getHotls();
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async updateLite(): Promise<void> {
    try {
      this.couple = await this.coupleService.updateRegister(this.couple);
      this.layoutService.setCoupleInformation(this.couple);
    } catch (e) {
      console.log(e);
    } finally {    
    }
  }

}
