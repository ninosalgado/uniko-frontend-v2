import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestFaqsComponent } from './guest-faqs.component';

describe('GuestFaqsComponent', () => {
  let component: GuestFaqsComponent;
  let fixture: ComponentFixture<GuestFaqsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestFaqsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestFaqsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
