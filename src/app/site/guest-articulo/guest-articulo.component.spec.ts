import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestArticuloComponent } from './guest-articulo.component';

describe('GuestArticuloComponent', () => {
  let component: GuestArticuloComponent;
  let fixture: ComponentFixture<GuestArticuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestArticuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestArticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
