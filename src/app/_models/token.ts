export class Token {
    id: string;
    livemode: boolean;
    object: string;
    used: boolean;
    isLoad: boolean;
    constructor() {
        this.isLoad = false;
    }
    getIsLoad() {
        return this.isLoad && !this.used;
    }
}