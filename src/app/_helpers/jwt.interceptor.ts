
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LayoutService } from './../services/layout.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private layoutService: LayoutService) {

    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentUser = this.layoutService.getCurrentUser();
        if (currentUser && currentUser.id) {
            request = request.clone({
                setHeaders: { 
                    authorization: `${currentUser.id}`
                }
            });
        }

        return next.handle(request);
    }
}