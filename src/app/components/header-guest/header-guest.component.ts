import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { Cart } from 'src/app/_models/cart';
import { ICart } from 'src/app/_interface/cart';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header-guest',
  templateUrl: './header-guest.component.html',
  styleUrls: ['./header-guest.component.scss']
})
export class HeaderGuestComponent implements OnInit {
bolsaCompra:boolean = true;
isOpen = false;
cart: ICart;
menus: Array<any> = [];
title: string;
open = false;
guest: boolean = false;
url: string;
totalItems;
  constructor(
    private layoutService: LayoutService,
    private userService: UserService
  ) { 
    const couple = this.layoutService.getCoupleInformation();
    this.guest = this.layoutService.getCurrentUser().id ? false : true;
    this.title = `${couple.weddingData.nameP1.charAt(0).toUpperCase()} & ${couple.weddingData.nameP2.charAt(0).toUpperCase()}`;
    this.url = couple.url;
    this.layoutService.cart.cartEmitter.subscribe(data => {
      this.isOpen = true;
      this.cart = this.layoutService.cart.cart;
      this.totalItems = this.layoutService.cart.totalItems;
    });
    this.cart = this.layoutService.cart.cart;
    this.totalItems = this.layoutService.cart.totalItems;
  }

  ngOnInit() {
    this.getMenus();

  }

  onClose() {
    this.isOpen = false;
  }


  isActive(menu: string) {
    if (this.guest) {
      const _menu = this.menus.find(data => data._key === menu);
      if (_menu && _menu.active) {
        return true
      }
      return false;
    } else {
      return true;
    }
  }

  get currency() {
    return this.layoutService.settings.info.currency;
  }

  async getMenus(): Promise<void> {
    this.menus = await this.userService.getMenu(this.layoutService.getCoupleInformation().id);
    console.log(this.menus);
  }

}
