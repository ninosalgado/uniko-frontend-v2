import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { URL_RESOURCES } from 'src/environments/environment';
import { LayoutService } from 'src/app/services/layout.service';
import Clipboard from './../../../assets/clipboard.js-2.0.4';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareComponent implements OnInit {
  couple: any;
  @Input() isOpen: boolean = false;
  @Input() customUrl: string = "";
  textlink: string = '';
  isCouple: boolean = true;
  constructor(
    private notification: ToastrService,
    private layoutService: LayoutService
  ) {
    this.isCouple = this.getCouple(this.layoutService.coupleInformation.info.weddingData.type);
    this.couple = this.layoutService.getCoupleInformation();
    this.layoutService.coupleEmitter.subscribe(data => {
      this.couple = this.layoutService.getCoupleInformation();
      this.textlink = `${URL_RESOURCES.UNIKO}/${this.couple.url}/`;
    });
    this.textlink = `${URL_RESOURCES.UNIKO}/${this.couple.url}/`;
  }

  ngOnInit() {
    try {
      this.textlink = this.textlink + this.customUrl;
      console.log(this.textlink);
      new Clipboard(".clipboard");
    } catch (e) {

    }
  }

  getCouple = (type) => {
    switch (type) {
      case 'BABY':
        return false;
      case 'WEDDING':
      default:
        return true;
    }
  }

  what() {
    window.open(
      `https://wa.me/?text=${encodeURI(this.textlink)}`,
      "Diseño Web", "width=300, height=200");
  }

  copyMessage() {
    this.notification.success('Copiado al porta papeles');
    // var aux = document.createElement("input");
    // aux.setAttribute("value", `${URL_RESOURCES.UNIKO}/${this.layoutService.coupleInformation.info.url}/save-date`);
    // document.body.appendChild(aux);
    // aux.select();
    // const copy = document.execCommand('copy');
    // // document.body.removeChild(aux);
    // if (copy) {
    //   this.notification.success('Copiado al porta papeles');
    // } else {
    //   var contenido = document.querySelector('#text');
    //   contenido['select']();
    //   const copy2 = document.execCommand('copy');
    //   this.notification.error('Lo sentimos no se pudo copiar '+copy2);
    // }

  }

}
