import { 
  Component, 
  OnInit, 
  Input, 
  Output, 
  EventEmitter 
} from '@angular/core';
import { Button } from './../../_models/data';
import {MatDialog} from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { LayoutService } from 'src/app/services/layout.service';
import { MenuService } from 'src/app/services/menu.service';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header-action',
  templateUrl: './header-action.component.html',
  styleUrls: ['./header-action.component.scss']
})
export class HeaderActionComponent implements OnInit {
  @Input() buttons: Array<Button>;
  @Input() menu: any;
  @Input() guest: any;
  constructor( 
    public dialog: MatDialog,
    private layoutService: LayoutService,
    private userService: UserService,
    private menuService: MenuService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService
  ) {     
    //console.log(this.buttons);
  }

  ngOnInit() {    
    console.log("////////")
    console.log(this.buttons);
    console.log(this.menu);
  }

  checkMenu(){
    if(this.menu && !this.guest){
      return this.layoutService.authorization.verify(this.menu._key);
    }
    return false
  }

  getButtonsLengt(){
    return this.buttons.length + (this.checkMenu()?1:0);
  }

  async onActive(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    try {
      this.spinner.show();            
      if (this.menu && this.checkMenu()) {
        this.menu.active = !this.menu.active;
        await this.menuService.setActive(this.menu.id, this.menu.active)        
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }
}
