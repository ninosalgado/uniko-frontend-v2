import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderSaveDateComponent } from './header-save-date.component';

describe('HeaderSaveDateComponent', () => {
  let component: HeaderSaveDateComponent;
  let fixture: ComponentFixture<HeaderSaveDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderSaveDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderSaveDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
