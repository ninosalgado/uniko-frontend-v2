import { Component, OnInit, Input } from '@angular/core';
import { Button } from 'src/app/_models/data';
import { LayoutService } from 'src/app/services/layout.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { CoupleService } from 'src/app/services/couple.service';
import { IAuthorization } from 'src/app/_interface/authorization';
import { DialogAlertaPro } from './../header/header.component';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header-profile',
  templateUrl: './header-profile.component.html',
  styleUrls: ['./header-profile.component.scss']
})
export class HeaderProfileComponent implements OnInit {
  @Input() buttons: Array<Button> = [];
  title: string;
  isOpen: boolean = false;
  couple: ICoupleInformation;
  auth: IAuthorization;
  constructor(
    public dialog: MatDialog,
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private router: Router
  ) {
    this.couple = this.layoutService.getCoupleInformation();
    this.getPlan();
  }

  getTitle() {
    switch (this.couple.weddingData.type.toUpperCase()) {
      case 'BABY':
        return `${this.couple.weddingData.nameP1.charAt(0).toUpperCase()}`;
      case 'WEDDING':
      default:
        return `${this.couple.weddingData.nameP1.charAt(0).toUpperCase()} & ${this.couple.weddingData.nameP2.charAt(0).toUpperCase()}`
    }
    return '';
  }

  ngOnInit() {
    this.title = this.getTitle();
  }

  go(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAlertaPro, { disableClose: false });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  async getPlan() {
    try {
      if (this.couple && this.couple.id) {
        this.auth = await this.coupleService.getPlanPermisions(this.couple.id);
        this.layoutService.authorization.info = this.auth;
      }
    } catch (e) {

    }
  }

  goUrl(event: Event, type: string) {
    event.preventDefault();
    event.stopPropagation();
    if (this.layoutService.authorization.verify(type)) {
      this.router.navigate(['/tools', type === 'GUEST_CONFIRMATION' ? 'guest' : 'domain']);
    } else {
      this.openDialog();
    }
  }

  logout(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.layoutService.clear();
    location.reload();
  }

}
