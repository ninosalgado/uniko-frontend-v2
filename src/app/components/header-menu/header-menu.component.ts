import { Component, OnInit, Input } from '@angular/core';
import { UserService } from './../../services/user.service';
import { LayoutService } from './../../services/layout.service';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss'],
  providers: [UserService]
})
export class HeaderMenuComponent implements OnInit {
  menus: Array<any> = [];
  title: string;
  open = false;
  guest: boolean = false;
  url: string;
  couple: ICoupleInformation;
  constructor(
    private userService: UserService,
    private layoutService: LayoutService
  ) {
    this.couple = this.layoutService.getCoupleInformation();
    this.url = this.couple.url;
    this.guest = this.layoutService.getCurrentUser().id ? false : true;
  }

  getTitle() {
    switch (this.couple.weddingData.type.toUpperCase()) {
      case 'BABY':
        return `${this.couple.weddingData.nameP1.charAt(0).toUpperCase()}`;
      case 'WEDDING':
      default:
        return `${this.couple.weddingData.nameP1.charAt(0).toUpperCase()} & ${this.couple.weddingData.nameP2.charAt(0).toUpperCase()}`
    }
    return '';
  }

  ngOnInit() {
    this.getMenus();
    this.title = this.getTitle();
  }

  isActive(menu: string) {
    if (this.guest) {
      const _menu = this.menus.find(data => data._key === menu);
      if (_menu && _menu.active) {
        return true
      }
      return false;
    } else {
      return true;
    }
  }

  onClose() {
    this.open = false;
  }

  async getMenus(): Promise<void> {
    this.menus = await this.userService.getMenu(this.layoutService.getCoupleInformation().id);
    console.log(this.menus);
  }

}
