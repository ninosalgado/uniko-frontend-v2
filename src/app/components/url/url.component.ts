import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CoupleService } from 'src/app/services/couple.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-url',
  templateUrl: './url.component.html',
  styleUrls: ['./url.component.scss']
})
export class UrlComponent implements OnInit {
  url: string;
  constructor(
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private coupleService: CoupleService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.url = this.route.snapshot.params.url;
    this.get();
  }

  ngOnInit() {
  }

  async get(): Promise<void> {
    try {
      this.spinner.show();
      const couple: ICoupleInformation = await this.coupleService.findOneRestrict(this.url);
      this.layoutService.setCoupleInformation(couple);
      const auth = await this.coupleService.getPlanPermisions(this.layoutService.coupleInformation.info.id);
      const _menus = await this.userService.getMenu(this.layoutService.getCoupleInformation().id);
      const guest = this.layoutService.isGuest();
      if (guest) {
        this.layoutService.activeMenuRedirect(_menus);
        return;
      }
      this.layoutService.authorization.info = auth;
      this.layoutService.redirect();
    } catch (e) {
      this.notification.error(e);
      this.router.navigate(['/search']);
    } finally {
      this.spinner.hide();
    }
  }

}
