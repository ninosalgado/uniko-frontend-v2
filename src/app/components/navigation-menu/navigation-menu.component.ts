import { Component, OnInit, Input } from '@angular/core';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogAlertaPro } from './../header/header.component';
import { MatDialog } from '@angular/material';
import { ICart } from 'src/app/_interface/cart';
import { UserService } from 'src/app/services/user.service';
import { URL_RESOURCES } from 'src/environments/environment';
@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent implements OnInit {
  @Input() guest: boolean;
  @Input() couple: ICoupleInformation;
  @Input() menuKey: string;
  @Input() menus: any;
  @Input() plan: any;
  @Input() public: boolean;
  @Input() off: boolean = false;
  isOpenMenu: boolean = false;
  isOpenMWT: boolean = false;
  isOpenProfile: boolean = false;
  isOpenCart = false;
  cart: ICart;
  title: string;
  guestUrl: string;
  activeMenus: any;
  totalItems;
  bolsaCompra: boolean = true;
  global: any = URL_RESOURCES.GLOBAL;
  totalAmmount: number;
  constructor(
    public dialog: MatDialog,
    private layoutService: LayoutService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    if (!this.public && !this.off) {
      this.layoutService.cart.cartEmitter.subscribe(data => {
        this.isOpenCart = true;
        this.cart = this.layoutService.cart.cart;
        this.totalItems = this.layoutService.cart.totalItems;
      });
      this.cart = this.layoutService.cart.cart;
      this.totalItems = this.layoutService.cart.totalItems;
      this.totalAmmount = this.cart.products.reduce((total, gift) => {
        return total + gift.price;
      }, 0) / this.layoutService.settings.info.currencyChange;
      this.title = this.getTitle();
    }
  }

  getTitle() {
    console.log(this.couple.weddingData.type);
    switch (this.couple.weddingData.type) {
      case 'BABY':
        return `${this.couple.weddingData.nameP1.charAt(0).toUpperCase()}`;
      case 'WEDDING':
      default:
        return `${this.couple.weddingData.nameP1.charAt(0).toUpperCase()} & ${this.couple.weddingData.nameP2.charAt(0).toUpperCase()}`
    }
    return '';
  }

  emptyProduct(obj) {
    return (this.cart.product && (Object.keys(this.cart.product).length === 0));
  }

  emptyProductList(obj) {
    return (this.cart.products && this.cart.products.length === 0);
  }

  formatPrice(articulo) {
    return articulo.price / this.layoutService.settings.info.currencyChange;
  }

  get currency() {
    return this.layoutService.settings.info.currency;
  }

  isActive(menu: string) {
    if (this.guest && this.menus) {
      const _menu = this.menus.find(data => data._key === menu);
      if (_menu && _menu.active) {
        return true
      }
      return false;
    } else {
      return true;
    }
  }

  onClose() {
    this.isOpenCart = false;
  }

  mapMenuInactive(key) {
    if (this.plan) {
      const menu = this.plan.planAggre.find(item => item.listAggre.keyMenu == key);
      if (menu) {
        return !menu.isActive;
      }
    }
    return false;
  }

  mapMenuSelected(key) {
    if (this.menuKey) {
      return this.menuKey == key;
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAlertaPro, { disableClose: false });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  goUrlTools(event: Event, type: string) {
    event.preventDefault();
    event.stopPropagation();
    let url = [];
    switch (type) {
      case "GUEST_CONFIRMATION": url = ["/tools", "guest"];
        break;
      case "DOMINE": url = ["/tools", "domain"];
        break;
      case "WEDDING_ACADEMY": url = ["weddingAccademy"];
        break;
      case "TICKET": url = ["/gift-table", "tickets"];
        break;
    }
    if (this.layoutService.authorization.verify(type)) {
      this.router.navigate(url);
    } else {
      this.openDialog();
    }
  }

  logout(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.layoutService.clear();
    location.reload();
  }
}
