import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AnswerService } from './../../services/answer.service';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss']
})
export class AnswerComponent implements OnInit {
  guestBook: any;
  answer: any = {
    comment: '',
    isPublic: false,
    guestBookId: ''
  }
  constructor(
    public dialogRef: MatDialogRef<AnswerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private answerService: AnswerService
  ) { 
    this.guestBook = this.data;
    this.answer.guestBookId = this.guestBook.id;
  }

  ngOnInit() {
  }

  async save(): Promise<void> {
    try {
      console.log(this.answer);
      const answer = await this.answerService.addAnswer(
        this.answer.guestBookId,
        this.answer.comment,
        this.answer.isPublic
      );
      this.close();
    } catch(err) {
      console.log(err);
    }
  }

  close(): void {
    this.dialogRef.close();
  }

}
