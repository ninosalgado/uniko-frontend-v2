import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AnswerComponent } from './answer/answer.component';
import { AnswerService } from './../services/answer.service';
import { LayoutService } from './../services/layout.service';
import { CoupleService } from '../services/couple.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Button, ImageSnippet, CurrentUser } from '../_models/data';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationsService } from '../services/notifications.service';

@Component({
  selector: 'app-guest-book',
  templateUrl: './guest-book.component.html',
  styleUrls: ['./guest-book.component.scss']
})
export class GuestBookComponent implements OnInit {
  imgDefault = 'https://uniko-prod.s3-us-west-2.amazonaws.com/guest-gif/WhatsApp+Video+2019-08-02+at+16.25.42.mp4?type=video';
  default2 = 'https://uniko-prod.s3-us-west-2.amazonaws.com/guest-gif/giphy.gif';
  guestBooks: any[] = [];
  guest: boolean = false;
  buttons: Array<Button> = [];
  sendMesageForm: FormGroup;
  sendAnswerForm: FormGroup;
  isOpenEdit: boolean = false;
  isDeleteOpen: boolean = false;
  submitAnswer: boolean = false;
  isOpen: boolean = false;
  selectedFile: any;
  file: any;
  submit: boolean = false;
  guestBook: any;
  load: boolean = false;
  targetGuest: any;
  eventType: string = 'WEDDING';
  constructor(
    private dialog: MatDialog,
    private answerService: AnswerService,
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: NotificationsService,
  ) {
    this.guest = this.layoutService.isGuest();
    if (this.guest) {
      localStorage.removeItem("currentUser");
      this.layoutService.setCurrentUser(new CurrentUser);
    }
    this.generateButtons();
    this.get();
  }

  ngOnInit() {
    this.sendMesageForm = this.formBuilder.group({
      question: ['', Validators.required],
      names: ['', Validators.required],
      email: ['', Validators.required]
    });
    this.sendAnswerForm = this.formBuilder.group({
      answer: ['', Validators.required],
      question: ['']
    });
  }

  generateButtons() {
    this.buttons = [];
    if (this.guest) {
      const add = new Button(
        'Escriba una dedicatoria',
        this.modalAdd,
        'fa fa-comment',
        null,
        this
      );
      this.buttons = [add];
    } else {

    }
  }

  openDeleteModal(_guest) {
    this.isDeleteOpen = true;
    this.targetGuest = _guest;
  }

  getImageToVideo(guestBook) {
    const isExist = guestBook.img.length ? true : false;
    const link = guestBook.img === this.imgDefault ? this.default2 : guestBook.img;
    const existType = link.search("type");
    if (existType > 10) {
      try {
        const type = link.slice(existType);
        const typeArray = type.split('=');
        const _type = atob(typeArray[1]);
        if (_type.search('video') >= 0 || (typeArray[1].search('video') >= 0)) {
          return `<video src="${link}" controls=""></video>`;
        } else {
          return `<img src="${link}">`;
        }
      } catch (e) {
        return isExist ? `<img src="${link}">` : '';
      }
    } else {
      return isExist ? `<img src="${link}">` : '';
    }
  }


  async upload(event, type: string) {
    if (event.target.files && event.target.files[0]) {
      if (this.checkFileSize(event)) {
        this.notification.warning("El tamaño del archivo no debe exceder los 5 MB");
        document.getElementById('new-gift-image').nodeValue = '';
        return
      }
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async (data) => {
        console.log(data);
        this.selectedFile = event.target.files[0];
        this.file = data.target['result'];
      };
    }
  }

  checkFileSize(fileEvent: any) {
    const file = fileEvent.target.files[0];
    return (file.size / 1024 / 1024) > 5;
  }

  onUpload(event: Event, element: string) {
    event.stopPropagation();
    event.preventDefault();
    document.getElementById(element).click();
  }

  modalAdd(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    const _this: any = this['parent'];
    _this.add()
  }

  add() {

    this.isOpen = true;
  }


  contestaMensaje() {
    const dialogRef = this.dialog.open(DialogContestaMensaje, { data: 'idMensaje', disableClose: true });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        console.log("Guardar");
      }
      else {
        console.log("Cancelar");
      }
    });
  }
  async get(): Promise<void> {
    try {
      this.spinner.show();
      if (this.guest) {
        const couple = await this.coupleService.findOneRestrict(
          this.route.snapshot.parent.params.url
        );
        this.layoutService.setCoupleInformation(couple);
      }
      const auth = await this.coupleService.getPlanPermisions(this.layoutService.coupleInformation.info.id);
      this.layoutService.authorization.info = auth;
      if (!this.layoutService.authorization.verify('GUEST_BOOK')) {
        this.layoutService.redirect();
      }
      this.guestBooks = await this.answerService.getGuestbook(
        this.layoutService.getCoupleInformation().fakeid
      );
      this.load = true;
      if (this.layoutService.isGuest() && this.route.snapshot.queryParams.comment) {
        this.isOpen = true;
      }
      this.eventType = this.layoutService.getCoupleInformation().weddingData.type ? this.layoutService.getCoupleInformation().weddingData.type : this.eventType;
    } catch (err) {
      this.router.navigate(['/search']);
      this.notification.error(err);
    } finally {
      this.spinner.hide();
    }
  }

  onActive(active) {
  }

  openDialog(event: Event, guest: any) {
    event.preventDefault();
    event.stopPropagation();
    const dialogRef = this.dialog.open(AnswerComponent, {
      width: '700px',
      data: guest
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  async remove(_guest) {
    try {
      this.spinner.show();
      this.isDeleteOpen = false;
      const result = await this.answerService.remove(_guest.id);
      if (result) {
        this.guestBooks = await this.answerService.getGuestbook(
          this.layoutService.getCoupleInformation().fakeid
        );
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }


  async changePublic(event: Event, guest) {
    try {
      this.spinner.show();
      guest.isPublic = !guest.isPublic;
      const _guest = await this.answerService.isShow(guest.isPublic, guest.id);
      console.log(_guest);
    } catch (err) {
      this.notification.error(err);
    } finally {
      this.spinner.hide();
    }
  }

  async saveAnswer(): Promise<void> {
    try {
      this.spinner.show();
      this.submitAnswer = true;
      if (this.sendAnswerForm.invalid) {
        return;
      }
      const answer = await this.answerService.addAnswer(
        this.guestBook.id,
        this.sendAnswerForm.value.answer,
        this.guestBook.isPublic
      );
      this.isOpenEdit = false;
      this.get();
    } catch (err) {
      console.log(err);
      this.notification.error(err);
    } finally {
      this.spinner.hide();
    }
  }

  closeModal() {
    this.isOpenEdit = false;
  }

  closeModalGuest() {
    this.isOpen = false;
  }

  onEdit(event: Event, guest: any) {
    this.guestBook = guest;
    this.isOpenEdit = true;
    this.sendAnswerForm.patchValue({
      answer: '',
      question: guest.comment
    });
    this.sendAnswerForm.controls.question.disable();
  }

  async save() {
    try {
      this.spinner.show();
      this.submit = true;
      let coverPhoto;
      if (this.sendMesageForm.invalid) {
        return;
      }
      if (this.selectedFile) {
        coverPhoto = await this.coupleService.addCoverPhotoGuest(
          this.layoutService.getCoupleInformation().id,
          this.selectedFile
        );
      }
      const guest = await this.answerService.add({
        comment: this.sendMesageForm.value.question,
        img: coverPhoto ? coverPhoto.url : '',
        names: this.sendMesageForm.value.names,
        email: this.sendMesageForm.value.email,
        fakeid: this.layoutService.coupleInformation.info.fakeid
      });
      this.notification.success('Se le enviará un correo cuando respondan', 'Gracias su mensaje se envío');
      this.isOpen = false;
      this.sendMesageForm.patchValue({
        question: '',
        names: '',
        email: ''
      });
      this.file = null;
      this.selectedFile = null;
    } catch (e) {
      this.notification.error(e)
    } finally {
      this.spinner.hide();
    }
  }

  close() {

  }
}

@Component({
  selector: 'contesta-mensaje-dialog',
  templateUrl: './contesta-mensaje-dialog.html',
  styleUrls: ['./guest-book.component.scss']
})
export class DialogContestaMensaje {
  answer: any = "";
  constructor(public matDialogRef: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: any) {

  }
}
