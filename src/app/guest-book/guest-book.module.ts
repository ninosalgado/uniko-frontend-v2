import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GuestBookComponent } from './../guest-book/guest-book.component';
import { GuestBookRoutingModule } from './guest-book-routing.module';
import { HeaderComponent } from './../components/header/header.component'; 
import { HeaderMenuComponent } from './../components/header-menu/header-menu.component';
import { SharedModule } from './../shared/shared.module';
import { AnswerComponent } from './answer/answer.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogContestaMensaje } from './../guest-book/guest-book.component';

@NgModule({
  declarations: [
    GuestBookComponent,
    AnswerComponent,
    DialogContestaMensaje
    // HeaderComponent,
    // HeaderMenuComponent
  ],
  imports: [
    CommonModule,
    GuestBookRoutingModule,
    SharedModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    AnswerComponent,
    DialogContestaMensaje
  ]
})
export class GuestBookModule { }
