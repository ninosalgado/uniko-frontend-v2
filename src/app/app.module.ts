import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AuthGuard } from './_guards/auth.guard';

import { NotificationsService } from './services/notifications.service';
import { LayoutService } from './services/layout.service';
import { UserService } from './services/user.service';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { AgmCoreModule } from '@agm/core';
import { UrlComponent } from './components/url/url.component';
import { DatePipe } from '@angular/common';
import { ReferidosComponent } from './referidos/referidos.component';
import { HeaderComponent } from './components/header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    UrlComponent,
    ReferidosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    NgbModule,
    NgxSpinnerModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC-cNfPK1guu4XvoxSpktEIkg_xvf7-gHU',
      apiVersion: '3.22', //defaults to latest 3.X anyhow
      libraries: ["places"],
      region: 'MX'
    })
  ],
  providers: [
    AuthGuard,
    DatePipe,
    NotificationsService,
    LayoutService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
