import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HeaderComponent, DialogCrearRegalo , DialogAlertaPro } from './../components/header/header.component'; 
import { HeaderMenuComponent } from './../components/header-menu/header-menu.component';
import { HeaderActionComponent } from './../components/header-action/header-action.component';
import { MatDialogModule } from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import { MaterialModule } from '../material.module';
import { HeaderGuestComponent } from '../components/header-guest/header-guest.component';
import { ModalComponent } from '../components/modal/modal.component';
import { HeaderProfileComponent } from '../components/header-profile/header-profile.component';
import { ModalCartComponent } from '../components/modal-cart/modal.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { GuestArticuloComponent } from '../site/guest-articulo/guest-articulo.component';
import { DialogAlertaBorrar } from '../gift-table/articulo-tarjeta/articulo-tarjeta.component';
import { HeaderSaveDateComponent } from '../components/header-save-date/header-save-date.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from '../components/footer/footer.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ShareComponent } from '../components/share/share.component';
import { NavigationMenuComponent } from '../components/navigation-menu/navigation-menu.component';
import { NoticeComponent } from '../components/notice/notice.component';
import { FillPipe } from '../_helpers/fill.pipe';

@NgModule({
  declarations: [    
    HeaderComponent,
    HeaderMenuComponent,
    NavigationMenuComponent,
    NoticeComponent,
    HeaderActionComponent,
    HeaderProfileComponent,
    DialogAlertaPro,
    DialogCrearRegalo,
    HeaderGuestComponent,
    ModalComponent,
    ModalCartComponent,
    DialogAlertaBorrar,
    HeaderSaveDateComponent,
    GuestArticuloComponent,
    FooterComponent,
    ShareComponent,
    FillPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    ImageCropperModule,
    ReactiveFormsModule,
    FormsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
  ],
  exports: [
    HeaderComponent,
    HeaderMenuComponent,
    NavigationMenuComponent,
    NoticeComponent,
    HeaderActionComponent,
    HeaderGuestComponent,
    HeaderProfileComponent,
    MaterialModule,
    ModalComponent,
    ModalCartComponent,
    ImageCropperModule,
    DialogAlertaBorrar,
    HeaderSaveDateComponent,
    GuestArticuloComponent,
    FooterComponent,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    ShareComponent,
    FillPipe
  ],
  entryComponents: [
    DialogAlertaPro,
    DialogCrearRegalo,
    DialogAlertaBorrar,

  ],
})
export class SharedModule { }