import { Component, OnInit, Injectable, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { type } from 'os';
import { TEMPLATES } from 'src/app/_const/templates';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ISaveDate } from 'src/app/_interface/sateDate';
import { SaveDateService } from 'src/app/services/save-date.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
  @Input() templateId: number;
  colorDetail = 'red';
  mobileActive: boolean = false;
  couple: ICoupleInformation;
  saveDate: ISaveDate;
  colors: any[];
  svgTemplate: SafeHtml;
  svg: string;
  editDesing: boolean;
  openColor = false;
  openColorLetter = false;
  listBackround = TEMPLATES;
  template:any;
  templateObj:any;
  backgroundTemplate: any;
  color: any;
  model: string; 
  sizes: any[];
  sizesModel: any;
  fontModel: any;
  sizesHeightModel: any;
  latterSpaceModel: any;
  sizesHeight: any[];
  latterSpace: any[];
  constructor(
    private domSanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private router: Router,
    private saveDateService: SaveDateService,
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService
  ) {
    this.templateId = this.route.snapshot.params.id;
    const range = (start, stop, step) => Array.from({ length: (stop - start) / step + 1}, (_, i) => start + (i * step));
    this.sizes = range(5,80, 1);
    this.sizesHeight = range(0, 10, 0.5);
    this.latterSpace = range(1,40, 1);
    this.couple = this.layoutService.getCoupleInformation();
   }
  
  ngOnInit() {
    const template = Object.assign(this.listBackround[this.templateId]);
    this.templateObj = JSON.parse(JSON.stringify(template));
    this.setImg();
    this.setColor();
    this.setModel();
    this.info();
  }

  ngOnDestroy(){
    this.templateObj = null;
  }

  setModel() {
    this.sizesModel = this.templateObj[this.templateObj.select].fontSize;
    this.sizesHeightModel = this.templateObj[this.templateObj.select].fontSize;
    this.fontModel = this.templateObj[this.templateObj.select].fontFamily;
    this.latterSpaceModel = this.templateObj[this.templateObj.select].latterSpace;
    this.model = this.templateObj[this.templateObj.select].text;
  }

  changeText($event) {
    console.log($event);
    this.templateObj[this.templateObj.select].text = this.model;
  }

  get img() {
    const img = this.templateObj.backgroundTemplate.find(data => data.select);
    return `${this.templateObj.name}${img.index}.png`;
  }

  get background() {
    const color = this.templateObj.colors.find(data => data.select);
    return color ? color.color : '';
  }

  setEditText(type, _style) {
    console.log(type, _style);
    this.templateObj[this.templateObj.select][type] = _style;
  }

  changeInput(type) {
    this.templateObj.select = type;
    this.setModel();
    this.mobileActive = true;
    document.getElementById('text').focus();
    // window.document.getElementById('text');
  }

  setImg() {
    this.backgroundTemplate = this.templateObj.backgroundTemplate.find(data => data.select);
  }

  setColor() {
    this.color = this.templateObj.colors.find(data => data.select);
    console.log(this.color);
  }

  loadSvg(svg){
    //this.svgTemplate = this.domSanitizer.bypassSecurityTrustHtml(svg);
  }

  selectColorBackground(index, color?: string){
    this.templateObj.colors.forEach((element, _index) => {
      element.select = _index !== index ? false : true;
      if (index == 4 && _index == 4) {
        element.color = color;
      }
    });
  }

  selectColor(index){
    this.templateObj.backgroundTemplate.forEach((element, _index) => {
      element.select = index === _index ? true : false;
    });
  }

  selectSavedColor(index){
    /*this.templateObj.backgroundTemplate.forEach((element, _index) => {
      element.select = element.index == index ? true : false;
    });*/
    console.log(index);
    this.templateObj.backgroundTemplate = this.templateObj.backgroundTemplate.map(data => {
      if (data.index == index) {
        data.select = true;
        data.color = this.saveDate.colors.color;
      } else {
        data.select = false;
      }
      return data;
    });

    console.log(this.templateObj.colors);

    this.templateObj.colors = this.templateObj.colors.map(data => {
      if (data.index == (index +1)) {
        data.select = true;
        data.color = this.saveDate.colors.color;
      } else {
        data.select = false;
      }
      return data;
    });
  }

  handleChangeComplete(color) {
    this.selectColorBackground(4, color.color.hex);
  }

  handleChangeCompleteLatter(color) {
    this.setEditText('color', color.color.hex);
  }

  prev() {
    this.router.navigate(['/', 'save-date', 'template', this.templateId]);
  }

  async info() {
    try {
      this.spinner.show();
      this.saveDate = await this.saveDateService.getSaveDate(this.couple.id);
      console.log(this.templateId, this.saveDate.templateId);
      if (this.templateId == this.saveDate.templateId) {
        //this.templateObj = this.listBackround[Number(this.saveDate.templateId)]
        this.templateObj.title = this.saveDate.title;
        this.templateObj.nametitle = this.saveDate.nametitle;
        this.templateObj.titleDate = this.saveDate.titleDate;
        this.templateObj.date = this.saveDate.date;
        this.templateObj.titleDescription = this.saveDate.titleDescription;
        this.templateObj.description = this.saveDate.description;
        this.selectSavedColor(this.saveDate.backgroundTemplate.index);
        console.log(this.saveDate, this.templateObj.backgroundTemplate);
      } else {
        this.templateObj.title.text = this.saveDate.title.text;
        this.templateObj.nametitle.text = this.saveDate.nametitle.text;
        this.templateObj.titleDate.text = this.saveDate.titleDate.text;
        this.templateObj.date.text = this.saveDate.date.text;
        this.templateObj.titleDescription.text = this.saveDate.titleDescription.text;
        this.templateObj.description.text = this.saveDate.description.text;
      }
      console.log(this.saveDate);
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }

  
  async save() {
    try {
      this.spinner.show();
      this.saveDate = {
        desktop: this.templateObj.desktop,
        template: this.templateObj.name,
        templateId: this.templateId,
        opacity: this.templateObj.opacity,
        title: this.templateObj.title,
        nametitle: this.templateObj.nametitle,
        titleDate: this.templateObj.titleDate,
        date: this.templateObj.date,
        titleDescription: this.templateObj.titleDescription,
        description: this.templateObj.description,
        backgroundTemplate: this.templateObj.backgroundTemplate.find(data => data.select === true),
        colors: this.templateObj.colors.find(data => data.select === true),
        coupleAccountId: this.couple.id,

      }
      await this.saveDateService.setSaveDate(this.saveDate);
      console.log(this.saveDate);
      this.notification.success("Template guardado");
    } catch (e) {
      this.notification.error(e)
    } finally {
      this.spinner.hide();
    }
  }
}

