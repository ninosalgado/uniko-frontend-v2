import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderProfileComponent } from './../components/header-profile/header-profile.component';
import { SaveDateRoutingModule } from './save-date-routing.module';
import { SaveDateComponent } from './save-date.component';
import { EditaTemplateComponent } from './edita-template/edita-template.component';
import { TemplatesComponent } from './templates/templates.component';
import { PreviewComponent } from './preview/preview.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material.module';
import { EditorComponent } from './editor/editor.component';
import { ColorSketchModule } from 'ngx-color/sketch';
import { FormsModule } from '@angular/forms';
import { GuestViewComponent } from './guest-view/guest-view.component';

@NgModule({
  declarations: [
    SaveDateComponent,
    EditaTemplateComponent,
    TemplatesComponent,
    PreviewComponent,
    EditorComponent,
    GuestViewComponent
],
  imports: [
    CommonModule,
    FormsModule,
    SaveDateRoutingModule,
    SharedModule,
    MaterialModule,
    ColorSketchModule
  ]
})
export class SaveDateModule { }
