import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { SaveDateService } from 'src/app/services/save-date.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-guest-view',
  templateUrl: './guest-view.component.html',
  styleUrls: ['./guest-view.component.scss']
})
export class GuestViewComponent implements OnInit {

  constructor(
    private layoutSerice: LayoutService,
    private saveDateService: SaveDateService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private route: ActivatedRoute
  ) { 
    console.log(this.route.snapshot.parent);
  }

  ngOnInit() {
  }

}
