import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditaTemplateComponent } from './edita-template.component';

describe('EditaTemplateComponent', () => {
  let component: EditaTemplateComponent;
  let fixture: ComponentFixture<EditaTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditaTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditaTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
