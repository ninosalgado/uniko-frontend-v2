import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Button, ImageSnippet } from 'src/app/_models/data';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { TEMPLATES } from 'src/app/_const/templates';
import { SaveDate } from 'src/app/_models/saveDate';
import { ISaveDate } from 'src/app/_interface/sateDate';
import { SaveDateService } from 'src/app/services/save-date.service';
import html2canvas from 'html2canvas';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-edita-template',
  templateUrl: './edita-template.component.html',
  styleUrls: ['./edita-template.component.scss']
})
export class EditaTemplateComponent implements OnInit {
  templateId:any = {};
  isOpenImg: boolean = false;
  couple: ICoupleInformation;
  isOpenShare: boolean = false;
  buttons: Array<Button> = [];
  previw: boolean = false;
  activeEdit = false;
  template: string = '';
  listBackround = TEMPLATES;
  selectedFile: any;
  cover: any;
  saveDate: SaveDate;
  _saveDate: ISaveDate;
  image: any;
  templateObj:any;
  backgroundTemplate: any;
  color: any;
  isOpen: boolean = false;
  isShared: boolean = false;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  imageChooser: boolean = true;
  file: any;
  url: any;
  order66: boolean = false;
  constructor(
    private activatedRoute:ActivatedRoute,
    private router: Router,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private notification: NotificationsService,
    private saveDateService: SaveDateService,
    private datePipe: DatePipe

  ) {
    this.templateId = this.activatedRoute.snapshot.params.id;    
    this.cover = './../../../assets/img/templates/foto' + this.templateId + '.jpg';
    this.couple = this.layoutService.getCoupleInformation();
  }

  setButtons(){ 
    console.log("trying to set buttons:");   
    const buttonSave = new Button(
      'Guardar plantilla',
      this.setTemplate,
      'fa-save',
      null,
      this
    );
    const buttonShared = new Button(
      'Compartir',
      this.screenShared,
      'fa-share-alt',
      null,
      this
    );
    const buttonScreen = new Button(
      'Descargar imagen',
      this.prepareScreen,
      'fa-download',
      null,
      this
    );
    const buttonChangeTemplate = new Button(
      'Ver plantillas',
      null,
      'fa-chevron-circle-left',
      '/save-date'
    );

    if(this.templateId != this._saveDate.templateId){
      this.buttons =[buttonShared, buttonScreen, buttonSave, buttonChangeTemplate];  
    }else{
      this.buttons =[buttonShared, buttonScreen, buttonChangeTemplate];
    }
    
  }

  ngOnInit() {
    const template = Object.assign(this.listBackround[this.templateId]);
    this.templateObj = JSON.parse(JSON.stringify(template));
    this.get();
  }

  ngOnDestroy(){
    this.templateObj = null;
  }

  onClose() {
    this.isOpen = false;
  }

  onCloseShare() {
    this.isOpenShare = false;
  }

  prepareScreen(event: Event){
    event.stopPropagation();
    event.preventDefault();
    this['parent'].isOpen = true;
    this['parent'].imageChooser = true;
  }

  executeorder66(type: string){
    try{
      switch(type){
        case 'mobile':
          this.order66 = true;
          break;
        case 'desktop':
          this.order66 = false;
          break;  
      }
      console.log(this.order66);
      this.imageChooser = false;
    }catch(e){
      console.log(e);
    }
  }

  screen() {
    try{
      this.spinner.show();
      /*const element = document.getElementById('screen-modal');
      document.getElementsByClassName('editarArt');
      this['parent'].isOpen = true;
      */
      html2canvas(document.getElementById('pseudo-screen'),{
        allowTaint: true,
        removeContainer: true,
        proxy: "https://proxy-image.uniko.co",
      }).then((canvas) => {
        canvas.style.width = '100%';
        canvas.style.height = 'auto';
        this.image = canvas.toDataURL("image/png;base64");
        this.download();
        //console.log(this['parent'].image)
        /*if (element.children.length) {
          element.children[0].remove();
        }
        element.appendChild(canvas);*/
      });
    }catch(e){
      console.log(e);
      this.notification.error(e);
    }finally{
      this.spinner.hide();
    }
  }

  screenShared(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    this['parent'].isOpenShare = true;
  }

  setTemplate(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    this["parent"].templateSave();
  }

  baseB4ToBlob(b64Data, contentType, sliceSize?: any) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers); 
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  download() {    
    const contentType = 'image/png';
    const b64Data = this.image.split(',')[1];
    const blob = this.baseB4ToBlob(b64Data, contentType, b64Data.length);
    const url = window.URL.createObjectURL(blob);
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.href = url;
    a.download = `${this.couple.url}.png`;
    a.click();
    window.URL.revokeObjectURL(url);
    this.isOpen = false;
  }

  goPreviw(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    
  }
  editarImg(){
    alert('Editar Imagen');
  }
  editarCont(){
    this.router.navigate(['/save-date', 'edit-template', this.templateId])
  }


  onUpload(event: Event, element: string) {
    event.stopPropagation();
    event.preventDefault();
    document.getElementById(element).click();
  }

  async upload(event, type: string) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async() => {
        //  await this.uploadCoverPhoto(url, file);
        // this.selectedFile = new ImageSnippet(url, file);
        this.isOpenImg = true;
        this.imageChangedEvent = event;
      };
    }
  }

  async uploadCoverPhoto():Promise<void> {
    try {
      this.spinner.show();
      this.selectedFile = new ImageSnippet('', this.file);
      const coverPhoto = await this.coupleService.addCoverPhotoSaveDate(
        this.layoutService.getCoupleInformation().id, 
        this.selectedFile.file
      );
      console.log(coverPhoto);
        this.cover = coverPhoto.url;
        await this.save();
        this.isOpenImg = false;
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async get(): Promise<void> {
    try {
      this.spinner.show();
      this._saveDate = await this.saveDateService.getSaveDate(this.couple.id);
      console.log("Trying to get");
      this.setButtons();
      if (this._saveDate && String(this.templateId) === String(this._saveDate.templateId)) {
        console.log("Alpha", this._saveDate);
        //this.templateObj = this.listBackround[Number(this._saveDate.templateId)]
        this.templateObj.title = this._saveDate.title;
        this.templateObj.nametitle = this._saveDate.nametitle;
        this.templateObj.titleDate = this._saveDate.titleDate;
        this.templateObj.date = this._saveDate.date;
        this.templateObj.titleDescription = this._saveDate.titleDescription;
        this.templateObj.description = this._saveDate.description;             
        if (this._saveDate.cover) {
          this.cover = this._saveDate.cover;
        }
        this.selectColor(this._saveDate.backgroundTemplate.index);
      } else if (this._saveDate) {
        console.log("Beta");
        this.templateObj.title.text = this._saveDate.title.text;
        this.templateObj.nametitle.text = this._saveDate.nametitle.text;
        this.templateObj.titleDate.text = this._saveDate.titleDate.text;
        this.templateObj.titleDescription.text = this._saveDate.titleDescription.text;
        this.templateObj.description.text = this._saveDate.description.text;
        this.templateObj.date.text = this._saveDate.date.text;
        this.templateObj.description.text = this._saveDate.description.text;
        if (this._saveDate.cover) {
          this.cover = this._saveDate.cover;
        }
      } else {
        console.log("Gamma");
        this.templateObj.title.text = this._saveDate.title.text;
        this.templateObj.nametitle.text = `${this.couple.weddingData.nameP1} ${this.couple.weddingData.nameP2}`;
        this.templateObj.titleDate.text = this.datePipe.transform(this.couple.weddingData.date, 'dd . MMM . yyyy')
        this.templateObj.titleDescription.text = this.couple.weddingData.date;
        this.templateObj.description.text = this._saveDate.description.text;
        this.templateObj.date.text = this._saveDate.date.text;
        this.templateObj.description.text = this._saveDate.description.text;
      }
      //console.log(this.templateObj);
    } catch (e) {
      console.log("Error error!");
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }

  get img() {
    const img = this.templateObj.backgroundTemplate.find(data => data.select);
    return `${this.templateObj.name}${img ? img.index : ''}.png`;
  }

  get background() {
    const color = this.templateObj.colors.find(data => data.select);
    return color ? color.color : '';
  }

  selectColor(index){
    /*this.templateObj.backgroundTemplate.forEach((element, _index) => {
      element.select = element.index == index ? true : false;
    });*/
    console.log(index);
    this.templateObj.backgroundTemplate = this.templateObj.backgroundTemplate.map(data => {
      if (data.index == index) {
        data.select = true;
        data.color = this._saveDate.colors.color;
      } else {
        data.select = false;
      }
      return data;
    });

    console.log(this.templateObj.colors);

    this.templateObj.colors = this.templateObj.colors.map(data => {
      if (data.index == (index +1)) {
        data.select = true;
        data.color = this._saveDate.colors.color;
      } else {
        data.select = false;
      }
      return data;
    });
  }

  setImg() {
    this.backgroundTemplate = this.templateObj.backgroundTemplate.find(data => data.select);
  }

  setColor() {
    this.color = this.templateObj.colors.find(data => data.select);
    console.log(this.color);
  }

  async templateSave(){
    try{
      this.spinner.show();

      let saveDate = this._saveDate;
      
      saveDate.template = this.templateObj.name,
      saveDate.templateId = this.templateId;
      saveDate.opacity = this.templateObj.opacity,
      saveDate.backgroundTemplate = this.templateObj.backgroundTemplate.find(data => data.select === true);
      console.log(this.templateObj.backgroundTemplate.find(data => data.select === true));
      saveDate.colors = this.templateObj.colors;
      saveDate.title = this.templateObj.title;
      saveDate.nametitle = this.templateObj.nametitle;
      saveDate.titleDate = this.templateObj.titleDate;
      saveDate.titleDescription = this.templateObj.titleDescription;
      saveDate.description = this.templateObj.description;
      saveDate.date = this.templateObj.date;
      saveDate.description = this.templateObj.description;

      await this.saveDateService.setSaveDate(saveDate);
      this.notification.success("Plantilla actualizada");

    } catch (e) {
      console.log(e);
      this.notification.error(e)
    } finally {
      this.spinner.hide();
    }
  }

  async save() {
    try {
      this.spinner.show();
      if (!this._saveDate) {
        const saveDate = {
          desktop: this.templateObj.desktop,
          template: this.templateObj.name,
          templateId: this.templateId,
          opacity: this.templateObj.opacity,
          title: this.templateObj.title,
          nametitle: this.templateObj.nametitle,
          titleDate: this.templateObj.titleDate,
          date: this.templateObj.date,
          titleDescription: this.templateObj.titleDescription,
          description: this.templateObj.description,
          backgroundTemplate: this.templateObj.backgroundTemplate.find(data => data.select === true),
          colors: this.templateObj.colors.find(data => data.select === true),
          coupleAccountId: this.couple.id,
          cover: this.cover
        }
        await this.saveDateService.setSaveDate(saveDate);
        
      } else {
        this._saveDate.cover = this.cover;
        await this.saveDateService.setSaveDate(this._saveDate);
      }
      this.notification.success("Imagen actualizada");
    } catch (e) {
      console.log(e);
      this.notification.error(e)
    } finally {
      this.spinner.hide();
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
      this.file = event.file;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }



}
