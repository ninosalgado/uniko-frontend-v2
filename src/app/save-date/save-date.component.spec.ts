import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveDateComponent } from './save-date.component';

describe('SaveDateComponent', () => {
  let component: SaveDateComponent;
  let fixture: ComponentFixture<SaveDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
