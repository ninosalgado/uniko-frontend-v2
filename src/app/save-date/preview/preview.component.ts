import { Component, OnInit } from '@angular/core';
import { SaveDateService } from 'src/app/services/save-date.service';
import { Layout } from 'src/app/_models/layout';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SaveDate } from './../../_models/saveDate';
import { ToastrService } from 'ngx-toastr';
import { TEMPLATES } from 'src/app/_const/templates';
import { ISaveDate } from 'src/app/_interface/sateDate';
import { ActivatedRoute } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {
  saveDate: SaveDate;
  couple: ICoupleInformation;
  _saveDate: ISaveDate;
  listBackround = TEMPLATES;
  templateObj:any;
  backgroundTemplate: any;
  templateId: string;
  color: any;
  cover: any;
  constructor(
    private layoutSerice: LayoutService,
    private saveDateService: SaveDateService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private route: ActivatedRoute
  ) { 
    this.couple = this.layoutSerice.getCoupleInformation();
    this.templateId = this.route.snapshot.params.id;
    this.templateObj = this.listBackround[this.templateId];
    this.cover = './../../../assets/img/templates/foto' + this.templateId + '.jpg';
    this.get();
  }

  ngOnInit() {
  }

  async get(): Promise<void> {
    try {
      this.spinner.show();
      this._saveDate = await this.saveDateService.getSaveDate(this.couple.id);
      if (String(this.templateId) === String(this._saveDate.templateId)) {
        this.templateObj = this.listBackround[Number(this._saveDate.templateId)]
        this.templateObj.title = this._saveDate.title;
        this.templateObj.nametitle = this._saveDate.nametitle;
        this.templateObj.titleDate = this._saveDate.titleDate;
        this.templateObj.date = this._saveDate.date;
        this.templateObj.titleDescription = this._saveDate.titleDescription;
        this.templateObj.description = this._saveDate.description;
        this.templateObj.colors.forEach(data => {
          console.log(data.index, this._saveDate.colors.index);
          if (data.index === this._saveDate.colors.index) {
            data = this._saveDate.colors
          } else {
            data.select = false;
          }
        });
        console.log(this._saveDate.backgroundTemplate);
        this.selectColor(this._saveDate.backgroundTemplate.index);
        if (this._saveDate.cover) {
          this.cover = this._saveDate.cover;
        }
      } else if (this._saveDate) {
        this.templateObj.title.text = this._saveDate.title.text;
        this.templateObj.nametitle.text = this._saveDate.nametitle.text;
        this.templateObj.titleDate.text = this._saveDate.titleDate.text;
        this.templateObj.titleDescription.text = this._saveDate.titleDescription.text;
        this.templateObj.description.text = this._saveDate.description.text;
        this.templateObj.date.text = this._saveDate.date.text;
        this.templateObj.description.text = this._saveDate.description.text;
        if (this._saveDate.cover) {
          this.cover = this._saveDate.cover;
        }
      }
      console.log(this.templateObj);
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }

  get img() {
    const img = this.templateObj.backgroundTemplate.find(data => data.select);
    return `${this.templateObj.name}${img ? img.index : ''}.png`;
  }

  get background() {
    const color = this.templateObj.colors.find(data => data.select);
    return color ? color.color : '';
  }

  selectColor(index){
    this.templateObj.backgroundTemplate.forEach((element, _index) => {
      element.select = element.index == index ? true : false;
    });
  }

  setImg() {
    this.backgroundTemplate = this.templateObj.backgroundTemplate.find(data => data.select);
  }

  setColor() {
    this.color = this.templateObj.colors.find(data => data.select);
    console.log(this.color);
  }

  

}
