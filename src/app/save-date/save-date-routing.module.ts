import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SaveDateComponent } from './save-date.component';
import { EditaTemplateComponent } from './edita-template/edita-template.component';
import { PreviewComponent } from './preview/preview.component';
import { EditorComponent } from './editor/editor.component';

const routes: Routes = [{
  path: '',
  component: SaveDateComponent,
},
{
    path: '',
    children: [{
      path: 'edit-template/:id',
      component: EditorComponent
    },{
      path: 'template/:id',
      component: EditaTemplateComponent
    }]
},
{
    path: '',
    children: [{
      path: 'preview/:id',
      component: PreviewComponent
    }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaveDateRoutingModule { }
