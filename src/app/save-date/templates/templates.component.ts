import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SaveDateService } from '../../services/save-date.service';
import { LayoutService } from '../../services/layout.service';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.scss']
})
export class TemplatesComponent implements OnInit {
  saveDate: any;
  isSelected: boolean = false;
  constructor(
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private saveDateService: SaveDateService,
    private layoutService: LayoutService,
  ) { 
    this.getInfo();    
  }

  ngOnInit() {
  }

  async getInfo() {
    try {
      this.spinner.show();
      this.saveDate = await this.saveDateService.getSaveDate(this.layoutService.getCoupleInformation().id);
      if (this.saveDate) {
        this.isSelected = true;
      }
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
