import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { CoupleService } from 'src/app/services/couple.service';
import { URL_RESOURCES } from 'src/environments/environment';
import * as moment from 'moment';

@Component({
  selector: 'app-domain-manager',
  templateUrl: './domain-manager.component.html',
  styleUrls: ['./domain-manager.component.scss']
})
export class DomainManagerComponent implements OnInit {
  isOpen:boolean = false;
  isOpenShare: boolean = false;
  domainForm: FormGroup;
  couple: ICoupleInformation;
  activeDomain:string;
  domains:any[];
  currentDomain: string;
  isSolicited: boolean = false;
  isActive: boolean = false;
  fullDomain: string = "";
  diffDate: any = {
    days: '00',
    hours: '00',
    minutes: '00',
    seconds: '00'
  }
  date: any;
  hour: any;
  time: any;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private notification: ToastrService,
    private router: Router  
  ) { 
    this.couple = this.layoutService.getCoupleInformation();    
    console.log(this.couple);
  }

  async reloadCouple(){
    this.spinner.show();
    const couple = await this.coupleService.get(this.couple.id);
    this.couple = couple;
    this.layoutService.setCoupleInformation(this.couple);
    await this.getDomains();
    this.spinner.hide();
  }

  ngOnInit() {   
    this.domainForm = this.formBuilder.group({
      url1: ['', Validators.required],
      url2: ['', Validators.required],
      url3: ['', Validators.required]
    });
    this.reloadCouple();
  }
  async getDomains(){
    
    const extraInfo = await this.coupleService.getDomains(this.couple.id);
    this.domains = extraInfo.domines;
    if(this.domains.length){            
      const activeDomain = this.domains.find(item=> item.status == true);      
      if(activeDomain != null){    
        this.isActive = true;
        this.currentDomain = activeDomain.url
      }else{
        this.isSolicited = true;
        this.currentDomain = this.couple.url;
      }      
    }else{
      this.currentDomain = this.couple.url;
    }
    this.fullDomain = `${URL_RESOURCES.UNIKO}/${this.currentDomain}`;
  }

  maxYear(): boolean {
    const diffDays = moment(this.couple.weddingData.date).diff(moment(), 'day');
    if (diffDays > 360 ) {
      return true;
    }
    return false;
  }

  async saveUrls(){
    try {
      this.spinner.show();    
      if (this.maxYear()) {
        return this.notification.error('El evento es mayor al año');
      }
      if(this.isSolicited){
        this.isOpen = false;
        return;
      }
      if (this.domainForm.invalid) {
        return;
      }
      const url1 = this.domainForm.value.url1;
      const url2 = this.domainForm.value.url2;
      const url3 = this.domainForm.value.url3;

      let result = this.coupleService.generateDomains(url1,url2,url3);
      
      const couple = await this.coupleService.get(this.couple.id);
      this.couple = couple;
      this.layoutService.setCoupleInformation(this.couple);

      await this.getDomains();

      console.log(result);
    }catch(e){
      this.notification.error(e);
    }finally{
      this.spinner.hide();
    }    
  }

  onCloseShare(){
    this.isOpenShare = false;
  }
  
}