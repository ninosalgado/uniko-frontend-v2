import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeddingToolsComponent } from './wedding-tools.component';
import { GuestManagerComponent } from './guest-manager/guest-manager.component';
import { DomainManagerComponent } from './domain-manager/domain-manager.component';


const routes: Routes = [{
    path: '',
    component: WeddingToolsComponent,
    children: [{
      path:'',
      pathMatch: 'full',
      redirectTo: 'dashboard'
    },{
      path: 'guest',//invitados
      component: GuestManagerComponent
    },{
      path: 'domain',//dominio
      component: DomainManagerComponent
    }
    ]
}]



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeddingToolsRoutingModule { }
