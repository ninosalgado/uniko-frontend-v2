import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Button, IProductRegister } from '../../_models/data';
import { ICoupleInformation } from '../../_interface/coupleInformation';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { ErrorHandler } from '@angular/router/src/router';

@Component({
  selector: 'app-guest-manager',
  templateUrl: './guest-manager.component.html',
  styleUrls: ['./guest-manager.component.scss']
})
export class GuestManagerComponent implements OnInit {
  couple: ICoupleInformation;
  buttons: Array<Button> = [];
  fileUrl;
  viewList: boolean = false;
  guestList:any;
  currentOrder:number = 3;
  checkAll: boolean= true;
  viewOrder: boolean=false;
  guests:number = 0;
  confirmed:number = 0;
  @ViewChild('downloadTemplate') templateLink: ElementRef<HTMLElement>;
  @ViewChild('uploadList') inputField : ElementRef;

  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private coupleService: CoupleService,
    private notification: ToastrService,
    private router: Router,
    private sanitizer: DomSanitizer,
  ) { 
    const buttonDownload = new Button(
      'Descargar la plantilla',
      this.eventDownload,
      'fa-download'
    );
    const buttonUpload = new Button(
      'Subir archivo',
      this.eventUpload,
      'fa-upload'
    );

    this.buttons =[buttonDownload, buttonUpload];
    this.couple = this.layoutService.getCoupleInformation();
    console.log(this.couple);
  }

  ngOnInit() {    
    this.reloadCouple();    
  }

  async reloadCouple(){
    this.spinner.show();
    const couple = await this.coupleService.get(this.couple.id);
    this.couple = couple;
    this.layoutService.setCoupleInformation(this.couple);
    await this.getGuestList();
    this.spinner.hide();
  }
    
  setPreviewOrder(type){
    let itemsSorted  = <any[]> JSON.parse(JSON.stringify(this.guestList));
    try{
      switch(type){
        case 1:
          itemsSorted.sort(function(obj1, obj2) {
            // Ascending: lol
            return obj2.asistentesConfirm - obj1.asistentesConfirm;
          });
          break;
        case 2:
          itemsSorted.sort(function(obj1, obj2) {
            // Descending: lol
            return obj2.asistentes - obj1.asistentes;
          });
          break;
        case 3:
          itemsSorted.sort(function(obj1, obj2) {
            // Original: index ( previewOrder )
            return obj1.name.localeCompare(obj2.name);
          });
          break;
          //array.sort((a,b) => a.title.rendered.localeCompare(b.title.rendered));
      } 
    }
    catch(e){
      console.log(e)
    }
    finally{
      this.currentOrder = type;
      this.guestList = itemsSorted;
    }       
  }

  eventDownload(event){
    event.stopPropagation();
    event.preventDefault();
    document.getElementById('downloadTemplate').click();
  }

  eventUpload(event){
    event.stopPropagation();
    event.preventDefault();
    document.getElementById('uploadList').click();
  }

  confirmationsDownload(event){
    event.stopPropagation();
    event.preventDefault();
    document.getElementById('downloadList').click();
  }

  resetButtons(){
    const updateList = new Button(
      'Descarga Confirmaciones',
      this.confirmationsDownload,
      'fa-download'
    );
    this.buttons = [updateList]
  }

  async getGuestList(){    
    let result = await this.coupleService.getGuestList(this.couple.id);
    this.guestList = result;
    this.setListView();    
  }

  async setListView(){
    this.viewList = this.guestList.length?true:false;
    if(this.viewList){
      this.resetButtons();
    }

    let guests = this.guestList.length;
    let confirmed = this.guestList.reduce((total, item) => {
      return total + item.asistentesConfirm
    }, 0);

    this.guests = guests;
    this.confirmed = confirmed;
    return true;
  }

  async getTemplate(){
    this.spinner.show();
    let result = await this.coupleService.getGuestListTemplate();
    let a = document.createElement("a");
    document.body.appendChild(a);
    a.style.display ="none";
    let blob = result.data,
        url = window.URL.createObjectURL(new Blob([result], {type: "application/zip"}));
    a.href = url;
    a.download = 'Lista_invitados.xlsx';
    a.click();
    window.URL.revokeObjectURL(url);
    document.body.removeChild(a);
    this.spinner.hide();
  }

  async getConfirmations(){
    this.spinner.show();
    let result = await this.coupleService.getUpdatedGuestList();
    let a = document.createElement("a");
    document.body.appendChild(a);
    a.style.display ="none";
    let blob = result.data,
        url = window.URL.createObjectURL(new Blob([result], {type: "application/zip"}));
    a.href = url;
    a.download = 'Lista_invitados.xlsx';
    a.click();
    window.URL.revokeObjectURL(url);
    document.body.removeChild(a);
    this.spinner.hide();
  }

  async setList($event,form?: any){
    try{      
      this.spinner.show();
      if(this.guestList.length>0){
        this.notification.error('Ya existe una lista de invitados para tu evento.');
        return;
      }
      if ($event.target.files && $event.target.files[0]) {
        const file = $event.target.files[0];
        let result = await this.coupleService.uploadGuestList(this.couple.id,file);
        this.guestList = result.guests;        
      }      
    }catch(e){
      this.notification.error('Error al leer el archivo.');
    }finally{
      await this.setListView();
      this.inputField.nativeElement.value = null;     
      this.spinner.hide();
    }
  }

  getAssistants(item){
    if(item.notAsist){
      return false;
    }
    if(item.asistentesConfirm>0){
      return true;
    }
    return false;
  }
}
