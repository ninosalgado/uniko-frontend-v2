import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeddingToolsComponent } from './wedding-tools.component';

describe('WeddingToolsComponent', () => {
  let component: WeddingToolsComponent;
  let fixture: ComponentFixture<WeddingToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeddingToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeddingToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
