import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { WeddingToolsComponent } from './wedding-tools.component';
import { GuestManagerComponent } from './guest-manager/guest-manager.component';
import { DomainManagerComponent } from './domain-manager/domain-manager.component';
import { WeddingToolsRoutingModule } from './wedding-tools-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    WeddingToolsComponent, 
    GuestManagerComponent, 
    DomainManagerComponent
  ],
  imports: [
    WeddingToolsRoutingModule,
    CommonModule,
    SharedModule,    
    FormsModule,
    ReactiveFormsModule
  ]
})
export class WeddingToolsModule { }
