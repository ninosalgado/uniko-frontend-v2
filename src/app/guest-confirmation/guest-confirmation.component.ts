import { Component, OnInit } from '@angular/core';
import { CoupleService } from '../services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-guest-confirmation',
  templateUrl: './guest-confirmation.component.html',
  styleUrls: ['./guest-confirmation.component.scss']
})
export class GuestConfirmationComponent implements OnInit {
  assistantId:string;
  assistantInfo:any;
  complete:boolean = false;
  options: any[] = [];
  constructor(
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder,
    private activateRoute: ActivatedRoute
  ) {
    activateRoute.params.subscribe(params => {
      this.assistantId= params['id'];
    });
   }

  async ngOnInit() {
    this.spinner.show();
    console.log(this.assistantId);
    await this.getAssistantInfo();
    console.log(this.assistantInfo);
    this.spinner.hide();
  }

  async getAssistantInfo(){
    try{
      this.assistantInfo = await this.coupleService.getAssistantInfo(this.assistantId);
      for(let i = 1; i<=this.assistantInfo.asistentes; i++){
        this.options.push(i);
      }
    }catch(e){      
      this.notification.error(e);
    }
  }

  async confirmAssistants(){
    try{
      this.spinner.show();
      const result = await this.coupleService.confirmAssistant(this.assistantId, this.assistantInfo.asistentesConfirm);
      console.log(result);
    }catch(e){      
      this.notification.error(e);
    }finally{
      this.complete = true;
      this.spinner.hide();
    }
  }
}
