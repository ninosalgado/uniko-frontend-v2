import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestConfirmationComponent } from './guest-confirmation.component';


const routes: Routes = [{
  path: '',
  component: GuestConfirmationComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestConfirmationRoutingModule { }