import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { Ng5SliderModule } from 'ng5-slider';
import { FormsModule } from '@angular/forms';

import { GuestConfirmationComponent } from './guest-confirmation.component';
import { GuestConfirmationRoutingModule } from './guest-confirmation-routing.module';

@NgModule({
  declarations: [
    GuestConfirmationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    Ng5SliderModule,
    GuestConfirmationRoutingModule
  ]
})
export class GuestConfirmationModule { }
