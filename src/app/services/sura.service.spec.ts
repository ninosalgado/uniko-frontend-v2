import { TestBed } from '@angular/core/testing';

import { SuraService } from './sura.service';

describe('SuraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuraService = TestBed.get(SuraService);
    expect(service).toBeTruthy();
  });
});
