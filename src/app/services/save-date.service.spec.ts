import { TestBed } from '@angular/core/testing';

import { SaveDateService } from './save-date.service';

describe('SaveDateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveDateService = TestBed.get(SaveDateService);
    expect(service).toBeTruthy();
  });
});
