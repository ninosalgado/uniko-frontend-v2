import { TestBed } from '@angular/core/testing';

import { CoupleService } from './couple.service';

describe('CoupleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoupleService = TestBed.get(CoupleService);
    expect(service).toBeTruthy();
  });
});
