import { TestBed } from '@angular/core/testing';

import { FrequentQuestionService } from './frequent-question.service';

describe('FrequentQuestionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FrequentQuestionService = TestBed.get(FrequentQuestionService);
    expect(service).toBeTruthy();
  });
});
