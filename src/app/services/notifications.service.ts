import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationsService {
  constructor(
    private toastyService: ToastrService
    ) { 
    }

    info(title: string, message?: string) {
      this.toastyService.info(title, message);
    }

    success(title: string, message?: string) {
      this.toastyService.success(title, message);
    }

    error(title: string, message?: string) {
      this.toastyService.error(title, message);
    }

    warning(title: string, message?: string) {
      this.toastyService.warning(title, message);
    }
}
