import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_RESOURCES } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SuraService {

  constructor(
    private http: HttpClient
  ) { }

  async plan(key: string): Promise<any> {
    return this.http.get(`${URL_RESOURCES.API_UNIKO}/plansSura/findKey/${key}`).toPromise();
  }

  async payment(payment: object): Promise<any> {
    return this.http.post(`${URL_RESOURCES.API_UNIKO}/plansSura/payment`,{
      payment
    }).toPromise();
  }

  async paypal(payment: object): Promise<any>  {
    return this.http.post(
      `${URL_RESOURCES.API_UNIKO}/paymentsSura/paypal`,
      payment
    ).toPromise();
  }

  async paypalPlus(payment: object): Promise<any>  {
    return this.http.post(
      `${URL_RESOURCES.API_UNIKO}/paymentsSura/paypalPlus`,
      payment
    ).toPromise();
  }

  async save(customer: object): Promise<any> {
    return this.http.post(`${URL_RESOURCES.API_UNIKO}/customersSura/add`,{
      customer
    }).toPromise();
  }
}
