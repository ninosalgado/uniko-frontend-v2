import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_RESOURCES } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CashoutService {

  constructor(
    private http: HttpClient
  ) { }

  async list(): Promise<any[]> {
    return this.http.get<any[]>(
      `${URL_RESOURCES.API_UNIKO}/cashouts/list`
    ).toPromise();
  }
}
