import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { $ } from 'protractor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent  {
  selectedItem:string;
  constructor(
    private router: Router
  ) {    
  }

  ngOnInit(){
    this.selectedItem= 'datoGen';
  }

  changeContent(event: Event) {
    switch(this.selectedItem) {
      case 'datoGen':
        this.router.navigate(['/','profile', 'datos-generales']);
      break;
      case 'regRecib':
        this.router.navigate(['/','profile', 'regalos-recibidos']);
      break;
      case 'retFondo':
        this.router.navigate(['/','profile', 'retiro-fondos']);
      break;
      case 'Recomendar':
        this.router.navigate(['/','profile', 'recomendar-amigo']);
      break;
      case 'Plan':
        this.router.navigate(['/','profile', 'planes']);
      break;
      case '':
      break;
    }
  }
}
