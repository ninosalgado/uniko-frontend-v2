import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { LayoutService } from 'src/app/services/layout.service';
import { formatDate } from '@angular/common';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { URL_RESOURCES } from 'src/environments/environment';

@Component({
  selector: 'app-datos-generales',
  templateUrl: './datos-generales.component.html',
  styleUrls: ['./datos-generales.component.scss']
})
export class DatosGeneralesComponent implements OnInit {
  isOpenShare: boolean = false;
  eventDate: Date;
  eventTime: any;
  url: string;
  link: string;
  isDomine: boolean = false;
  private geoCoder;
  gpsaddress: any = null;
  coupleInformation: ICoupleInformation;
  timeList: any[] = [];
  eventPlace: string;
  @ViewChild('search')
  public searchElementRef: ElementRef;


  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService
  ) {
    this.coupleInformation = this.layoutService.getCoupleInformation();
    this.gpsaddress = this.coupleInformation.weddingData.gpsaddress;
    this.eventPlace = this.coupleInformation.weddingData.gpsaddress ? this.coupleInformation.weddingData.gpsaddress.name : null;
    console.log(this.eventPlace);
    this.setUrl();
  }

  ngOnInit() {
    let value = this.coupleInformation.weddingData.address ? this.coupleInformation.weddingData.address : null;

    this.searchElementRef.nativeElement.value = value;
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation()
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        console.log("is changed");
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.gpsaddress = place;
          this.eventPlace = place.name;
        });
      });
    });
  }

  checkForNullity(address_result) {
    if (!address_result.target.value) {
      this.gpsaddress = null;
      this.eventPlace = null;
    }
  }

  isCouple() {
    switch (this.coupleInformation.weddingData.type) {
      case 'WEDDING':
        return true;
      case 'BABY':
        return false;
      default:
        return true;
    }
  }

  setUrl() {
    if (this.coupleInformation.domine) {
      this.url = this.coupleInformation.domine;
    } else {
      this.url = `${URL_RESOURCES.UNIKO}/${this.coupleInformation.url}`
      this.link = URL_RESOURCES.UNIKO;
    }
  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        //set position
      });
    }
  }

  getCity() {
    let city = this.gpsaddress.address_components.filter(items => items.types.includes('locality') || items.types.includes('sublocality')).map(function (obj) {
      return obj.long_name;
    });
    return city[0];
  }

  formatData() {
    //Asign to Couple
    if (this.gpsaddress && this.gpsaddress.formatted_address) {
      this.coupleInformation.weddingData.address = this.gpsaddress.formatted_address;
      this.coupleInformation.weddingData.city = this.getCity();
      this.coupleInformation.weddingData.gpsaddress = this.gpsaddress;
    } else {
      this.coupleInformation.weddingData.address = null;
      this.coupleInformation.weddingData.city = null;
      this.coupleInformation.weddingData.gpsaddress = null;
    }
    this.saveData();
  }

  async saveData() {
    this.spinner.show();
    let couple: ICoupleInformation;
    couple = await this.coupleService.updateRegister(this.coupleInformation);
    this.layoutService.setCoupleInformation(this.coupleInformation);
    console.log(couple);
    this.spinner.hide();
  }

  onCloseShare() {
    this.isOpenShare = false;
  }
}
