import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecomendarAmigoComponent } from './recomendar-amigo.component';

describe('RecomendarAmigoComponent', () => {
  let component: RecomendarAmigoComponent;
  let fixture: ComponentFixture<RecomendarAmigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecomendarAmigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecomendarAmigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
