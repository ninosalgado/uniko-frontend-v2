import { Component, OnInit, Inject } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CoupleInformation } from 'src/app/_models/coupleInformation';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { URL_RESOURCES } from 'src/environments/environment';
import Clipboard from './../../../assets/clipboard.js-2.0.4';

@Component({
  selector: 'app-recomendar-amigo',
  templateUrl: './recomendar-amigo.component.html',
  styleUrls: ['./recomendar-amigo.component.scss']
})
export class RecomendarAmigoComponent implements OnInit {
  couple: ICoupleInformation;
  isOpen: boolean = false;
  textlink: string = '';
  name: string = '';
  email: string = '';
  phone: string = '';
  referalInfo: any = {
    acumulado: 0,
    balance: 0,
    cashout: 0,
    contract: 0,
    list: [],
    total: 0
  };
  constructor( 
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification : ToastrService
  ) { 
    this.couple = this.layoutService.coupleInformation.info;
    this.textlink = `${URL_RESOURCES.UNIKO}/${this.layoutService.coupleInformation.info.url}/`;
    this.getReferals();
  }

  ngOnInit() {
    // try {
    //   this.textlink = this.textlink+this.customUrl;
    //   console.log(this.textlink);
    //   new Clipboard(".clipboard");
    // } catch(e) {

    // }
  }

  onClosedInfo(event?: Event) {
    if (event) {
      event.stopPropagation();
    }
    this.isOpen = false;
  }

  openShare() {
    this.isOpen = true;
    new Clipboard(".clipboard");
  }


  ordenPor(tipo:number){
    alert(tipo);
  }

  copy( ) {
    this.notification.success("¡Copiado!")
  }

  async save() {
    try {
      if (!this .name || !this.email || !this.phone) {
        this.notification.error("Todos los campos son obligatorios");
        return;
      }
      await this.coupleService.addReferal(
        this.name,
        this.email,
        this.phone
      );
      this.getReferals();
      this.notification.success("Solicitud enviada");
      this.isOpen = false;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async getReferals() {
    try {
      this.spinner.show();
      this.referalInfo = await this.coupleService.referals();
      this.textlink = `${URL_RESOURCES.UNIKO}/referido/${this.referalInfo.link}`;
      console.log(this.referalInfo);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  colapsa(tipo:any){
    let elemento = document.getElementById('collapse'+tipo);
    let toggleDat = elemento.dataset.toggle;
    if(toggleDat=="up"){
        elemento.setAttribute("data-toggle","down");
        elemento.style.display="none";
    }
    else if(toggleDat=="down"){
        elemento.setAttribute("data-toggle","up");
        elemento.style.display="";
    }
    
  }

}