import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DatosGeneralesComponent } from './datos-generales/datos-generales.component';
import { RegalosRecibidosComponent } from './regalos-recibidos/regalos-recibidos.component';
import { RetiroFondosComponent } from './retiro-fondos/retiro-fondos.component';
import { RecomendarAmigoComponent } from './recomendar-amigo/recomendar-amigo.component';
import { DialogEnviaAgradecimiento } from './regalos-recibidos/regalos-recibidos.component';
import { RetiraAcumuladoComponent } from './retira-acumulado/retira-acumulado.component';
import { SolicitarCreditoComponent } from './solicitar-credito/solicitar-credito.component';
import { RetiraCreditoComponent } from './retira-credito/retira-credito.component';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { PlansComponent } from './plans/plans.component';
import { PlanComponent } from './plan/plan.component';
import { MenuComponent } from './menu/menu.component';

@NgModule({
  declarations: [
    ProfileComponent,
    DatosGeneralesComponent,
    RegalosRecibidosComponent,
    RetiroFondosComponent,
    RecomendarAmigoComponent,
    DialogEnviaAgradecimiento,
    RetiraAcumuladoComponent,
    SolicitarCreditoComponent,
    RetiraCreditoComponent,
    PlansComponent,
    PlanComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ReactiveFormsModule,
    MatMenuModule,
    FormsModule,
    MaterialModule,
    SharedModule
  ],
  providers: [  
    MatDatepickerModule,  
  ],
  entryComponents:[
    DialogEnviaAgradecimiento
  ]
})
export class ProfileModule { }
