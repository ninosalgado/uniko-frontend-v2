import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './profile.component';
import { RetiraAcumuladoComponent } from './retira-acumulado/retira-acumulado.component';
import { SolicitarCreditoComponent } from './solicitar-credito/solicitar-credito.component';
import { RetiraCreditoComponent } from './retira-credito/retira-credito.component'
import { DatosGeneralesComponent } from './datos-generales/datos-generales.component';
import { RegalosRecibidosComponent } from './regalos-recibidos/regalos-recibidos.component';
import { RecomendarAmigoComponent } from './recomendar-amigo/recomendar-amigo.component';
import { PlansComponent } from './plans/plans.component';
import { RetiroFondosComponent } from './retiro-fondos/retiro-fondos.component';

const routes: Routes = [{
  path: '',
  component: ProfileComponent,
  children: [{
    path: 'datos-generales',
    component: DatosGeneralesComponent
  },{
    path: 'retiro-fondos',
    component: RetiroFondosComponent
  },{
    path: 'regalos-recibidos',
    component: RegalosRecibidosComponent
  }, {
    path: 'solicita-credito',
    component: SolicitarCreditoComponent
  },{
    path: 'planes',
    component: PlansComponent
  },{
    path: 'recomendar-amigo',
    component: RecomendarAmigoComponent
  },{
    path: 'retira-credito',
    component: RetiraCreditoComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
