import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-solicitar-credito',
  templateUrl: './solicitar-credito.component.html',
  styleUrls: ['./solicitar-credito.component.scss']
})
export class SolicitarCreditoComponent implements OnInit {
  @ViewChild('solicita1') solicita1: ElementRef;
  @ViewChild('solicita2') solicita2: ElementRef;
  @ViewChild('solicita3') solicita3: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  muestConten(elemMostrar){
    if(elemMostrar=="paso0"){
      this.solicita1.nativeElement.hidden = false;
      this.solicita2.nativeElement.hidden = true;
      this.solicita3.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso1"){
      this.solicita1.nativeElement.hidden = true;
      this.solicita2.nativeElement.hidden = false;
      this.solicita3.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso2"){
      this.solicita1.nativeElement.hidden = true;
      this.solicita2.nativeElement.hidden = true;
      this.solicita3.nativeElement.hidden = false;
    }
    else if(elemMostrar=="paso3"){
      alert("Ya terminó")
    }
  }

}
