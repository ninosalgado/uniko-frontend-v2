import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { formatDate } from '@angular/common';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-regalos-recibidos',
  templateUrl: './regalos-recibidos.component.html',
  styleUrls: ['./regalos-recibidos.component.scss']
})
export class RegalosRecibidosComponent implements OnInit {
  coupleInformation: ICoupleInformation;
  format = 'dd/MM/yyyy';
  locale = 'en-US';
  isOpen:boolean = false;
  selectedGift: any;
  currentMessage:string;
  currentOrder: number = 3;
  currentVisible: number = 0;
  constructor( 
    public dialog: MatDialog,
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService
  ) {
    this.coupleInformation = this.layoutService.getCoupleInformation();
   }

   async reloadCouple(){
    this.spinner.show();
    const coupleInformation = await this.coupleService.get(this.coupleInformation.id);
    this.coupleInformation = coupleInformation;
    this.layoutService.setCoupleInformation(this.coupleInformation);
    this.setPreviewOrder(1);
    this.spinner.hide();
  }

  ngOnInit() {   
    this.reloadCouple();
  }

  setPreviewOrder(type){
    let itemsSorted  = <any[]> JSON.parse(JSON.stringify(this.coupleInformation.giftList));
    try{
      switch(type){
        case 1:
          itemsSorted.sort(function(obj1, obj2) {
            // Ascending: date
            return obj1.date - obj2.date;
          });
          break;
        case 2:
          itemsSorted.sort(function(obj1, obj2) {
            // Descending: price
            return obj2.gift.price - obj1.gift.price;
          });
          break;
        case 3:
          itemsSorted.sort(function(obj1, obj2) {          
            // Original: index ( previewOrder )
            return obj1.givers[0].localeCompare(obj2.givers[0]);
            //return obj1.givers[0] - obj2.givers[0];
          });
          break;
          //array.sort((a,b) => a.title.rendered.localeCompare(b.title.rendered));
      } 
    }
    catch(e){
      console.log(e)
    }
    finally{
      this.currentOrder = type;
      this.coupleInformation.giftList = itemsSorted;
    }       
  }

  setVisible(gift){
    switch(this.currentVisible){
      case 2://checked
        return !gift.responded;
        break;
      case 1://unchecked
        return gift.responded;
        break;
      case 0: 
      default:
        return false;
    }
  }

  openModal(gift){
    this.selectedGift = gift; 
    this.isOpen=true;
  }
  closeModal(){
    this.isOpen=false;
  }

  save(gift){
    this.setGift(this.selectedGift,this.currentMessage);
  }

  colapsa(tipo:any){
    let elemento = document.getElementById('collapse'+tipo);
    let toggleDat = elemento.dataset.toggle;
    if(toggleDat=="up"){
        elemento.setAttribute("data-toggle","down");
        elemento.style.display="none";
    }
    else if(toggleDat=="down"){
        elemento.setAttribute("data-toggle","up");
        elemento.style.display="";
    }
    
  }

  orderBy(type: string){

  }

  showBy(type: string){

  }

  eventFormatDate(date): string{
    return formatDate(date,this.format,this.locale);
  }

  openDialog(gift): void {
    const dialogRef = this.dialog.open(DialogEnviaAgradecimiento, { data:{gift:gift},disableClose: true });
    
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.setGift(gift,result);
      }
      else{
        //document.getElementById('checkboxCambio').checked = false;
      }
    });
  }
  async setGift(gift,message){    
    try {
      this.spinner.show();
      const giftId = gift.id;
      let newGift = await this.coupleService.setThanksMessage(this.coupleInformation.id,giftId,message);
      gift.responded = true;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.notification.success("Mensaje enviado");
      this.spinner.hide();
      this.closeModal();
    }
  }
}


@Component({
  selector: 'envia-agradecimiento-dialog',
  templateUrl: './envia-agradecimiento-dialog.html',
  styleUrls: ['./regalos-recibidos.component.scss']
})
export class DialogEnviaAgradecimiento {
  message:string;
  constructor(public matEnviaAgradeciDia: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);
  }
  save() {
    this.matEnviaAgradeciDia.close({ message: this.message });
  }
  eventFormatDate(date): string{
    return formatDate(date,'dd/MM/yyyy','en-US');
  }
}
