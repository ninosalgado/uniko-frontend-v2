import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegalosRecibidosComponent } from './regalos-recibidos.component';

describe('RegalosRecibidosComponent', () => {
  let component: RegalosRecibidosComponent;
  let fixture: ComponentFixture<RegalosRecibidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegalosRecibidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegalosRecibidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
