import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-retira-credito',
  templateUrl: './retira-credito.component.html',
  styleUrls: ['./retira-credito.component.scss']
})
export class RetiraCreditoComponent implements OnInit {
  @ViewChild('retiraCred1') retiraCred1: ElementRef;
  @ViewChild('retiraCred2') retiraCred2: ElementRef;
  @ViewChild('retiraCred3') retiraCred3: ElementRef;
  cashoutForm: FormGroup;
  couple: ICoupleInformation;
  submit: boolean = false;
  referalInfo: any = {
    acumulado: 0,
    balance: 0,
    cashout: 0,
    contract: 0,
    list: [],
    total: 0
  };
  retiro = 0;
  name = '';
  bank = '';
  numberBank = '';
  constructor(
    private coupleService: CoupleService,
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification : ToastrService,
    private formBuilder: FormBuilder,
  ) { 
    this.couple = this.layoutService.coupleInformation.info;
    this.getReferals();
  }

  ngOnInit() {
    this.cashoutForm = this.formBuilder.group({
      name: [
        `${this.couple.weddingData.nameP1} ${this.couple.weddingData.lastnameP1}`,
        Validators.required
      ],
      bank: ['', Validators.required],
      numberBank: ['', Validators.required],
      amount: [0, Validators.required]
    })
  }

  async getReferals() {
    try {
      this.spinner.show();
      this.referalInfo = await this.coupleService.referals();
      console.log(this.referalInfo);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  muestConten(elemMostrar) {
    if(elemMostrar=="paso0"){
      this.retiraCred1.nativeElement.hidden = false;
      this.retiraCred2.nativeElement.hidden = true;
      this.retiraCred3.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso1") {
      this.submit = true;
      if (!this.cashoutForm.value.amount) {
        this.cashoutForm.setErrors({
          amount: 'El valor no puede ser 0'
        });
        this.notification.error('El valor no puede ser 0');
      }
      if (this.cashoutForm.value.amount > this.referalInfo.balance) {
        this.cashoutForm.setErrors({
          amount: 'El valor no puede ser mayor al existente'
        });
        this.notification.error('El valor no puede ser mayor a ' + this.referalInfo.balance);
      }
      if (this.cashoutForm.invalid) {
        return;
      }
      this.retiro = this.cashoutForm.value.amount;
      this.name = this.cashoutForm.value.name;
      this.numberBank = this.cashoutForm.value.numberBank;
      this.bank = this.cashoutForm.value.bank;
      this.retiraCred1.nativeElement.hidden = true;
      this.retiraCred2.nativeElement.hidden = false;
      this.retiraCred3.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso2") {
      // this.retiraCred1.nativeElement.hidden = true;
      // this.retiraCred2.nativeElement.hidden = true;
      // this.retiraCred3.nativeElement.hidden = false;
      this.generate();
    }
    else if(elemMostrar=="paso3"){
      alert("Ya terminó")
    }
  }

  async generate() {
    try {
      this.spinner.show();
      await this.coupleService.referalsCashout(
        this.name,
        this.bank,
        this.numberBank,
        this.retiro
      )
      this.retiraCred1.nativeElement.hidden = true;
      this.retiraCred2.nativeElement.hidden = true;
      this.retiraCred3.nativeElement.hidden = false;
    } catch (err) {
      this.notification.error(err);
    } finally {
      this.spinner.hide();
    }
  }

}
