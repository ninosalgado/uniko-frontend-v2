import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetiraCreditoComponent } from './retira-credito.component';

describe('RetiraCreditoComponent', () => {
  let component: RetiraCreditoComponent;
  let fixture: ComponentFixture<RetiraCreditoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetiraCreditoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetiraCreditoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
