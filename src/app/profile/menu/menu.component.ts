import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  selectedItem: string;
  coupleInformation: ICoupleInformation;
  constructor(
    private router: Router,
    private layoutService: LayoutService,
  ) {
    this.coupleInformation = this.layoutService.getCoupleInformation();
  }

  ngOnInit() {
    this.selectedItem = 'datoGen';
  }

  isCouple() {
    switch (this.coupleInformation.weddingData.type) {
      case 'WEDDING':
        return true;
      case 'BABY':
        return false;
      default:
        return true;
    }
  }

  changeContent(event: Event) {
    switch (this.selectedItem) {
      case 'datoGen':
        this.router.navigate(['/', 'profile', 'datos-generales']);
        break;
      case 'regRecib':
        this.router.navigate(['/', 'profile', 'regalos-recibidos']);
        break;
      case 'retFondo':
        this.router.navigate(['/', 'profile', 'retiro-fondos']);
        break;
      case 'Recomendar':
        this.router.navigate(['/', 'profile', 'recomendar-amigo']);
        break;
      case 'Plan':
        this.router.navigate(['/', 'profile', 'planes']);
        break;
      case '':
        break;
    }
  }
}
