import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetiraAcumuladoComponent } from './retira-acumulado.component';

describe('RetiraAcumuladoComponent', () => {
  let component: RetiraAcumuladoComponent;
  let fixture: ComponentFixture<RetiraAcumuladoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetiraAcumuladoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetiraAcumuladoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
