import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-retira-acumulado',
  templateUrl: './retira-acumulado.component.html',
  styleUrls: ['./retira-acumulado.component.scss']
})
export class RetiraAcumuladoComponent implements OnInit {
  @ViewChild('retira1') retira1: ElementRef;
  @ViewChild('retira2') retira2: ElementRef;
  @ViewChild('retira3') retira3: ElementRef;
  constructor() { }

  ngOnInit() {
  }

  muestConten(elemMostrar){
    if(elemMostrar=="paso0"){
      this.retira1.nativeElement.hidden = false;
      this.retira2.nativeElement.hidden = true;
      this.retira3.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso1"){
      this.retira1.nativeElement.hidden = true;
      this.retira2.nativeElement.hidden = false;
      this.retira3.nativeElement.hidden = true;
    }
    else if(elemMostrar=="paso2"){
      this.retira1.nativeElement.hidden = true;
      this.retira2.nativeElement.hidden = true;
      this.retira3.nativeElement.hidden = false;
    }
    else if(elemMostrar=="paso3"){
      alert("Ya terminó")
    }
  }
  }
