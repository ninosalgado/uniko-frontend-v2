import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { CoupleService } from 'src/app/services/couple.service';
import { LayoutService } from 'src/app/services/layout.service';
import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {
  plans: any[] = [];
  plan: any;
  isPaid: boolean = false;
  constructor(
    private notificationService: ToastrService,
    private spinner: NgxSpinnerService,
    private catalogsService: CatalogsService ,
    private coupleService: CoupleService,
    private layoutService: LayoutService
  ) {
    this.isPaid = this.layoutService.getCoupleInformation().payInformationData['isPaid'];
    this.getPlans();
  }

  ngOnInit() {
  }

  get price() {
    const plan = this.plans.find(data => data.isPaid);
    if (plan) {
      return plan.amount;
    }
    return 0;
  }

  orderPlans(){
    let itemsSorted  = <any[]> JSON.parse(JSON.stringify(this.plans));
    try{
      itemsSorted.sort(function(obj1, obj2) {
        // Ascending: ammount
        return obj1.amount - obj2.amount;
      });
    }
    catch(e){
      console.log(e)
    }
    finally{
      this.plans = itemsSorted;
    }
  }

  async getPlans() {
    try {
      this.spinner.show();
      const plans =  await this.catalogsService.plansType();
      const plan = await this.coupleService.getPlanPermisions(this.layoutService.coupleInformation.info.id);
      this.plan = await this.coupleService.getPlan(this.layoutService.getCoupleInformation().id);
      this.plans = [];
      plans.forEach(data => {
        if (data.id == this.plan.planId) {
          data['isPaid'] = true;
        } else {
          data['isPaid'] = false;
        }
        if (plan.isBack) {
          if (data.isBack) {
            this.plans.push(data);
          }
        } else if (!plan.isSuscription && data.isPaid) {
          this.plans.push(data);
        } else {
          if (!data.isBack && (plan.isSuscription || plan.isBasic)) {
            this.plans.push(data);
          }
        }
      });
      this.orderPlans();
      if(!this.plans[this.plans.length-1].isPaid){
        this.plans[this.plans.length-1].bestValue = true;
      }
    } catch (e) {
      this.notificationService.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
