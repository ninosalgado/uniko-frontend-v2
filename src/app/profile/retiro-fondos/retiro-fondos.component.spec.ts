import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetiroFondosComponent } from './retiro-fondos.component';

describe('RetiroFondosComponent', () => {
  let component: RetiroFondosComponent;
  let fixture: ComponentFixture<RetiroFondosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetiroFondosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetiroFondosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
