import { Component, OnInit } from '@angular/core';
import { CashoutService } from 'src/app/services/cashout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { LayoutService } from 'src/app/services/layout.service';
import { CoupleService } from 'src/app/services/couple.service';

@Component({
  selector: 'app-retiro-fondos',
  templateUrl: './retiro-fondos.component.html',
  styleUrls: ['./retiro-fondos.component.scss']
})
export class RetiroFondosComponent implements OnInit {
  cashouts: any[] = [];
  totals = {
    giftTotal: 0,
    cashoutTotal: 0,
    total: 0
  }
  currentOrder: number = 3;
  previousOrder:number;
  orders: any = [
    {order:1,reverse:false},
    {order:2,reverse:false},
    {order:3,reverse:false},
    {order:4,reverse:false},
    {order:5,reverse:false},
  ]
  constructor(
    private cashoutService: CashoutService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private layoutService: LayoutService,
    private coupleService: CoupleService
  ) { 
    this.getList();
  }

  ngOnInit() {
  }

  getStatus(cashout){
    if(cashout.isCancelled){
      return 'Cancelado'
    }

    return cashout.isPaid ? 'Pagado' : 'Pendiente';
  }

  setPreviewOrder(order){
    let itemsSorted  = <any[]> JSON.parse(JSON.stringify(this.cashouts));         
    let reversed;
    try{
      switch(order){
        case 1:
          reversed = this.orders.find(data=>{
            return data.order == 1;
          });        

          itemsSorted.sort(function(obj1, obj2) {
            const time1 = new Date(obj1.date);
            const time2 = new Date(obj2.date);

            if(reversed.reverse == true){
              return <any>time1 - <any>time2;
            }else{
              return <any>time2 - <any>time1;
            }        
          });
          reversed.reverse = !reversed.reverse;
          break;
        case 2:
          reversed = this.orders.find(data=>{
            return data.order == 2;
          });
          itemsSorted.sort(function(obj1, obj2) {
            if(reversed.reverse == true){
              return obj1.requiredAmount - obj2.requiredAmount;
            }
            return obj2.requiredAmount - obj1.requiredAmount;
          });

          reversed.reverse = !reversed.reverse;
          break;
        case 3:
          reversed = this.orders.find(data=>{
            return data.order == 3;
          });

          itemsSorted.sort(function(obj1, obj2) {   
            if(reversed.reverse == true){
              return obj2.target.localeCompare(obj1.target);
            }   
            return obj1.target.localeCompare(obj2.target);
          });

          reversed.reverse = !reversed.reverse;
          break;
        case 4:
          reversed = this.orders.find(data=>{
            return data.order == 4;
          });

          itemsSorted.sort(function(obj1, obj2) {          
            if(reversed.reverse == true){
              return obj2.bank.localeCompare(obj1.bank);
            }
            return obj1.bank.localeCompare(obj2.bank);
          });

          reversed.reverse = !reversed.reverse;
          break;  
        case 5:
          reversed = this.orders.find(data=>{
            return data.order == 5;
          });

          itemsSorted.sort(function(obj1, obj2) {          
            let statusObj1 = obj1.isCancelled?2:1;
            statusObj1 = obj1.isPaid?0:statusObj1;

            let statusObj2 = obj2.isCancelled?2:1;
            statusObj2 = obj2.isPaid?0:statusObj2;
            if(reversed.reverse == true){
              return statusObj2 - statusObj1;
            }
            return statusObj1 - statusObj2;
          });

          reversed.reverse = !reversed.reverse;
          break;  
      } 
    }
    catch(e){
      console.log(e)
    }
    finally{
      this.currentOrder = order;
      this.cashouts = itemsSorted;
    }       
  }

  async getList() {
    try {
      this.spinner.show();
      this.cashouts = await this.cashoutService.list();
      const couple = await this.coupleService.get(this.layoutService.getCoupleInformation().id);
      const totals = await this.coupleService.getCashoutTotal();
      couple.cashOutInformationList = this.cashouts;
      this.layoutService.coupleInformation.info = couple;
      this.totals = this.layoutService.coupleInformation.totals;
      this.totals.total = totals.total;
    } catch(e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  ordenPor(tipo:number){
    alert(tipo);
  }

  colapsa(tipo:any){
    let elemento = document.getElementById('collapse'+tipo);
    let toggleDat = elemento.dataset.toggle;
    if(toggleDat=="up"){
        elemento.setAttribute("data-toggle","down");
        elemento.style.display="none";
    }
    else if(toggleDat=="down"){
        elemento.setAttribute("data-toggle","up");
        elemento.style.display="";
    }
    
  }
  
}
