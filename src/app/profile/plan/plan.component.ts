import { Component, OnInit, Input } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {
  @Input() plan: any;
  @Input() isPaid: boolean = false;
  constructor(
    private layoutService: LayoutService
  ) { 
    console.log(this.plan);
  }

  ngOnInit() {
    console.log(this.plan);

  }

}
