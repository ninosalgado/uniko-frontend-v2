import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { KEYS } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Token } from '@angular/compiler/src/ml_parser/lexer';
import { SuraService } from 'src/app/services/sura.service';
import { LayoutService } from 'src/app/services/layout.service';
declare var Conekta: any;

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  planForm: FormGroup;
  days: string = '15 días';
  years: number[] = [];
  months: string[] = [];
  price: number = 199;
  type: any = {
    type: '',
    card: 16,
    cvc: 3
  }
  types: any = [{
    type: 'visa',
    card: 16,
    cvc: 3
  }, {
    type: 'mastercard',
    card: 16,
    cvc: 3
  }, {
    type: 'amex',
    card: 15,
    cvc: 4
  }]
  constructor(
    private suraService: SuraService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private layoutService: LayoutService
  ) {
    const year = new Date().getFullYear();
    this.years = Array.from({ length: 11 }, (v, i) => year + i)
    this.months = Array.from({ length: 12 }, (v, i) => String(i + 1));
    Conekta.setPublicKey(KEYS.CONEKTA_GUEST);
  }

  ngOnInit() {
    this.planForm = this.formBuilder.group({
      name: ['', Validators.required],
      numberCard: ['', Validators.required],
      cvc: ['', Validators.required],
      year: [new Date().getFullYear(), Validators.required],
      month: [String(new Date().getMonth() + 1), Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required]
    })
  }

  async createToken(card): Promise<any> {
    return new Promise((resolve, reject) => {
      Conekta.token.create(card, (token: Token) => {
        return resolve(token);
      }, (error) => {
        reject(error.message_to_purchaser);
      })
    });
  }

  validCard() {
    const first = this.planForm.value.numberCard.slice(0, 1);
    switch (first) {
      case '4':
        if (this.type.type !== 'visa') {
          this.type = this.types.find(data => data.type == 'visa');
        }
        break;
      case '3':
        if (this.type.type !== 'amex') {
          this.type = this.types.find(data => data.type == 'amex');
        }
        break;
      case '5':
        if (this.type.type !== 'mastercard') {
          this.type = this.types.find(data => data.type == 'mastercard');
        }
        break;
      default:
    }
  }


  async payment() {
    try {
      if (this.planForm.invalid) {
        return;
      }
      this.spinner.show();
      const token = {
        card: {
          number: this.planForm.value.numberCard,
          name: this.planForm.value.name,
          exp_year: this.planForm.value.year,
          exp_month: this.planForm.value.month,
          cvc: this.planForm.value.cvc
        }
      }
      const response = await this.createToken(token);
      const payment = {
        info: {
          planId: this.route.parent.snapshot.params.planId,
          method: 'card',
          token: response.id,
          payer: {
            phone: this.planForm.value.phone,
            email: this.planForm.value.email,
            name: this.planForm.value.name
          }
        }
      }
      const responsePayment = await this.suraService.payment(payment);
      this.layoutService.sura.email = this.planForm.value.email;
      this.layoutService.sura.paid = true;
      this.layoutService.sura.customerId = responsePayment.customer.id;
      this.layoutService.sura.ab = this.route.parent.snapshot.params.planId;
      this.router.navigate(['/sura/congrats']);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }


}
