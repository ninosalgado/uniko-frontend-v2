import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { SuraRoutingModule } from './sura-routing';
import { HeaderLogoComponent } from './header-logo/header-logo.component';
import { MenuComponent } from './menu/menu.component';
import { HeaderPaymentComponent } from './header-payment/header-payment.component';
import { PaymentComponent } from './payment/payment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CongratsComponent } from './congrats/congrats.component';
import { CardComponent } from './card/card.component';
import { MaterialModule } from '../material.module';
import { MatDatepickerModule } from '@angular/material';
import { FinishComponent } from './finish/finish.component';
import { OxxoComponent } from './oxxo/oxxo.component';
import { SpeiComponent } from './spei/spei.component';
import { PaypalComponent } from './paypal/paypal.component';
import { PaypalPlusComponent } from './paypal-plus/paypal-plus.component';
import { ConfirmPaypalComponent } from './confirm-paypal/confirm-paypal.component';
import { ConfirmComponent } from './confirm/confirm.component';

@NgModule({
  declarations: [
    HomeComponent, 
    HeaderLogoComponent, 
    MenuComponent, 
    HeaderPaymentComponent, 
    PaymentComponent, 
    CongratsComponent,
    CardComponent,
    FinishComponent,
    OxxoComponent,
    SpeiComponent,
    PaypalComponent,
    PaypalPlusComponent,
    ConfirmPaypalComponent,
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    SuraRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [
    MatDatepickerModule
  ]
})
export class SuraModule { }
