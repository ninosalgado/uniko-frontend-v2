import { Component, OnInit } from '@angular/core';
import { SuraService } from 'src/app/services/sura.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.scss']
})
export class PaypalComponent implements OnInit {
  planForm: FormGroup;
  pending: boolean = false;
  charge: any;
  constructor(
    private suraService: SuraService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private layoutService: LayoutService
  ) { }

  ngOnInit() {
  }

  async payment() {
    try {
      this.spinner.show();
      const payment = {
        info: {
          planId: this.route.parent.snapshot.params.planId,
          method: 'paypal',
          payer: {
            
          }
        }
      }
      console.log('hjsajhsjhahjashjahjs');
      const response = await this.suraService.paypal(payment);
      location.href = response.url;
    } catch(e) {
      this.spinner.hide();
      console.log(e);
      throw e;
    }
  }

}
