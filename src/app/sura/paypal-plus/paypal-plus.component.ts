import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { KEYS } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { SuraService } from 'src/app/services/sura.service';
declare var PAYPAL: any;

@Component({
  selector: 'app-paypal-plus',
  templateUrl: './paypal-plus.component.html',
  styleUrls: ['./paypal-plus.component.scss']
})
export class PaypalPlusComponent implements OnInit {
  paypalId: any;
  paypalPlus: any;
  constructor(
    private layoutService: LayoutService,
    public spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute,
    private suraService: SuraService
  ) { 
    this.generate();
  }

  ngOnInit() {
  }

  load(info: any) {
    this.paypalId = info.paypal.id;
      this.paypalPlus =  PAYPAL.apps.PPP({
        approvalUrl: info.paypal.url.href,
        buttonLocation: "outside",
        preselection: "none",
        surcharging: false,
        hideAmount: false,
        placeholder: "paypal-plus",
        disableContinue: "continueButton",
        enableContinue: "continueButton",
        onContinue: (rememberedCards, PayerID, token, term) => {
          console.log("start .......................");
          this.spinner.hide();
          this.router.navigate([
            '/',
          this.layoutService.coupleInformation.info.url,
          'checkout',
          'payment',
          'confirm',
          this.layoutService.order.get.id
          ], {queryParams: {
            token,
            PayerID,
            paymentId: this.paypalId
          }});
        },
        onError:  (err) => {
          console.log(err);
          this.spinner.hide();
        },
        onLoad: (err) => {
          console.log(err);
        },
        language: "es_MX",
        country: "MX",
        disallowRememberedCards: true,
        rememberedCards: true,
        mode: KEYS.paypalLive,
        useraction: "continue",
        payerEmail: 'info@uniko.co',
        payerPhone: '5555555555',
        payerFirstName: 'sura',
        payerLastName: 'uniko',
        payerTaxId: "",
        payerTaxIdType: "",
        merchantInstallmentSelection: 1,
        merchantInstallmentSelectionOptional: 1,
        hideMxDebitCards: true,
        iframeHeight: 460
      });
  }

  async generate() {
    try {
      this.spinner.show();
      const payment = {
        info: {
          planId: this.route.parent.snapshot.params.planId,
          method: 'paypal',
          payer: {
            
          }
        }
      }
      console.log('hjsajhsjhahjashjahjs');
      const response = await this.suraService.paypalPlus(payment);
      console.log(response);
      this.load({paypal: response.url});
    } catch(e) {
      throw e;
    } finally {
      this.spinner.hide();
    }
  }

}
