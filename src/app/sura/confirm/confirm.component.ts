import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SuraService } from 'src/app/services/sura.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  plan: any;
  constructor(
    private layoutService: LayoutService,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private suraService: SuraService,
    private router: Router
  ) { 
    this.sura();
  }

  ngOnInit() {
  }

  go() {
    this.router.navigate(['sura/congrats'])
  }

  async sura() {
    try {
      this.spinner.show();
      this.plan = await this.suraService.plan(this.layoutService.sura.ab);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
