import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PaymentComponent } from './payment/payment.component'
import { CongratsComponent } from './congrats/congrats.component';
import { CardComponent } from './card/card.component';
import { FinishComponent } from './finish/finish.component';
import { OxxoComponent } from './oxxo/oxxo.component';
import { SpeiComponent } from './spei/spei.component';
import { PaypalComponent } from './paypal/paypal.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { PaypalPlusComponent } from './paypal-plus/paypal-plus.component';

const routes: Routes = [{
    path: 'payment/:planId',
    component: PaymentComponent,
    children: [{
        path: '',
        component: CardComponent
    }, {
        path: 'oxxo',
        component: OxxoComponent
    }, {
        path: 'paypal',
        component: PaypalComponent
    }, {
        path: 'paypal-plus',
        component: PaypalPlusComponent
    }, {
        path: 'spei',
        component: SpeiComponent
    }, {
        path: 'card',
        component: CardComponent
    }]
},{
    path: 'confirm',
    component: ConfirmComponent
},{
    path: 'congrats',
    component: CongratsComponent
}, {
    path: 'congrats-info',
    component: FinishComponent
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SuraRoutingModule {}
