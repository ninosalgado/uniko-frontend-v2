import { Component, OnInit } from '@angular/core';
import { SuraService } from 'src/app/services/sura.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-spei',
  templateUrl: './spei.component.html',
  styleUrls: ['./spei.component.scss']
})
export class SpeiComponent implements OnInit {
  planForm: FormGroup;
  pending: boolean = false;
  charge: any;
  constructor(
    private suraService: SuraService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private layoutService: LayoutService
  ) { }

  ngOnInit() {
    this.planForm = this.formBuilder.group({
      phone: ['', Validators.required],
      email: ['', Validators.required],
      name: ['', Validators.required]
    });
    console.log(this.route.parent.snapshot.params.planId);
  }

  async payment() {
    try {
      if (this.planForm.invalid) {
        return ;
      }
      this.spinner.show();
      const payment = {
        info: {
          planId: this.route.parent.snapshot.params.planId,
          method: 'spei',
          payer: {
            phone: this.planForm.value.phone,
            email: this.planForm.value.email,
            name: this.planForm.value.name
          }
        }
      }
      const response = await this.suraService.payment(payment);
      this.charge = response.charge.charges.data[0];
      this.pending = true;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }
}
