import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SuraService } from 'src/app/services/sura.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Route, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  plan: any;
  totalSteps = 3;
  step = 1;
  planId: string;
  textPlan: string = 'Seguro accidentes luna de miel';
  price: number = 0;
  type: string = 'card';
  methods: string[] = ['card', 'paypal', 'paypal-plus', 'oxxo', 'spei']
  constructor(
    private suraService: SuraService,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router
  ) { 
    this.planId = this.route.snapshot.params.planId;
    this.getPlan();
  }

  ngOnInit() {
    console.log(this.route);
  }

  getWidth(){
    return (100/this.totalSteps)+'%';
  }

  getClass(index){
    const _index = +index + 1;
    const _step = +this.step;
    if(_step == _index){
      return 'active';
    }else
    if(_step > _index){
      return 'done';
    }else
    {
      return 'active2';
    }
  }

  changeMethod(type) {
    console.log(type);
    switch(type) {
      case 'card':
        this.router.navigate([
          `/sura/payment/${this.route.snapshot.params.planId}/`
        ]);
      break;
      case 'oxxo':
        this.router.navigate([
          `/sura/payment/${this.route.snapshot.params.planId}/oxxo`
        ]);
      break;
      case 'spei':
        this.router.navigate([
          `/sura/payment/${this.route.snapshot.params.planId}/spei`
        ]);
      break;
      case 'paypal':
        this.router.navigate([
          `/sura/payment/${this.route.snapshot.params.planId}/paypal`
        ]);
      break;
      case 'paypal-plus':
        this.router.navigate([
          `/sura/payment/${this.route.snapshot.params.planId}/paypal-plus`
        ]);
      break;
    }
  }

  async getPlan() {
    try {
      this.spinner.show();
      this.plan = await this.suraService.plan(this.planId);
      this.textPlan = this.plan.name;
      this.price = this.plan.price;
    } catch(e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
