import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { SuraService } from 'src/app/services/sura.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {
  plan: any;
  constructor(
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private suraService: SuraService,
    private layoutService: LayoutService
  ) { }

  ngOnInit() {
    this.getPlan();
  }

  async getPlan() {
    try {
      this.spinner.show();
      this.plan = await this.suraService.plan(this.layoutService.sura.ab);
    } catch(e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
