import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { SuraService } from 'src/app/services/sura.service';
import { Router } from '@angular/router';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-congrats',
  templateUrl: './congrats.component.html',
  styleUrls: ['./congrats.component.scss']
})
export class CongratsComponent implements OnInit {
  registerForm: FormGroup;
  plan: any;
  planId: string;
  constructor(
    private ngZone: NgZone,
    private formBuilder: FormBuilder,
    private notification: ToastrService,
    private spinner: NgxSpinnerService,
    private suraService: SuraService,
    private router: Router,
    private layoutService: LayoutService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      rfc: ['', Validators.required],
      address: ['', Validators.required],
      name: ['', Validators.required],
      name2: ['', Validators.required],
      name3: ['', Validators.required],
      lastName: ['', Validators.required],
      lastName2: ['', Validators.required],
      secondLastName: ['', Validators.required],
      secondLastName2: ['', Validators.required],
      age: ['', Validators.required],
      age2: ['', Validators.required],
      gender: ['M', Validators.required],
      gender2: ['M', Validators.required]
    });
    this.getPlan();
  }

  formGenerate() {
    this.registerForm.reset();
    if (!this.plan.one) {
      this.registerForm = this.formBuilder.group({
        rfc: ['', Validators.required],
        address: ['', Validators.required],
        name: ['', Validators.required],
        name2: ['', Validators.required],
        name3: ['', Validators.required],
        lastName: ['', Validators.required],
        lastName2: ['', Validators.required],
        secondLastName: ['', Validators.required],
        secondLastName2: ['', Validators.required],
        age: ['', Validators.required],
        age2: ['', Validators.required],
        gender: ['M', Validators.required],
        gender2: ['M', Validators.required]
      });
    } else {
      this.registerForm = this.formBuilder.group({
        rfc: ['', Validators.required],
        address: ['', Validators.required],
        name: ['', Validators.required],
        name2: ['', Validators.required],
        lastName: ['', Validators.required],
        secondLastName: ['', Validators.required],
        age: ['', Validators.required],
        gender: ['M', Validators.required],
      });
    }
  }

  async getPlan() {
    try {
      this.spinner.show();
      console.log(this.layoutService.sura);
      this.plan = await this.suraService.plan(
        this.layoutService.sura.ab
      );
      this.formGenerate();
    } catch(e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async save() {
    try {
      
      if (this.registerForm.invalid) {
        console.log(this.registerForm);
        return ;
      } 
      this.spinner.show();
      const customer: any = {
        customerPaymentId: this.layoutService.sura.customerId,
        planId: this.layoutService.sura.ab,
        contractor: {
          name: this.registerForm.value.name,
          rfc: this.registerForm.value.rfc,
          address: this.registerForm.value.address
        },
        assured: {
          name:this.registerForm.value.name,
          lastName: this.registerForm.value.lastName,
          secondLastName: this.registerForm.value.secondLastName,
          age: this.registerForm.value.age,
          gender: this.registerForm.value.gender
        },
      }
      if (!this.plan.one) {
        customer.assured2 = {
          name:this.registerForm.value.name2,
          lastName: this.registerForm.value.lastName2,
          secondLastName: this.registerForm.value.secondLastName2,
          age: this.registerForm.value.age2,
          gender: this.registerForm.value.gender2
        }
      }
      const registerResponse = await this.suraService.save(customer);
      this.notification.success("Proceso finalizado");
      this.router.navigate(['/sura/congrats-info']);
    } catch(e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
