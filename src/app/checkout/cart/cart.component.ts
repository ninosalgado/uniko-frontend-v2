import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { Cart } from 'src/app/_models/cart';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { Router } from '@angular/router';
import { timingSafeEqual } from 'crypto';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cart: Cart;
  couple: ICoupleInformation;
  constructor(
    private layoutService: LayoutService,
    private router: Router
  ) { 
    this.cart = this.layoutService.cart;
    this.couple = this.layoutService.getCoupleInformation();
    this.cart.cartEmitter.subscribe(data=> {
      this.cart = this.layoutService.cart;
    });
  }

  ngOnInit() {
    if (!this.cart.cart.products.length){
      this.router.navigate([this.couple.url,'gift-table']);
    }
  }

  get currency() {
    return this.layoutService.settings.info.currency;
  }

  formatPrice(price) {
    return price / this.layoutService.settings.info.currencyChange;
  }

  get getArtNum() {
    return this.cart.cart.products.length;
  }

}
