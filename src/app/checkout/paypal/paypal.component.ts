import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { Route } from '@angular/compiler/src/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { IOrder, IPaymentInfo } from 'src/app/_interface/order';

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.scss']
})
export class PaypalComponent implements OnInit {
  couple: ICoupleInformation;
  commision: any;
  constructor(
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private orderService: OrderService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.couple = this.layoutService.getCoupleInformation();
    this.getCommision();
  }

  ngOnInit() {
  }

  async getCommision() {
    try {
      this.spinner.show();
      this.commision = await this.orderService.calculateComission({
        cardNumber: 'paypal',
        value: this.layoutService.cart.total
      });
      this.layoutService.cart.setCommisions(this.commision.comission, 0);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async save () {
    try {
      this.spinner.show();
      const payment: IPaymentInfo = {
        address: {},
        payer: {
        },
        otro: {
          pay_type: 'paypal'
        },
        first6: '',
        pay: {
          name: '',
          month: null,
          year: null
        },
        products: this.layoutService.cart.productIds,
        birthdate: {},
        method: 'paypal',
        message: {
          givers: this.layoutService.order.get.paymentData.message.givers,
          emails: this.layoutService.order.get.paymentData.message.emails,
          prefijo: this.layoutService.order.get.paymentData.message.prefijo,
          message: this.layoutService.order.get.paymentData.message.message,
          tel: this.layoutService.order.get.paymentData.message.tel,
          names: this.layoutService.order.get.paymentData.message.names
        },
        urlRegistry: this.couple.url
      }
      const order: any = await this.orderService.makePaypalPayment(
        this.layoutService.order.get.id,
        payment
      );
      console.log(order);
      location.href = order.url;
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }
}
