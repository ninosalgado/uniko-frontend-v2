import { Component, OnInit, OnDestroy } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { Cart } from 'src/app/_models/cart';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { IOrder } from 'src/app/_interface/order';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-congrats',
  templateUrl: './congrats.component.html',
  styleUrls: ['./congrats.component.scss']
})
export class CongratsComponent implements OnInit, OnDestroy {
  couple: ICoupleInformation;
  order: IOrder;
  url: string = '';
  bonnusUrl: any;
  isExistBonnus: boolean = false;
  constructor(
    private layoutService: LayoutService,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private sanitizer: DomSanitizer
  ) { 
    this.couple = this.layoutService.coupleInformation.info;
    this.order = this.layoutService.order.get;
    this.isExistBonnus = this.order.bonnus && this.order.bonnus.url ? true : false;
    if (this.isExistBonnus) {
      this.bonnusUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.order.bonnus.url);
    }
    
    
    this.url = `${location.host}/${this.couple.url}/gift-table`
    this.getInfo();
  }

  ngOnInit() {
   
  }

  ngOnDestroy() {
    this.layoutService.cart.clear();
    this.layoutService.order.clear();
    localStorage.clear();
    this.layoutService.clear();
  }

  async getInfo() {
    try {
      this.spinner.show();
      const couple: ICoupleInformation = await this.coupleService.findOneRestrict(this.couple.url);
      this.layoutService.coupleInformation.info = couple;
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
