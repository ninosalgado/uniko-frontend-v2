import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { Cart } from 'src/app/_models/cart';
import { Router } from '@angular/router';

@Component({
  selector: 'app-methods',
  templateUrl: './methods.component.html',
  styleUrls: ['./methods.component.scss']
})
export class MethodsComponent implements OnInit {
  cart: Cart;
  commission: number = 0;
  commissionMsi: number = 0;
  totalComission: number = 0;
  subtotal: number = 0;
  isDisabled: boolean = false;
  type: string;
  methods: string[] = ['card', 'paypal', 'paypal-plus', 'oxxo', 'spei', 'rappi']
  infoMessageLang = 'ESP';
  constructor(
    private layoutService: LayoutService,
    private roter: Router
  ) {
    this.cart = this.layoutService.cart;
    const paymentComissions = this.layoutService.coupleInformation.info.paymentCommision;
    this.cart.cartEmitter.subscribe(data => {
      this.totalComission = this.cart.totalComission ? this.cart.totalComission : 0;
      this.subtotal = this.cart.total - (paymentComissions ? this.totalComission : 0);
    });
    this.type = this.cart.type;
    this.totalComission = this.cart.totalComission ? this.cart.totalComission : 0;
    this.subtotal = this.cart.total - (paymentComissions ? this.totalComission : 0);
    this.isDisabled = paymentComissions ? true : false;
  }

  ngOnInit() {
    console.log(this.cart);
  }

  setMessage(lang) {
    this.infoMessageLang = lang;
  }

  formatPrice(price) {
    return price / this.layoutService.settings.info.currencyChange;
  }

  get currency() {
    return this.layoutService.settings.info.currency;
  }

  changeMethod(method) {
    this.cart.type = method;
    this.roter.navigate([
      '/',
      this.layoutService.getCoupleInformation().url,
      'checkout',
      'payment',
      method
    ])
  }

}
