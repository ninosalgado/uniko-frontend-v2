import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { OrderService } from 'src/app/services/order.service';
import { CoupleService } from 'src/app/services/couple.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { IPaymentInfo, IOrder } from 'src/app/_interface/order';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.scss']
})
export class RecoveryComponent implements OnInit {
  id: string;
  constructor(
    private layoutService: LayoutService,
    private orderService: OrderService,
    private coupleService: CoupleService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) { 
    this.id = this.route.snapshot.params.id;
    this.get();
    localStorage.clear();
    this.layoutService.clear();
  }

  ngOnInit() {
  }

  async get() {
    try {
      this.spinner.show();
      const order: any = await this.orderService.recoveryCart(this.id);
      const couple: any = await this.coupleService.findOneRestrict(
        order.url[order.key].url
      );
      this.layoutService.cart.clear();
      this.layoutService.order.clear();
      order.products[order.key].forEach(element => {
        const art = JSON.parse(JSON.stringify(element));
        art.quantity = element.bought;
        this.layoutService.cart.add(art);
      });
      this.layoutService.setCoupleInformation(couple);
      const payment: IPaymentInfo = {
        address: {},
        products: this.layoutService.cart.productIds,
        birthdate: {},
        payer: order.paymentInfo[order.key].payer,
        pay: order.paymentInfo[order.key].pay,
        message: order.paymentInfo[order.key].message,
        urlRegistry: order.url[order.key].url
      }
      let _order: IOrder;
      console.log(payment);
      _order = await this.orderService.updateOrder(
        this.id,
        payment
      );
      this.layoutService.order.set = _order;
      this.router.navigate([
        '/',
        couple.fakeid,
        'checkout',
        'cart'
      ])
    } catch (e) {
      this.notification.error(e);
      this.router.navigate([
        '/search',
      ])
    } finally {
      this.spinner.hide();
    }
  }

}
