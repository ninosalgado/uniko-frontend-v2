import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { Route } from '@angular/compiler/src/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
import { IOrder, IPaymentInfo } from 'src/app/_interface/order';
import { KEYS } from 'src/environments/environment';
declare var PAYPAL: any;

@Component({
  selector: 'app-paypal-plus',
  templateUrl: './paypal-plus.component.html',
  styleUrls: ['./paypal-plus.component.scss']
})
export class PaypalPlusComponent implements OnInit {
  couple: ICoupleInformation;
  commision: any;
  paypalId: any;
  paypalPlus: any;
  constructor(
    private layoutService: LayoutService,
    public spinner: NgxSpinnerService,
    private notification: ToastrService,
    private orderService: OrderService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.couple = this.layoutService.getCoupleInformation();
    this.getCommision();
  }

  ngOnInit() {
  }

  load(info: any) {
    const couple = this.layoutService.coupleInformation.info;
    this.paypalId = info.paypal.id;
      this.paypalPlus =  PAYPAL.apps.PPP({
        approvalUrl: info.paypal.url.href,
        buttonLocation: "outside",
        preselection: "none",
        surcharging: false,
        hideAmount: false,
        placeholder: "paypal-plus",
        disableContinue: "continueButton",
        enableContinue: "continueButton",
        onContinue: (rememberedCards, PayerID, token, term) => {
          console.log("start .......................");
          this.spinner.hide();
          this.router.navigate([
            '/',
          this.layoutService.coupleInformation.info.url,
          'checkout',
          'payment',
          'confirm',
          this.layoutService.order.get.id
          ], {queryParams: {
            token,
            PayerID,
            paymentId: this.paypalId
          }});
        },
        onError:  (err) => {
          console.log(err);
          this.spinner.hide();
        },
        onLoad: (err) => {
          console.log(err);
        },
        language: "es_MX",
        country: "MX",
        disallowRememberedCards: true,
        rememberedCards: true,
        mode: KEYS.paypalLive,
        useraction: "continue",
        payerEmail: this.layoutService.order.get.paymentData.message.emails[0].text,
        payerPhone: this.layoutService.order.get.paymentData.message.tel,
        payerFirstName: this.layoutService.order.get.paymentData.message.givers[0].text,
        payerLastName: this.layoutService.order.get.paymentData.message.givers[0].text,
        payerTaxId: "",
        payerTaxIdType: "",
        merchantInstallmentSelection: 1,
        merchantInstallmentSelectionOptional: 1,
        hideMxDebitCards: true,
        iframeHeight: 460
      });
  }

  async getCommision() {
    try {
      this.spinner.show();
      this.commision = await this.orderService.calculateComission({
        cardNumber: 'paypal',
        value: this.layoutService.cart.total
      });
      this.layoutService.cart.setCommisions(this.commision.comission, 0);
      this.save();
    } catch (e) {
      this.notification.error(e);
      this.spinner.hide();
    }
  }

  async save () {
    try {
      const payment: IPaymentInfo = {
        address: {},
        payer: {
        },
        otro: {
          pay_type: 'paypal-plus'
        },
        first6: '',
        pay: {
          name: '',
          month: null,
          year: null
        },
        products: this.layoutService.cart.productIds,
        birthdate: {},
        method: 'paypal-plus',
        message: {
          givers: this.layoutService.order.get.paymentData.message.givers,
          emails: this.layoutService.order.get.paymentData.message.emails,
          prefijo: this.layoutService.order.get.paymentData.message.prefijo,
          message: this.layoutService.order.get.paymentData.message.message,
          tel: this.layoutService.order.get.paymentData.message.tel,
          names: this.layoutService.order.get.paymentData.message.names
        },
        urlRegistry: this.couple.url
      }
      const order = await this.orderService.makePaypalPlusPayment(
        this.layoutService.order.get.id,
        payment
      );
      this.load(order);
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
