import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaypalPlusComponent } from './paypal-plus.component';

describe('PaypalPlusComponent', () => {
  let component: PaypalPlusComponent;
  let fixture: ComponentFixture<PaypalPlusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaypalPlusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaypalPlusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
