import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderService } from 'src/app/services/order.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder } from '@angular/forms';
import { LayoutService } from 'src/app/services/layout.service';
import { IOrder } from 'src/app/_interface/order';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  constructor(
    private layoutService: LayoutService,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private orderService: OrderService,
    private router: Router,
    private route: ActivatedRoute
  ) { 
    const token = this.route.snapshot.queryParams.token;
    const payerID = this.route.snapshot.queryParams.PayerID;
    const paymentId = this.route.snapshot.queryParams.paymentId;
    const orderId = this.route.snapshot.params.orderId;
    this.confirm(orderId, token, payerID, paymentId);
  }

  ngOnInit() {
  }

  async confirm(orderId: string, token: string, payerID: string, paymentId: string) {
    try {
      this.spinner.show();
      console.log("nnnn------->", paymentId);
      const order: IOrder = await this.orderService.confirmPaypalPayment(
        orderId,
        paymentId,
        payerID,
        token
      );
      console.log(order);
      this.layoutService.order.set = order;
      this.router.navigate([
        '/', 
        this.layoutService.coupleInformation.info.url, 
        'checkout', 
        'congrats'
      ])
    } catch (e) {
      this.notification.error(e);
      this.router.navigate([
        '/',
        this.layoutService.coupleInformation.info.url,
        'checkout',
        'payment',
        'paypal'
      ])
    } finally {
      this.spinner.hide();
    }
  }

}
