import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckoutComponent } from './checkout.component';
import { CartComponent } from './cart/cart.component';
import { MessageComponent } from './message/message.component';
import { MethodsComponent } from './methods/methods.component';
import { CardComponent } from './card/card.component';
import { CongratsComponent } from './congrats/congrats.component';
import { OxxoComponent } from './oxxo/oxxo.component';
import { SpeiComponent } from './spei/spei.component';
import { PaypalComponent } from './paypal/paypal.component';
import { PaypalPlusComponent } from './paypal-plus/paypal-plus.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { RecoveryComponent } from './recovery/recovery.component';
import { RappiComponent } from './rappi/rappi.component';



const routes: Routes = [{
  path: '',
  component: CheckoutComponent,
  children: [{
    path: 'cart',
    component: CartComponent
  }, {
    path: 'recovery/:id',
    component: RecoveryComponent
  }, {
    path: 'message',
    component: MessageComponent
  }, {
    path: 'congrats',
    component: CongratsComponent
  }, {
    path: 'payment',
    component: MethodsComponent,
    children: [{
      path: 'card',
      component: CardComponent
    }, {
      path: 'oxxo',
      component: OxxoComponent
    }, {
      path: 'rappi',
      component: RappiComponent
    }, {
      path: 'spei',
      component: SpeiComponent
    }, {
      path: 'paypal',
      component: PaypalComponent
    }, {
      path: 'confirm/:orderId',
      component: ConfirmComponent
    }, {
      path: 'paypal-plus',
      component: PaypalPlusComponent
    }]
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutRoutingModule { }
