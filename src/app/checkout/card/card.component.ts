import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { COUNTRIES_PHONE } from 'src/app/_const/country-phones';
import { Order } from 'src/app/_models/order';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { KEYS } from 'src/environments/environment';
import { Token } from '@angular/compiler';
import { IPaymentInfo, IOrder } from 'src/app/_interface/order';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';


declare var Conekta: any;

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  registerForm: FormGroup;
  couple: ICoupleInformation;
  countries = COUNTRIES_PHONE;
  submit: boolean = false;
  order: Order;
  years: number[] = [];
  months: string[] = [];
  monthsMSI: any[] = [];
  commision: any;
  type: any = {
    type: '',
    card: 16,
    cvc: 3
  }
  types: any = [{
    type: 'visa',
    card: 16,
    cvc: 3
  }, {
    type: 'mastercard',
    card: 16,
    cvc: 3
  }, {
    type: 'amex',
    card: 15,
    cvc: 4
  }]
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private orderService: OrderService,
    private router: Router
  ) {
    this.couple = this.layoutService.getCoupleInformation();
    this.monthsMSI.push({
      msi: 1,
      label: this.layoutService.cart.total
    });
    const year = new Date().getFullYear();
    this.years = Array.from({ length: 11 }, (v, i) => year + i)
    this.months = Array.from({ length: 12 }, (v, i) => String(i + 1));
    Conekta.setPublicKey(KEYS.CONEKTA_GUEST);
    console.log(this.layoutService.order.get);
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: [this.layoutService.order.get.paymentData.message.givers[0].text, [Validators.required, Validators.pattern('[^0-9]*')]],
      email: [this.layoutService.order.get.paymentData.message.emails[0].text, Validators.required],
      confirmEmail: ['', Validators.required],
      phone: [this.layoutService.order.get.paymentData.message.tel, Validators.required],
      numberCard: ['', [Validators.required, Validators.pattern('[0-9]*')]],
      country: ['0052', Validators.required],
      city: ['', Validators.required],
      msi: ['1', Validators.required],
      cvc: ['', [Validators.required, Validators.pattern('[0-9]*')]],
      year: [new Date().getFullYear(), Validators.required],
      month: [String(new Date().getMonth() + 1), Validators.required]
    });
    this.getCommision();
  }

  async validCard() {
    const first = this.registerForm.value.numberCard.slice(0, 1);
    switch (first) {
      case '4':
        if (this.type.type !== 'visa') {
          this.type = this.types.find(data => data.type == 'visa');
          this.getCommision();
        }
        break;
      case '3':
        if (this.type.type !== 'amex') {
          this.type = this.types.find(data => data.type == 'amex');
          this.getCommision();
        }
        break;
      case '5':
        if (this.type.type !== 'mastercard') {
          this.type = this.types.find(data => data.type == 'mastercard');
          this.getCommision();
        }
        break;
      default:
    }
  }

  async getCommision() {
    try {
      this.spinner.show();
      this.commision = await this.orderService.calculateComission({
        cardNumber: this.registerForm.value.numberCard ? this.registerForm.value.numberCard : '4',
        value: this.layoutService.cart.total
      });
      this.commision.comissionMsi = this.commision.comission_msi;
      this.layoutService.cart.setCommisions(this.commision.comission, 0);
      if (this.registerForm.value.msi == 3) {
        this.layoutService.cart.setCommisions(this.commision.comission, this.commision.comissionMsi);
      } else {
        this.layoutService.cart.setCommisions(this.commision.comission, 0);
      }
      this.monthsMSI = [{
        msi: '3',
        label: (this.commision.comission + this.commision.comissionMsi + this.layoutService.cart.total) / 3
      }];
      console.log(this.commision.comission, this.commision.comissionMsi, this.layoutService.cart.total);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }



  async save() {
    try {
      this.spinner.show();
      this.submit = true;
      if (this.registerForm.invalid) {
        return;
      }
      const token = {
        card: {
          number: this.registerForm.value.numberCard,
          name: this.registerForm.value.name,
          exp_year: this.registerForm.value.year,
          exp_month: this.registerForm.value.month,
          cvc: this.registerForm.value.cvc
        }
      }
      const response = await this.createToken(token);
      const country: any = this.countries.find(data => data.phone == this.registerForm.value.country);
      const payment: IPaymentInfo = {
        address: {},
        payer: {
          country: country.iso,
          city: this.registerForm.value.city,
          email: this.registerForm.value.email,
          fullname: this.registerForm.value.name,
          phone: this.registerForm.value.phone
        },
        otro: {
          pay_type: this.type.type
        },
        first6: String(this.registerForm.value.numberCard).slice(0, 6),
        pay: response.id,
        msi: this.registerForm.value.msi ? this.registerForm.value.msi : 1,
        products: this.layoutService.cart.productIds,
        birthdate: {},
        method: 'Card',
        message: {
          givers: this.layoutService.order.get.paymentData.message.givers,
          emails: this.layoutService.order.get.paymentData.message.emails,
          prefijo: this.layoutService.order.get.paymentData.message.prefijo,
          message: this.layoutService.order.get.paymentData.message.message,
          tel: this.layoutService.order.get.paymentData.message.tel,
          names: this.layoutService.order.get.paymentData.message.names
        },
        urlRegistry: this.couple.url
      }
      const order: IOrder = await this.orderService.makeCardPayment(
        this.layoutService.order.get.id,
        payment
      );
      this.layoutService.order.set = order;
      this.router.navigate(['/', this.couple.url, 'checkout', 'congrats'])
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async createToken(card): Promise<any> {
    return new Promise((resolve, reject) => {
      Conekta.token.create(card, (token: Token) => {
        return resolve(token);
      }, (error) => {
        reject(error.message_to_purchaser);
      })
    });
  }

}
