import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { COUNTRIES_PHONE } from 'src/app/_const/country-phones';
import { Order } from 'src/app/_models/order';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { KEYS } from 'src/environments/environment';
import { IPaymentInfo, IOrder } from 'src/app/_interface/order';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

declare var RappiPagaCheckout: any;
@Component({
  selector: 'app-rappi',
  templateUrl: './rappi.component.html',
  styleUrls: ['./rappi.component.scss']
})
export class RappiComponent implements OnInit {
  registerForm: FormGroup;
  couple: ICoupleInformation;
  countries = COUNTRIES_PHONE;
  submit: boolean = false;
  order: Order;
  commision: any;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private orderService: OrderService,
    private router: Router
  ) {
    this.couple = this.layoutService.getCoupleInformation();
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: [this.layoutService.order.get.paymentData.message.givers[0].text, [Validators.required, Validators.pattern('[^0-9]*')]],
      email: [this.layoutService.order.get.paymentData.message.emails[0].text, Validators.required],
      confirmEmail: ['', Validators.required],
      phone: [this.layoutService.order.get.paymentData.message.tel, [Validators.required, Validators.pattern('[0-9]*')]],
    });
    this.getCommision();
  }

  async getCommision() {
    try {
      this.spinner.show();
      this.commision = await this.orderService.calculateComission({
        cardNumber: 'rappi',
        value: this.layoutService.cart.total
      });
      this.layoutService.cart.setCommisions(this.commision.comission, 0);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

  async updatePay(rappiId) {
    try {
      this.spinner.show();
      const order = await this.orderService.confirmRappiPayment(this.layoutService.order.get.id, rappiId);
      this.spinner.hide();
      this.router.navigate(['/', this.couple.url, 'checkout', 'congrats'])
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }


  async save() {
    try {
      this.spinner.show();
      const [{
          text: name
      }] = this.layoutService.order.get.paymentData.message.givers;
      const [{
        text: email
      }] = this.layoutService.order.get.paymentData.message.emails;

      const payment: IPaymentInfo = {
        address: {},
        payer: {
          city: '',
          email: email,
          fullname: name,
          phone: this.layoutService.order.get.paymentData.message.tel,
        },
        otro: {
          pay_type: 'rappi'
        },
        first6: '',
        pay: {
          name: name,
          month: null,
          year: null
        },
        products: this.layoutService.cart.productIds,
        birthdate: {},
        method: 'rappi',
        message: {
          givers: this.layoutService.order.get.paymentData.message.givers,
          emails: this.layoutService.order.get.paymentData.message.emails,
          prefijo: this.layoutService.order.get.paymentData.message.prefijo,
          message: this.layoutService.order.get.paymentData.message.message,
          tel: this.layoutService.order.get.paymentData.message.tel,
          names: this.layoutService.order.get.paymentData.message.names
        },
        urlRegistry: this.couple.url
      }
      const order: any = await this.orderService.makeRappiPayment(
        this.layoutService.order.get.id,
        payment
      );

      const checkout = RappiPagaCheckout({
        checkout_id: order.externalId,
        callback_url: false,
        type: 'popop',
        onSuccess: (id) => this.updatePay(id),
        onFailure: (id) => console.log('Failure: ' + id),
        onClose: (id) => console.log('Close: ' + id),
        onCancel: (id) => console.log('Cancel: ' + id)
    });
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
