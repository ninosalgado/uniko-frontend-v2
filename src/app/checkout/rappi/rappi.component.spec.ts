import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RappiComponent } from './rappi.component';

describe('RappiComponent', () => {
  let component: RappiComponent;
  let fixture: ComponentFixture<RappiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RappiComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RappiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
