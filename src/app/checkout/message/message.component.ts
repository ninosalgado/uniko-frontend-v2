import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { COUNTRIES_PHONE } from 'src/app/_const/country-phones';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Order } from 'src/app/_models/order';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { IPaymentInfo, IOrder } from 'src/app/_interface/order';
import { Cart } from 'src/app/_models/cart';
import { Router } from '@angular/router';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  registerForm: FormGroup;
  couple: ICoupleInformation;
  countries = COUNTRIES_PHONE;
  submit: boolean = false;
  order: Order;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private orderService: OrderService,
    private router: Router
  ) { 
    this.couple = this.layoutService.getCoupleInformation();
    this.order = this.layoutService.order;
  }

  ngOnInit() {
    console.log(this.order);
    this.registerForm = this.formBuilder.group({
      emails: [this.order.get ? this.getFormatMessage(this.order.get.paymentData.message.emails) : '', Validators.required],
      names: [this.order.get ? this.getFormatMessage(this.order.get.paymentData.message.givers) : '', Validators.required],
      phone: [this.order.get ? this.order.get.paymentData.message.tel : '', Validators.required],
      country: [this.order.get ? this.order.get.paymentData.message.prefijo : '0052', Validators.required],
      message: [this.order.get ? this.order.get.paymentData.message.message : '', Validators.required]
    })
  }

  get f() { return this.registerForm.controls; }

  getFormatMessage(info: any[]) {
    let text = [];
    info.forEach(data => {
      text.push(data.text)
    });
    return text.join(',');
  }


  formatMessage(info: string) {
    let _info = info.split(',');
    return _info.map(data => {
      return {
        text: data
      }
    })
  }


  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }  

  async save() {
    try {
      this.spinner.show();
      this.submit = true;
      this.markFormGroupTouched(this.registerForm);
      if (this.registerForm.invalid) {
        return;
      }
      const country: any = this.countries.find(data => data.phone == this.registerForm.value.country);

      const payment: IPaymentInfo = {
        address: {},
        payer: {
            country: country.iso
        },
        pay: {
          month: null,
          year: null
        },
        products: this.layoutService.cart.productIds,
        birthdate: {},
        message: {
          givers: this.formatMessage(this.registerForm.value.names),
          emails: this.formatMessage(this.registerForm.value.emails),
          prefijo: this.registerForm.value.country,
          message: this.registerForm.value.message,
          tel: this.registerForm.value.phone,
          names: []
        },
        urlRegistry: this.couple.url
      }
      let order: IOrder;
      if (this.order && this.order.get && this.order.get.id && !this.order.get.isPaid) {
        order = await this.orderService.updateOrder(
          this.order.get.id,
          payment
        )
      } else {
        order = await this.orderService.createOrder(payment);
      }
      this.order.set = order;
      this.router.navigate(['/', this.couple.url, 'checkout', 'payment', 'card'])
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
