import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckoutComponent } from './checkout.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutRoutingModule } from './checkout-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatRadioModule } from '@angular/material';
import { MessageComponent } from './message/message.component';
import { SharedModule } from '../shared/shared.module';
import { MethodsComponent } from './methods/methods.component';
import { CardComponent } from './card/card.component';
import { CongratsComponent } from './congrats/congrats.component';
import { OxxoComponent } from './oxxo/oxxo.component';
import { SpeiComponent } from './spei/spei.component';
import { PaypalComponent } from './paypal/paypal.component';
import { PaypalPlusComponent } from './paypal-plus/paypal-plus.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { RecoveryComponent } from './recovery/recovery.component';
import { StepsComponent } from './steps/steps.component';
import { DigitOnlyDirective } from './../_helpers/directive.digitOnly';
import { NoDigitsDirective } from './../_helpers/directive.noDigits';
import { RappiComponent } from './rappi/rappi.component';

@NgModule({
  declarations: [
    DigitOnlyDirective,
    NoDigitsDirective,
    StepsComponent,
    CheckoutComponent,
    CartComponent,
    MessageComponent,
    MethodsComponent,
    CardComponent,
    CongratsComponent,
    OxxoComponent,
    RappiComponent,
    SpeiComponent,
    PaypalComponent,
    PaypalPlusComponent,
    ConfirmComponent,
    RecoveryComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CheckoutRoutingModule,
    FormsModule,
    MatInputModule,
    MatRadioModule,
    SharedModule
  ]
})
export class CheckoutModule { }
