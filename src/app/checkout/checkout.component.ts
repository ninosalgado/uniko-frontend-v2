import { Component, OnInit } from '@angular/core';
import { Layout } from '../_models/layout';
import { Cart } from '../_models/cart';
import { LayoutService } from '../services/layout.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  cart: Cart;
  couple: any;
  constructor(
    private layoutService: LayoutService,
  ) {
    this.cart = this.layoutService.cart;
    this.couple = this.layoutService.getCoupleInformation();
    this.layoutService.coupleEmitter.subscribe(data => {
      this.couple = this.layoutService.getCoupleInformation();
    });
  }

  ngOnInit() {
  }

}
