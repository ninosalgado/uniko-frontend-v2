import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeiComponent } from './spei.component';

describe('SpeiComponent', () => {
  let component: SpeiComponent;
  let fixture: ComponentFixture<SpeiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
