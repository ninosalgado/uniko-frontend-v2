import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { COUNTRIES_PHONE } from 'src/app/_const/country-phones';
import { Order } from 'src/app/_models/order';
import { LayoutService } from 'src/app/services/layout.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from 'src/app/services/order.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { KEYS } from 'src/environments/environment';
import { Token } from '@angular/compiler';
import { IPaymentInfo, IOrder } from 'src/app/_interface/order';
import { ICoupleInformation } from 'src/app/_interface/coupleInformation';
declare var Conekta: any;

@Component({
  selector: 'app-spei',
  templateUrl: './spei.component.html',
  styleUrls: ['./spei.component.scss']
})
export class SpeiComponent implements OnInit {
  registerForm: FormGroup;
  couple: ICoupleInformation;
  countries = COUNTRIES_PHONE;
  submit: boolean = false;
  order: Order;
  commision: any;
  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private notification: ToastrService,
    private orderService: OrderService,
    private router: Router
  ) { 
    this.couple = this.layoutService.getCoupleInformation();
    Conekta.setPublicKey(KEYS.CONEKTA_GUEST);
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: [this.layoutService.order.get.paymentData.message.givers[0].text, [Validators.required, Validators.pattern('[^0-9]*')]],
      email: [this.layoutService.order.get.paymentData.message.emails[0].text, Validators.required],
      confirmEmail: ['', Validators.required],
      phone: [this.layoutService.order.get.paymentData.message.tel, [Validators.required, Validators.pattern('[0-9]*')]],
    });
    this.getCommision();
  }

  async getCommision() {
    try {
      this.spinner.show();
      this.commision = await this.orderService.calculateComission({
        cardNumber: 'spei',
        value: this.layoutService.cart.total
      });
      this.layoutService.cart.setCommisions(this.commision.comission, 0);
    } catch (e) {
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }



  async save () {
    try {
      this.spinner.show();
      this.submit = true;
      if (this.registerForm.invalid) {
        return ;
      }
      const payment: IPaymentInfo = {
        address: {},
        payer: {
            city: this.registerForm.value.city,
            email: this.registerForm.value.email,
            fullname: this.registerForm.value.name,
            phone: this.registerForm.value.phone
        },
        otro: {
          pay_type: 'spei'
        },
        first6: String(this.registerForm.value.numberCard).slice(0,6),
        pay: {
          name: this.registerForm.value.name,
          month: null,
          year: null
        },
        products: this.layoutService.cart.productIds,
        birthdate: {},
        method: 'spei',
        message: {
          givers: this.layoutService.order.get.paymentData.message.givers,
          emails: this.layoutService.order.get.paymentData.message.emails,
          prefijo: this.layoutService.order.get.paymentData.message.prefijo,
          message: this.layoutService.order.get.paymentData.message.message,
          tel: this.layoutService.order.get.paymentData.message.tel,
          names: this.layoutService.order.get.paymentData.message.names
        },
        urlRegistry: this.couple.url
      }
      const order: IOrder = await this.orderService.makeSpeiPayment(
        this.layoutService.order.get.id,
        payment
      );
      this.layoutService.order.set = order;
      this.router.navigate(['/', this.couple.url, 'checkout', 'congrats'])
    } catch (e) {
      console.log(e);
      this.notification.error(e);
    } finally {
      this.spinner.hide();
    }
  }

}
