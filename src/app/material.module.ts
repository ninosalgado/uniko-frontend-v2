import { NgModule } from '@angular/core';
import { 
    MatIconModule, 
    MatToolbarModule, 
    MatSnackBar,
    MatSnackBarModule,
    MatTooltipModule,
    MatButtonModule,
    MatGridListModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatRadioButton,
    MatRadioModule,
    MatSliderModule,
} from '@angular/material';
import {
    DragDropModule
} from '@angular/cdk/drag-drop'
// import { HeaderComponent } from './components/header/header.component';
;

@NgModule({
    declarations: [
    ],
    imports: [
        MatIconModule,
        MatToolbarModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatButtonModule,
        MatGridListModule,
        MatInputModule,
        MatCheckboxModule,
        MatDialogModule,
        MatMenuModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSliderModule,
        DragDropModule
    ],
    exports: [
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatButtonModule,
        MatGridListModule,
        MatInputModule,
        MatCheckboxModule,
        MatDialogModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSliderModule,
        DragDropModule
    ],

})

export class MaterialModule {}

